# Sorcery - Magical Authentication - による認証サンプル。

ユーザ登録、ログイン、ログアウトのみの小さなサンプル。

<i>Devise</i> は複雑すぎる。<a href="https://github.com/Sorcery/sorcery/">Sorcery: Magical Authentication</a> がお勧め。

See https://www.nslabs.jp/RoR-ruby-on-rails-hands-on-part-3.rhtml


## Sorcery モジュール

Sorcery はサブモジュールを組み合わせる。このサンプルでは "Remember Me" モジュールを有効にしている。

`config/initializers/sorcery.rb` ファイル:

```ruby
# Available submodules are: :user_activation, :http_basic_auth, :remember_me,
# :reset_password, :session_timeout, :brute_force_protection, :activity_logging,
# :magic_login, :external
Rails.application.config.sorcery.submodules = [:remember_me]
```



## How to run

`config/database.yml.sample` ファイルを `database.yml` にコピーして、適宜、編集。

```shell
  $ rake db:migrate
```

サーバ起動。

