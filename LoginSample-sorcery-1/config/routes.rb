# -*- coding:utf-8 -*-

Rails.application.routes.draw do
  # ユーザアカウント. 単数形リソース singleton resource を使う
  # 単数形リソースであってもデフォルトコントローラ名は複数形。
  # => controller: オプションでクラス名を指定する
  resource :account, only: [:show, :create, :edit, :update],
                     controller: 'account' do
    get    'sign_up'
    match  'sign_in', via:[:get, :post]
    delete 'sign_out'
  end

  # 顧客
  resources :customers

  # For details on the DSL available within this file,
  # see https://guides.rubyonrails.org/routing.html

  root "welcome#index"
end
