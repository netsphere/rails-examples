# -*- coding:utf-8 -*-

# 自動生成されたモデル
# users 表
class SorceryCore < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      # フィールド名は config の email_attribute_name で変更可.
      # デフォルト 'email'
      # メイルアドレスは最長 254 文字.
      t.string :email,            null: false, index: { unique: true }

      # フィールド名は config の crypted_password_attribute_name で変更可.
      # デフォルト 'crypted_password'
      t.string :crypted_password
      t.string :salt

      t.timestamps                null: false
    end

    #add_index :users, :email, unique: true
  end
end
