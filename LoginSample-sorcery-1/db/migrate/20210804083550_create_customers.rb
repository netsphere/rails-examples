# -*- coding:utf-8 -*-

# 顧客
# ログインユーザと別に作る.
class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    # 1対1の場合、どちらに外部キーを作るか。
    #   a. インスタンスが後から作られるほう, or 曖昧な場合は, 
    #   b. NOT NULL 制約を付けられるほう。概念としてサブクラスのほう。
    create_table :customers do |t|
      # 親
      t.belongs_to :user, index:{unique:true}, foreign_key:true, null:false

      t.string :address, null:false

      t.timestamps
    end
  end
end
