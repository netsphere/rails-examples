# -*- coding:utf-8 -*-

# 各コントローラの基底クラス.
# - ActionController::Base クラスは Rails における web request のコア。
#   ActionController::Metal クラスから派生.
# - Metal は actionpack パッケージ, AbstractController::Base から派生.
class ApplicationController < ActionController::Base
  # ポカ避けのため, ログイン不要のアクションだけ skip_before_action する.
  before_action :require_login


private
  
  # 未ログインの状態で要ログインのページにアクセスした時に呼び出される
  def not_authenticated
    redirect_to sign_in_account_url
  end  
end
