# -*- coding:utf-8 -*-

class ContentsController < ApplicationController
  # Pagy ページネーションを使う
  include Pagy::Backend

  # GET /
  def search
    # 値が配列のときは pagy_array() を使う.
    @pagy, @api_results = pagy(Content.all)
  end

  # DELETE /contents/1
  def destroy
    @content = Content.find params[:id]
    @content.destroy
    redirect_to '/', notice:'The content was successfully destroyed.',
                     status: :see_other 
  end
end
