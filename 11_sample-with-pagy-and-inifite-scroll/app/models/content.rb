class Content < ApplicationRecord
  validates :content, presence:true
end
