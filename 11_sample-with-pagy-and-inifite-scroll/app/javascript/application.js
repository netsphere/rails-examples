// Entry point for the build script in your package.json

import Rails from "@rails/ujs"
//import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "./channels"
import * as InfiniteScroll from "infinite-scroll";

Rails.start()
//Turbolinks.start()
ActiveStorage.start()

// Initialize with vanilla JavaScript
let elem = document.querySelector('#contents-list');
let infScroll = new InfiniteScroll( elem, {
    // options
    path: "nav.pagination a[rel=next]",
    append: ".content",
    hideNav: '.pagination',
    history: false,
    prefill: true,
    status: '.page-load-status'
  });

