# Rails 無限スクロール 5分チュートリアルサンプル

Ruby Gem Pagy + Infinite-Scroll Pluginで, データを無限スクロールするサンプルプログラム

See https://www.nslabs.jp/rubymemo.rhtml


## Dependencies

 - Ruby 3.1
 - Ruby on Rails 6.1. Webpacker 抜き. jsbundling-rails + webpack
 - Pagy 5.10   -- ページネィションは <i>will_paginate</i> が昔からあって良好, <i>kaminari</i> は Web上の解説多いが, 遅く、メモリも食う。<i>Pagy</i> は速度、メモリ節約が売り.
 - Slim 4.1
 - Infinite Scroll v4 (using <i>jsbundling-rails</i>, webpack)

Pagy は、Bootstrap と組み合わせるヘルパ関数も標準で用意されている。
https://ddnexus.github.io/pagy/extras/bootstrap#gsc.tab=0


## 先行事例

https://github.com/yuskubo/sample_with_pagy_and_inifite-scroll/
  - Rails 5.1
  - jQuery を利用
  - 表示するデータを配列で用意 (!)


## How to run

```shell
  $ git clone https://github.com/yuskubo/sample_with_pagy_and_inifite-scroll.git
```

```shell
  $ bin/rails db:migrate
  $ bin/rails db:seed
```

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
