class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :content, null:false

      t.timestamps null:false
    end
  end
end
