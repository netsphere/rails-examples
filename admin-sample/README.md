
# Rails で管理画面

簡単な商品マスタサンプル.
 - products
   + variants
 - product structure (BoM)
 - Quantity per Carton (QPC) packages
 - draft status
 - revisions
 
 
本格的なのは、例えば;
 - <a href="https://www.atropim.com/en/features">Features of AtroPIM | AtroPIM</a>
  demo: https://demo.atropim.com/
     Attribute
     Product Status - Draft, Prepared, Reviewed, Not Ready, Ready

 - https://github.com/pimcore/pimcore/
  Data Objects > Product Data
  [save draft] -> compare versions.
  


## 管理画面のライブラリ

Rails で管理画面を作るとき、ライブラリで開発効率を高められるか試してみた。

結論: どれもダメ。

 - <a href="https://rubygems.org/gems/rails_admin/">rails_admin</a>
   Live Demo を見る限り、ごく簡単な用途にはよさそう。

 - ▲ <a href="https://rubygems.org/gems/activeadmin/">activeadmin</a>
   右側のフィルタパネルがやや邪魔か。
   
 - <a href="https://rubygems.org/gems/administrate/">administrate</a>
   これも簡単な用途にはよさそう。

 - ○ https://github.com/motor-admin/motor-admin-rails/
   クエリなどもカスタマイズできる。が、カラムの値の表示方法などが固定か? ネストさせようとすると、クエリが参照先を取ってこれるのに値が表示できなかったりと、もう一歩。管理者がレコードをちょっと作ったり、みたいなのには有用。

 - ▲ https://docs.avohq.io/
   Rails とのヴァージョン相性か, 手許では JavaScript のビルドに失敗。原因は追い込んでいない。ライブデモは非常に面白く、改めて試してみたい。
   この辺りも試してみたが、やはり動かせなかった。何なんだ。
   + https://github.com/maglevhq/avo-weblog/
   + https://github.com/joelparkerhenderson/demo-rails-avo/


その他:
 - active_scaffold  歴史ある。開発継続。

 - trestle  新しい

 - bhf   最新リリースが 2023年 v1.0.0.beta16。https://antpaw.github.io/bhf/

 - ▲ typus  最終リリースが 2012年。終了
 


## React

Rails は API モードで動かし、フロントエンドは別に開発するのはどうか?

 - https://marmelab.com/react-admin/ よさそう。

 - https://refine.dev/ これもよさそう。



## motor-admin

`bundle install --path vendor/bundle` の `--path` フラグは非推奨.

```shell 
  $ bundle config set path vendor/bundle
  $ bundle install
```

motor-admin のテーブルを導入

```shell
  $ rails g motor:install && rake db:migrate
```

![screenshot 1](Screenshot_20241013_194642.png)
![screenshot 2](Screenshot_20241013_194729.png)



## CSS

https://bulmajs.tomerbe.co.uk/
