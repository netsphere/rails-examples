json.extract! prod_variant_trait, :id, :created_at, :updated_at
json.url prod_variant_trait_url(prod_variant_trait, format: :json)
