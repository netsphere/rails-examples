json.extract! sku_variant_value, :id, :created_at, :updated_at
json.url sku_variant_value_url(sku_variant_value, format: :json)
