json.extract! sales_sku, :id, :created_at, :updated_at
json.url sales_sku_url(sales_sku, format: :json)
