json.extract! sales_page, :id, :created_at, :updated_at
json.url sales_page_url(sales_page, format: :json)
