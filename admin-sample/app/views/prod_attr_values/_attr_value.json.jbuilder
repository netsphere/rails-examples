json.extract! attr_value, :id, :created_at, :updated_at
json.url attr_value_url(attr_value, format: :json)
