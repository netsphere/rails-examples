json.extract! prod_attr_value, :id, :created_at, :updated_at
json.url prod_attr_value_url(prod_attr_value, format: :json)
