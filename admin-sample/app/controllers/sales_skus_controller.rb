class SalesSkusController < ApplicationController
  before_action :set_sales_sku, only: %i[ show edit update destroy ]

  # GET /sales_skus or /sales_skus.json
  def index
    @sales_skus = SalesSku.all
  end

  # GET /sales_skus/1 or /sales_skus/1.json
  def show
  end

  # GET /sales_skus/new
  def new
    @sales_sku = SalesSku.new
  end

  # GET /sales_skus/1/edit
  def edit
  end

  # POST /sales_skus or /sales_skus.json
  def create
    @sales_sku = SalesSku.new(sales_sku_params)

    respond_to do |format|
      if @sales_sku.save
        format.html { redirect_to @sales_sku, notice: "Sales sku was successfully created." }
        format.json { render :show, status: :created, location: @sales_sku }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sales_sku.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_skus/1 or /sales_skus/1.json
  def update
    respond_to do |format|
      if @sales_sku.update(sales_sku_params)
        format.html { redirect_to @sales_sku, notice: "Sales sku was successfully updated." }
        format.json { render :show, status: :ok, location: @sales_sku }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sales_sku.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_skus/1 or /sales_skus/1.json
  def destroy
    @sales_sku.destroy!

    respond_to do |format|
      format.html { redirect_to sales_skus_path, status: :see_other, notice: "Sales sku was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_sku
      @sales_sku = SalesSku.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sales_sku_params
      params.fetch(:sales_sku, {})
    end
end
