
class ProdStructuresController < ApplicationController
  # POST
  def new
    @ps = ProdStructure.new
    @parent = Product.find params[:id]
    render turbo_stream: turbo_stream.prepend("child_items", partial:"form")
  end

  # POST
  def create
    @parent = Product.find params[:parent_id]
    @ps = ProdStructure.new params.require(:prod_structure).permit(:struc_type, :child_id, :qty)
    @ps.parent_id = @parent.product_code.id
    
    begin
      @ps.save!
      render turbo_stream: turbo_stream.replace("new_prod_structure", partial:"prod_structure", locals:{prod_structure:@ps})
    rescue ActiveRecord::RecordInvalid => e
      raise e.inspect 
    end
  end
end
