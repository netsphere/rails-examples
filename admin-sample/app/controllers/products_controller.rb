
# 品目
class ProductsController < ApplicationController
  before_action :set_sku,
        only: %i[ show edit update destroy approval_request add_attr edit_attrs update_attrs new_revision]

  # GET /skus or /skus.json
  def index
    @skus = ProductCode.latest_products(true)
  end

  # GET /skus/1 or /skus/1.json
  def show
  end

  # POST
  def approval_request
    @sku.appro_status = 20
    @sku.save!
    redirect_to({action:"show", id:@sku})
  end

  # POST
  def new_revision
    new_sku = @sku.dup # force new_record
    new_sku.revision_no = @sku.revision_no + 1
    new_sku.appro_status = 0  # draft

    begin
      ActiveRecord::Base.transaction do
        new_sku.save!
        @sku.prod_attr_values.each do |a|
          new_a = a.dup
          new_a.product_id = new_sku.id
          new_a.save!
        end
        @sku.packages.each do |pack|
          new_pack = pack.dup
          new_pack.product_id = new_sku.id
          new_pack.save!
        end
        
        redirect_to product_url(new_sku), notice: "Sku was successfully created."
      end
    rescue ActiveRecord::RecordInvalid => e
      render :show, status: :unprocessable_entity 
    end
  end

  
  # POST
  def add_attr
    attr_value = ProdAttrValue.new product_id: @sku.id, prod_attr_id: params[:prod_attr_id], raw_value: ""
    attr_value.save!
    render turbo_stream: turbo_stream.prepend("attributes", partial:"attr_value", locals:{attr_value:attr_value})
  end
  
  # GET /skus/new
  def new
    @sku = Product.new
    @prod_var_id = nil
    @sku_code = ProductCode.new
  end

  # GET /skus/1/edit
  def edit
    @prod_var_id = @sku.product_code.prod_variation &.id
  end

  # POST /skus or /skus.json
  def create
    @sku_code = ProductCode.new(id: params[:product_code])
    @prod_var_id = params[:prod_var_id]
    @sku_code.prod_variation_id = @prod_var_id
    @sku = Product.new(sku_params)

    begin
      ActiveRecord::Base.transaction do
        @sku_code.save!
        @sku.product_code_id = @sku_code.id
        @sku.save!
        redirect_to product_url(@sku), notice: "Sku was successfully created."
      end
    rescue ActiveRecord::RecordInvalid => e
      render :new, status: :unprocessable_entity 
    end
  end

  def edit_attrs
    
  end

  def update_attrs
  end
  
  
  # PATCH/PUT /skus/1 or /skus/1.json
  def update
    @prod_var_id = params[:prod_var_id]
    @sku.attributes = sku_params
    @sku.product_code.prod_variation_id = @prod_var_id

    begin
      ActiveRecord::Base.transaction do
        @sku.product_code.save!
        @sku.save!
        redirect_to product_url(@sku), notice: "Sku was successfully updated."
      end
    rescue ActiveRecord::RecordInvalid => e
      render :edit, status: :unprocessable_entity 
    end
  end

  
  # DELETE /skus/1 or /skus/1.json
  def destroy
    begin
      ActiveRecord::Base.transaction do
        @sku.destroy!
        if @sku.product_code.revisions.size == 0
          @sku.product_code.destroy!
        end
        redirect_to products_url, notice: "Sku was successfully destroyed." 
      end
    rescue
      raise
    end
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_sku
    @sku = Product.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def sku_params
    params.require(:product).permit(:cmpt_type, :name )
  end
end
