
# 販売ページ
class SalesPagesController < ApplicationController
  before_action :set_sales_page, only: %i[ show edit update destroy ]

  # GET /sales_pages or /sales_pages.json
  def index
    @sales_pages = SalesPage.all
  end

  # GET /sales_pages/1 or /sales_pages/1.json
  def show
  end

  # GET /sales_pages/new
  def new
    @sales_page = SalesPage.new
  end

  # GET /sales_pages/1/edit
  def edit
  end

  # POST /sales_pages or /sales_pages.json
  def create
    @sales_page = SalesPage.new(sales_page_params)

    respond_to do |format|
      if @sales_page.save
        format.html { redirect_to sales_page_url(@sales_page), notice: "Sales page was successfully created." }
        format.json { render :show, status: :created, location: @sales_page }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sales_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_pages/1 or /sales_pages/1.json
  def update
    respond_to do |format|
      if @sales_page.update(sales_page_params)
        format.html { redirect_to sales_page_url(@sales_page), notice: "Sales page was successfully updated." }
        format.json { render :show, status: :ok, location: @sales_page }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sales_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_pages/1 or /sales_pages/1.json
  def destroy
    @sales_page.destroy!

    respond_to do |format|
      format.html { redirect_to sales_pages_url, notice: "Sales page was successfully destroyed." }
      format.json { head :no_content }
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_sales_page
    @sales_page = SalesPage.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def sales_page_params
    params.require(:sales_page).permit(:title)
  end
end
