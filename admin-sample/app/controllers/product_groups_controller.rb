
# バリエーション
class ProductGroupsController < ApplicationController
  before_action :set_product, only: %i[ show edit update destroy ]

  # GET /products or /products.json
  def index
    @products = ProdVariation.all
  end

  # GET /products/1 or /products/1.json
  def show
    #@prod_keys = [:name, :color, :size_width, :size_depth, :size_height]
    @skus = []
    @product.product_codes.each do |prod_code|
      @skus << prod_code.latest_product(true)
    end
  end

  # GET /products/new
  def new
    @product = ProdVariation.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products or /products.json
  def create
    @product = ProdVariation.new(product_params)

    if @product.save
      redirect_to product_group_path(@product), notice: "Product was successfully created." 
    else
      render :new, status: :unprocessable_entity 
    end
  end

  # PATCH/PUT /products/1 or /products/1.json
  def update
    if @product.update(product_params)
      redirect_to product_group_url(@product), notice: "Product was successfully updated." 
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  # DELETE /products/1 or /products/1.json
  def destroy
    @product.destroy!

    redirect_to product_groups_url, notice: "Product was successfully destroyed." 
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = ProdVariation.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def product_params
    params.require(:prod_variation).permit(:name)
  end
end
