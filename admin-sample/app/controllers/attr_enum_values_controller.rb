
class AttrEnumValuesController < ApplicationController
  # POST
  def new
    @enum_value = AttrEnumValue.new
    @prod_attr = ProdAttr.find params[:id]
    render turbo_stream: turbo_stream.prepend("enum_values", partial:"form")
  end
  
  def create
    @prod_attr = ProdAttr.find params[:prod_attr_id]
    @enum_value = AttrEnumValue.new(params.require(:attr_enum_value).permit(:value, :aux_value1))
    @enum_value.prod_attr_id = @prod_attr.id

    begin
      @enum_value.save!
      render turbo_stream: turbo_stream.replace("new_attr_enum_value", partial:"attr_enum_value", locals:{attr_enum_value:@enum_value})
    rescue ActiveRecord::RecordInvalid => e
      raise e.inspect 
    end
  end
end
