
class PackagesController < ApplicationController
  before_action :set_package, only: %i[ show edit update destroy ]

  # GET /packages or /packages.json
  def index
    @packages = Package.all
  end

  # GET /packages/1 or /packages/1.json
  def show
  end

  # GET /packages/new
  def new
    @sku = Product.find params[:product_id]
    @package = Package.new
  end

  # GET /packages/1/edit
  def edit
    @sku = Product.find params[:product_id]
  end

  # POST /packages or /packages.json
  def create
    @package = Package.new(package_params)
    @sku = Product.find params[:sku_id]
    @package.product_id = @sku.id
      
    if @package.save
      redirect_to({controller:"products", action:"show", id:@sku},
                  notice: "Package was successfully created." )
    else
      render :new, status: :unprocessable_entity 
    end
  end

  
  # PATCH/PUT /packages/1 or /packages/1.json
  def update
    @sku = Product.find params[:product_id]
    if @package.update(package_params)
      redirect_to product_path(@sku), notice: "Package was successfully updated." 
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  
  # DELETE /packages/1 or /packages/1.json
  def destroy
    @package.destroy!

    redirect_to packages_path, status: :see_other, notice: "Package was successfully destroyed." 
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_package
    @package = Package.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def package_params
    params.require(:package).permit(:qpc, :case_no, :size_width, :size_depth, :size_height, :gross_weight)
  end
end
