// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import "./controllers"

//import Bulma from '@vizuaalog/bulmajs';
Bulma.parseDocument();

// rails7: after moving pages, BulmaJS does not work.
document.addEventListener("turbo:load", () => {
    Bulma.parseDocument();
});
