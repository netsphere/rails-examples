
# 物体のグループ
# TODO: rename `ProductGroup`
class ProdVariation < ApplicationRecord
  has_many :product_codes

  validates :name, presence: true

  # @return [true if same, value]
  def self.mix(skus)
    attr_keys = {}
    pack_keys = {}

    skus.each_with_index do |sku, idx|
      #prod = prod_code.latest_product(true)
      sku.prod_attr_values.each do |attr|
        attr_keys[attr.prod_attr.name] ||= Array.new(skus.length, "___hoge___")
        attr_keys[attr.prod_attr.name][idx] = attr.value
      end

      sku.packages.each do |pack|
        [:size_width, :size_depth, :size_height, :gross_weight].each do |k|
          pack_keys["#{pack.qpc}-#{pack.case_no}/#{k}"] ||= Array.new(skus.length, "___hoge___")
          pack_keys["#{pack.qpc}-#{pack.case_no}/#{k}"][idx] = pack.send(k)
        end       
      end
    end

    return [attr_keys, pack_keys]
  end
end
