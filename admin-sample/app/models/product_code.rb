
# これが商品
class ProductCode < ApplicationRecord
  has_many :revisions, class_name:"Product"

  # 単品のとき && only variations exist
  belongs_to :prod_variation, optional: true
  
  # product structure
  has_many :parent_hierarchies,
           class_name: "ProdStructure", foreign_key:"child_id"
  has_many :parents, through: :parent_hierarchies, source: "parent"

  has_many :child_hierarchies,
           class_name: "ProdStructure", foreign_key:"parent_id"
  has_many :children, through: :child_hierarchies, source: "child"
  
  before_validation :check_fields

  def self.latest_products(include_draft = false)
    # See https://qiita.com/yokoto/items/f52a253f821fee38168d
    # `USING` = `ON a.f1 = b.f1`
    prods = Product.joins(%Q{
INNER JOIN (
    SELECT product_code_id, MAX(revision_no) as revision_no
        FROM products
        #{include_draft ? "" : "WHERE appro_status = 20"}
        GROUP BY product_code_id
    ) USING (product_code_id, revision_no)
} )
    return prods
  end

  def latest_product(include_draft = false)
    return ProductCode.latest_products(include_draft).where('product_code_id = ?', self.id).first
  end

  
private
  # For `before_validation`
  def check_fields
    # auto coded
    self.id ||= SecureRandom.alphanumeric(11) # 62^11 = 65.4 bits
  end
end

