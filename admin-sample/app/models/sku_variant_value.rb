
# variant がある場合のみ
# 多:対
class SkuVariantValue < ApplicationRecord
  # sales_pages に繋げる多:多
  belongs_to :prod_trait, class_name: "SalesVariantTrait"
  
  # 条件が全部一致した場合に, product を選択する
  # Unlike `ProdAttrValue` class, it is linked to the `ProductCode` class.
  #belongs_to :product_code
  belongs_to :sales_sku
  
  #belongs_to :attr_value, class_name: "ProdAttrValue"
  belongs_to :enum_value, class_name:"AttrEnumValue", optional: true

  before_validation :check_fields

  
  def value
    v = if enum_value_id
          enum_value.value
        else
          self[:raw_value]
        end
    case prod_trait.prod_attr.attr_type
    when ProdAttr::TYPE_LENGTH, ProdAttr::TYPE_WEIGHT, ProdAttr::TYPE_AMOUNT
      return BigDecimal(v)
    when ProdAttr::TYPE_COLOUR, ProdAttr::TYPE_STRING, ProdAttr::TYPE_TEXT,
         ProdAttr::TYPE_RICHTEXT, ProdAttr::TYPE_ENUM
      return v
    when ProdAttr::TYPE_INT
      return v.to_i
    when ProdAttr::TYPE_BOOL
      return ["1", "TRUE", "YES", "ON"].include?(v.to_s.upcase)
    else
      raise "internal error"
    end
  end

  
private
  def check_fields
    if enum_value_id
      self.raw_value = ""
    end
  end
  
end
