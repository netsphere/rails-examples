
# enum values
class AttrEnumValue < ApplicationRecord
  # 親. colour など
  belongs_to :prod_attr

  validates :value, presence: true

  before_validation :check_fields

private
  def check_fields
    self.aux_value1 = "" if !aux_value1
  end
  
end
