
# 販売可能な商品
class SaleableItem < ApplicationRecord
  belongs_to :sales_sku
  belongs_to :product_code
  
  validates :qty, presence: true
end
