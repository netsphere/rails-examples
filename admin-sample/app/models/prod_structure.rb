
# 構成マスタ
class ProdStructure < ApplicationRecord
  MANUFACTURE = 1; KIT = 2
  
  belongs_to :parent, class_name: "ProductCode"
  belongs_to :child, class_name: "ProductCode"

  validates :struc_type, presence: true
  validates :qty, presence: true
end
