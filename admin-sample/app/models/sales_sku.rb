
# 販売情報. セット品は、販売情報のほうで持つ。discount rate を設定するため
class SalesSku < ApplicationRecord
  belongs_to :sales_page

  has_many :saleable_items
  has_many :product_codes, through: :saleable_items

  # 商品が一つだけのときは商品コードと合わせる。後から変更不可
  # UNIQUE 制約を付ける -> 単品は一つのページにしか置かない
  validates :sales_sku_code, presence: true, uniqueness: true
  
  validates :name, presence: true
  validates :discount_rate, presence: true

  before_validation :check_fields

  
private
  # For `before_validation`
  def check_fields
    self.sales_sku_code ||= SecureRandom.alphanumeric(6) # 62^6 = 35.7 bits
  end
end
