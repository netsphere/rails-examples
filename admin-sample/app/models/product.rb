
# 物体 (品目) revision
class Product < ApplicationRecord
  #SINGLE = 1; BUNDLE_SET = 2; COMPONENT = 3

  STATUS_STR = {
    0  => "draft",
    10 => "confirming",
    20 => "approved" }
  
  belongs_to :product_code
  validates :revision_no, presence: true
  
  # (attr - value) pairs 
  has_many :prod_attr_values
  
  # if BUNDLE_SET, not use
  has_many :packages
  
  #validates :cmpt_type, presence: true
  
  validates :name, presence: true

  before_validation :check_fields

private
  def check_fields
    # 0 = draft, 10 = inflow, 20 = approved
    self.appro_status = 0 if !appro_status
    self.revision_no = 0  if !revision_no
  end
  
end
