
class Package < ApplicationRecord
  belongs_to :product

  # Quantity per carton (QPC)
  validates :qpc, presence:true

  # 1..
  validates :case_no, presence:true
  
  # WDH, cm
  validates :size_width, presence: true
  validates :size_depth, presence: true
  validates :size_height, presence: true

  # kg
  validates :gross_weight, presence: true
end
