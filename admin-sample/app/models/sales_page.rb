
# 販売ページ
class SalesPage < ApplicationRecord
  # products
  has_many :sales_skus

  # optional: variant がある場合
  has_many :variant_traits, class_name:"SalesVariantTrait"
end
