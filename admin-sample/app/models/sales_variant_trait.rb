
# 軸. 多対多
class SalesVariantTrait < ApplicationRecord
  belongs_to :sales_page

  # 縦軸. 複数ありうる
  belongs_to :prod_attr

  # 選択肢は並べる商品で必要なもののみ
  has_many :variant_values, class_name:"SkuVariantValue", foreign_key: :prod_trait_id
  
  validates :name, presence:true
end
