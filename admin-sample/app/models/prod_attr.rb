
# 軸
class ProdAttr < ApplicationRecord
  # 値と単位
  TYPE_LENGTH = 1  # 1次元の長さ. cm
  #TYPE_DIMENSIONS_W = 2  # 2次元の長さ. cm 3D の "D".
  #TYPE_DIMENSIONS_H = 3  # 3次元. 3cm  width x depth x height (cm)
  TYPE_WEIGHT = 4  # kg
  TYPE_AMOUNT = 5

  # enum
  # maple, oak natural, walnut, aluminium, white
  # gray (us) grey (uk)
  TYPE_COLOUR = 21
  
  # 汎用
  TYPE_INT = 91
  TYPE_STRING = 92
  TYPE_TEXT = 93
  TYPE_RICHTEXT = 94
  TYPE_BOOL     = 95
  TYPE_ENUM = 96

  # enum の場合のみ
  has_many :enum_values, class_name:'AttrEnumValue'

  validates :attr_type, presence: true
  
  validates :name, presence: true
  validates :name, uniqueness: true

  before_validation :check_fields

private
  def check_fields
    self.description = "" if !description
  end
  
end
