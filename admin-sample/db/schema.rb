# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_10_19_095005) do
  create_table "attr_enum_values", force: :cascade do |t|
    t.integer "prod_attr_id", null: false
    t.string "value", null: false
    t.string "aux_value1", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prod_attr_id"], name: "index_attr_enum_values_on_prod_attr_id"
  end

  create_table "motor_alert_locks", force: :cascade do |t|
    t.integer "alert_id", null: false
    t.string "lock_timestamp", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["alert_id", "lock_timestamp"], name: "index_motor_alert_locks_on_alert_id_and_lock_timestamp", unique: true
    t.index ["alert_id"], name: "index_motor_alert_locks_on_alert_id"
  end

  create_table "motor_alerts", force: :cascade do |t|
    t.integer "query_id", null: false
    t.string "name", null: false
    t.text "description"
    t.text "to_emails", null: false
    t.boolean "is_enabled", default: true, null: false
    t.text "preferences", null: false
    t.bigint "author_id"
    t.string "author_type"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_alerts_name_unique_index", unique: true, where: "deleted_at IS NULL"
    t.index ["query_id"], name: "index_motor_alerts_on_query_id"
    t.index ["updated_at"], name: "index_motor_alerts_on_updated_at"
  end

  create_table "motor_api_configs", force: :cascade do |t|
    t.string "name", null: false
    t.string "url", null: false
    t.text "preferences", null: false
    t.text "credentials", null: false
    t.text "description"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_api_configs_name_unique_index", unique: true, where: "deleted_at IS NULL"
  end

  create_table "motor_audits", force: :cascade do |t|
    t.string "auditable_id"
    t.string "auditable_type"
    t.string "associated_id"
    t.string "associated_type"
    t.bigint "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.bigint "version", default: 0
    t.text "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "motor_auditable_associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "motor_auditable_index"
    t.index ["created_at"], name: "index_motor_audits_on_created_at"
    t.index ["request_uuid"], name: "index_motor_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "motor_auditable_user_index"
  end

  create_table "motor_configs", force: :cascade do |t|
    t.string "key", null: false
    t.text "value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_motor_configs_on_key", unique: true
    t.index ["updated_at"], name: "index_motor_configs_on_updated_at"
  end

  create_table "motor_dashboards", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.text "preferences", null: false
    t.bigint "author_id"
    t.string "author_type"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "motor_dashboards_title_unique_index", unique: true, where: "deleted_at IS NULL"
    t.index ["updated_at"], name: "index_motor_dashboards_on_updated_at"
  end

  create_table "motor_forms", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.text "api_path", null: false
    t.string "http_method", null: false
    t.text "preferences", null: false
    t.bigint "author_id"
    t.string "author_type"
    t.datetime "deleted_at"
    t.string "api_config_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_forms_name_unique_index", unique: true, where: "deleted_at IS NULL"
    t.index ["updated_at"], name: "index_motor_forms_on_updated_at"
  end

  create_table "motor_note_tag_tags", force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "note_id", null: false
    t.index ["note_id", "tag_id"], name: "motor_note_tags_note_id_tag_id_index", unique: true
    t.index ["tag_id"], name: "index_motor_note_tag_tags_on_tag_id"
  end

  create_table "motor_note_tags", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_note_tags_name_unique_index", unique: true
  end

  create_table "motor_notes", force: :cascade do |t|
    t.text "body"
    t.bigint "author_id"
    t.string "author_type"
    t.string "record_id", null: false
    t.string "record_type", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id", "author_type"], name: "motor_notes_author_id_author_type_index"
    t.index ["record_id", "record_type"], name: "motor_notes_record_id_record_type_index"
  end

  create_table "motor_notifications", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.bigint "recipient_id", null: false
    t.string "recipient_type", null: false
    t.string "record_id"
    t.string "record_type"
    t.string "status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_id", "recipient_type"], name: "motor_notifications_recipient_id_recipient_type_index"
    t.index ["record_id", "record_type"], name: "motor_notifications_record_id_record_type_index"
  end

  create_table "motor_queries", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.text "sql_body", null: false
    t.text "preferences", null: false
    t.bigint "author_id"
    t.string "author_type"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_queries_name_unique_index", unique: true, where: "deleted_at IS NULL"
    t.index ["updated_at"], name: "index_motor_queries_on_updated_at"
  end

  create_table "motor_reminders", force: :cascade do |t|
    t.bigint "author_id", null: false
    t.string "author_type", null: false
    t.bigint "recipient_id", null: false
    t.string "recipient_type", null: false
    t.string "record_id"
    t.string "record_type"
    t.datetime "scheduled_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id", "author_type"], name: "motor_reminders_author_id_author_type_index"
    t.index ["recipient_id", "recipient_type"], name: "motor_reminders_recipient_id_recipient_type_index"
    t.index ["record_id", "record_type"], name: "motor_reminders_record_id_record_type_index"
    t.index ["scheduled_at"], name: "index_motor_reminders_on_scheduled_at"
  end

  create_table "motor_resources", force: :cascade do |t|
    t.string "name", null: false
    t.text "preferences", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_motor_resources_on_name", unique: true
    t.index ["updated_at"], name: "index_motor_resources_on_updated_at"
  end

  create_table "motor_taggable_tags", force: :cascade do |t|
    t.integer "tag_id", null: false
    t.bigint "taggable_id", null: false
    t.string "taggable_type", null: false
    t.index ["tag_id"], name: "index_motor_taggable_tags_on_tag_id"
    t.index ["taggable_id", "taggable_type", "tag_id"], name: "motor_polymorphic_association_tag_index", unique: true
  end

  create_table "motor_tags", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "motor_tags_name_unique_index", unique: true
  end

  create_table "packages", force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "qpc", null: false
    t.integer "case_no", null: false
    t.decimal "size_width", precision: 6, scale: 1, null: false
    t.decimal "size_depth", precision: 6, scale: 1, null: false
    t.decimal "size_height", precision: 6, scale: 1, null: false
    t.decimal "gross_weight", precision: 8, scale: 3, null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id", "qpc", "case_no"], name: "index_packages_on_product_id_and_qpc_and_case_no", unique: true
    t.index ["product_id"], name: "index_packages_on_product_id"
  end

  create_table "prod_attr_values", force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "prod_attr_id", null: false
    t.integer "enum_value_id"
    t.string "raw_value", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["enum_value_id"], name: "index_prod_attr_values_on_enum_value_id"
    t.index ["prod_attr_id"], name: "index_prod_attr_values_on_prod_attr_id"
    t.index ["product_id", "prod_attr_id"], name: "index_prod_attr_values_on_product_id_and_prod_attr_id", unique: true
    t.index ["product_id"], name: "index_prod_attr_values_on_product_id"
  end

  create_table "prod_attrs", force: :cascade do |t|
    t.integer "attr_type", null: false
    t.string "name", null: false
    t.string "description", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_prod_attrs_on_name", unique: true
  end

  create_table "prod_structures", force: :cascade do |t|
    t.integer "struc_type", null: false
    t.string "parent_id", limit: 36, null: false
    t.string "child_id", limit: 36, null: false
    t.integer "qty", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["child_id"], name: "index_prod_structures_on_child_id"
    t.index ["parent_id", "child_id"], name: "index_prod_structures_on_parent_id_and_child_id", unique: true
    t.index ["parent_id"], name: "index_prod_structures_on_parent_id"
  end

  create_table "prod_variations", force: :cascade do |t|
    t.string "name", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_codes", id: { type: :string, limit: 36 }, force: :cascade do |t|
    t.integer "prod_variation_id"
    t.index ["prod_variation_id"], name: "index_product_codes_on_prod_variation_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "product_code_id", limit: 36, null: false
    t.integer "revision_no", null: false
    t.string "name", null: false
    t.integer "appro_status", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_code_id", "revision_no"], name: "index_products_on_product_code_id_and_revision_no", unique: true
    t.index ["product_code_id"], name: "index_products_on_product_code_id"
  end

  create_table "saleable_items", force: :cascade do |t|
    t.integer "sales_sku_id", null: false
    t.string "product_code_id", limit: 36, null: false
    t.integer "qty", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_code_id"], name: "index_saleable_items_on_product_code_id"
    t.index ["sales_sku_id", "product_code_id"], name: "index_saleable_items_on_sales_sku_id_and_product_code_id", unique: true
    t.index ["sales_sku_id"], name: "index_saleable_items_on_sales_sku_id"
  end

  create_table "sales_pages", force: :cascade do |t|
    t.string "title", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sales_skus", force: :cascade do |t|
    t.integer "sales_page_id", null: false
    t.string "sales_sku_code", null: false
    t.string "name", null: false
    t.decimal "discount_rate", precision: 5, scale: 2, null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sales_page_id"], name: "index_sales_skus_on_sales_page_id"
    t.index ["sales_sku_code"], name: "index_sales_skus_on_sales_sku_code", unique: true
  end

  create_table "sales_variant_traits", force: :cascade do |t|
    t.integer "sales_page_id", null: false
    t.string "name", null: false
    t.integer "prod_attr_id", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prod_attr_id"], name: "index_sales_variant_traits_on_prod_attr_id"
    t.index ["sales_page_id"], name: "index_sales_variant_traits_on_sales_page_id"
  end

  create_table "sku_variant_values", force: :cascade do |t|
    t.integer "prod_trait_id", null: false
    t.integer "sales_sku_id", null: false
    t.integer "enum_value_id"
    t.string "raw_value", null: false
    t.integer "lock_version", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["enum_value_id"], name: "index_sku_variant_values_on_enum_value_id"
    t.index ["prod_trait_id", "sales_sku_id"], name: "index_sku_variant_values_on_prod_trait_id_and_sales_sku_id", unique: true
    t.index ["prod_trait_id"], name: "index_sku_variant_values_on_prod_trait_id"
    t.index ["sales_sku_id"], name: "index_sku_variant_values_on_sales_sku_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "crypted_password"
    t.string "salt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "attr_enum_values", "prod_attrs"
  add_foreign_key "motor_alert_locks", "motor_alerts", column: "alert_id"
  add_foreign_key "motor_alerts", "motor_queries", column: "query_id"
  add_foreign_key "motor_note_tag_tags", "motor_note_tags", column: "tag_id"
  add_foreign_key "motor_note_tag_tags", "motor_notes", column: "note_id"
  add_foreign_key "motor_taggable_tags", "motor_tags", column: "tag_id"
  add_foreign_key "packages", "products"
  add_foreign_key "prod_attr_values", "attr_enum_values", column: "enum_value_id"
  add_foreign_key "prod_attr_values", "prod_attrs"
  add_foreign_key "prod_attr_values", "products"
  add_foreign_key "prod_structures", "product_codes", column: "child_id"
  add_foreign_key "prod_structures", "product_codes", column: "parent_id"
  add_foreign_key "products", "product_codes"
  add_foreign_key "saleable_items", "product_codes"
  add_foreign_key "saleable_items", "sales_skus"
  add_foreign_key "sales_skus", "sales_pages"
  add_foreign_key "sales_variant_traits", "prod_attrs"
  add_foreign_key "sales_variant_traits", "sales_pages"
  add_foreign_key "sku_variant_values", "attr_enum_values", column: "enum_value_id"
  add_foreign_key "sku_variant_values", "sales_skus"
  add_foreign_key "sku_variant_values", "sales_variant_traits", column: "prod_trait_id"
end
