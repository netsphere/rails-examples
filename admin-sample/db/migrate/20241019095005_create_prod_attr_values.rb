

class CreateProdAttrValues < ActiveRecord::Migration[7.1]
  def change
    create_table :prod_attr_values do |t|
      t.references :product, null:false, foreign_key: true
      t.references :prod_attr, null:false, foreign_key: true

      # enum の場合
      t.references :enum_value, foreign_key:{to_table: :attr_enum_values}
      # enum でない場合
      t.string :raw_value, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:product_id, :prod_attr_id], unique: true
    end
  end
end
