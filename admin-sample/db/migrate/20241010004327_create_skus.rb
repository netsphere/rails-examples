
class CreateSkus < ActiveRecord::Migration[7.1]
  def change
    # 構成マスタ
    create_table :prod_structures do |t|
      # 製造 or KIT
      t.integer :struc_type, null:false
      
      t.references :parent, type:"VARCHAR(36)", null:false, foreign_key:{to_table: :product_codes}
      t.references :child, type:"VARCHAR(36)", null:false, foreign_key:{to_table: :product_codes}
      
      t.integer :qty, null:false

      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:parent_id, :child_id], unique: true
    end

  end
end
