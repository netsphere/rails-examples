
class CreateAttrValues < ActiveRecord::Migration[7.1]
  def change
    create_table :attr_enum_values do |t|
      t.references :prod_attr, null:false, foreign_key: true
      
      t.string :value, null:false
      
      # 手抜き. attr で決め打ちする
      t.string :aux_value1, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps
    end
  end
end
