
class CreateProducts < ActiveRecord::Migration[7.1]
  def change
    # 物体: バリエーション
    create_table :prod_variations do |t|
      t.string :name, null:false

      # sqlite does not support the array.
      #t.string :variant_keys, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps
    end

    # only `id` field
    create_table :product_codes, id: "VARCHAR(36)" do |t|
      # 単品 && only variations exist 
      t.references :prod_variation
    end
    
    # 物体 (品目), revision
    create_table :products do |t|
      t.references :product_code, type:"VARCHAR(36)", null:false, foreign_key:true
      # must not name `revision`. system reserved.
      t.integer :revision_no, null:false
      
      # 単品, セット品, 構成品 -> move to SalesSku
      #t.integer :cmpt_type, null:false

      t.string :name, null:false

      #t.string :color #, null:false
     
      t.integer :appro_status, null:false

      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:product_code_id, :revision_no], unique: true
    end
  end
end
