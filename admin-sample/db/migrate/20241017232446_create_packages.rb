
class CreatePackages < ActiveRecord::Migration[7.1]
  def change
    create_table :packages do |t|
      t.references :product, null:false, foreign_key:true

      # 1 or more
      t.integer :qpc, null:false
      
      # 1..
      t.integer :case_no, null:false

      #t.string :name, null:false

      # WDH
      # 幅 (cm)
      t.decimal :size_width, precision:6, scale:1, null:false
      # 奥行き (cm)
      t.decimal :size_depth, precision:6, scale:1, null:false
      # 高さ (cm)
      t.decimal :size_height, precision:6, scale:1, null:false

      # kg  10cm^3 = 1kg. 1m^3 = 1t
      t.decimal :gross_weight, precision:8, scale:3, null:false

      #t.integer :appro_status, null:false
      #t.integer :revision_no, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:product_id, :qpc, :case_no], unique: true
    end
  end
end
