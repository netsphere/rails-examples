
class CreateSkuVariantValues < ActiveRecord::Migration[7.1]
  def change
    # 販売ページに置くモノ. セット品は合計に対してディスカウント率を定める
    create_table :sales_skus do |t|
      # variant なしのときに必要
      t.references :sales_page, null:false, foreign_key: true

      # 商品が一つだけのときは商品コードと合わせる。後から変更不可
      # UNIQUE 制約を付ける -> 単品は一つのページにしか置かない
      t.string :sales_sku_code, type:"VARCHAR(36)", null:false, index:{unique:true}
        
      t.string :name, null:false
      
      t.decimal :discount_rate, precision: 5, scale: 2, null:false,
                comment: "100分率"
        
      t.integer :lock_version, default: 0
      t.timestamps
    end

    # セット品がある. 多対多になる
    create_table :saleable_items do |t|
      # こっちが親
      t.references :sales_sku, null:false, foreign_key: true

      # 複数ひもづく
      t.references :product_code, type:"VARCHAR(36)", null:false, foreign_key: true

      t.integer :qty, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:sales_sku_id, :product_code_id], unique: true
    end
    
    # only if there are variants
    create_table :sku_variant_values do |t|
      t.references :prod_trait, null:false, foreign_key:{to_table: :sales_variant_traits}

      # ここが sales_sku のほうを向く
      #t.references :product_code, type:"VARCHAR(36)", null:false, foreign_key: true
      t.references :sales_sku, null:false, foreign_key: true
      
      # 複数の組み合わせが全部一致したら, product を選ぶ
      #t.references :attr_value, null:false, foreign_key:{to_table: :prod_attr_values}
      # enum の場合
      t.references :enum_value, foreign_key:{to_table: :attr_enum_values}
      # enum でない場合
      t.string :raw_value, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps

      t.index [:prod_trait_id, :sales_sku_id], unique: true
    end
  end
end
