
class CreateAttributes < ActiveRecord::Migration[7.1]
  def change
    # colour, size, ...
    create_table :prod_attrs do |t|
      t.integer :attr_type, null:false
      t.string :name, null:false, index: { unique: true}
      t.string :description, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps
    end
  end
end
