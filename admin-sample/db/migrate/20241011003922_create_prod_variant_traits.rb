
class CreateProdVariantTraits < ActiveRecord::Migration[7.1]
  def change
    create_table :sales_pages do |t|
      t.string :title, null:false
      
      t.integer :lock_version, default: 0
      t.timestamps
    end

    # 多対多
    create_table :sales_variant_traits do |t|
      t.references :sales_page, null:false, foreign_key: true

      t.string :name, null:false
      
      # 軸. 
      t.references :prod_attr, null:false, foreign_key: true
      
      t.integer :lock_version, default: 0
      t.timestamps

      #t.index [:sales_page_id, :prod_attr_id], unique: true
    end
  end
end
