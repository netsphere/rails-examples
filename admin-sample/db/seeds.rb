# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

def make_data
  ActiveRecord::Base.transaction do
    # 軸 ###################
    
    attr_set = ProdAttr.create!(attr_type: ProdAttr::TYPE_ENUM, name: "set")
    attr_color = ProdAttr.create!(attr_type: ProdAttr::TYPE_COLOUR, name: "color")
    #attr_pants_color = ProdAttr.create!(attr_type: ProdAttr::TYPE_COLOUR, name: "pants-color")
    attr_size = ProdAttr.create!(attr_type: ProdAttr::TYPE_ENUM, name: "wear size")   
    attr_dim_width = ProdAttr.create!(attr_type: ProdAttr::TYPE_LENGTH, name:"dim.width")
    attr_dim_depth = ProdAttr.create!(attr_type: ProdAttr::TYPE_LENGTH, name:"dim.depth")
    attr_dim_height = ProdAttr.create!(attr_type: ProdAttr::TYPE_LENGTH, name:"dim.height")
    
    set_single = AttrEnumValue.create! prod_attr_id: attr_set.id, value: "T-shirt only"
    set_set    = AttrEnumValue.create! prod_attr_id: attr_set.id, value: "Set of T-shirt & Pants"
    color_blue = AttrEnumValue.create! prod_attr_id: attr_color.id, value: "blue"
    color_red  = AttrEnumValue.create! prod_attr_id: attr_color.id, value: "red"
    #pants_color_red  = AttrEnumValue.create! prod_attr_id: attr_pants_color.id, value: "red"
    size_small = AttrEnumValue.create! prod_attr_id: attr_size.id, value: "small"

    # 物体 #################
  
    prod1 = ProdVariation.create! name: "T-shirt #1" 
    prod2 = ProdVariation.create! name: "Pants #1" 

    sku1_code = ProductCode.create!(id:"SKU1", prod_variation_id: prod1.id)
    sku1 = Product.create!(product_code:sku1_code,
                           #cmpt_type: Product::SINGLE,
                           name: "T-shirt #1 blue, small")
    sku1.prod_attr_values << ProdAttrValue.new(prod_attr: attr_color, enum_value: color_blue) <<
                ProdAttrValue.new(prod_attr: attr_dim_width, raw_value: 10) <<
                ProdAttrValue.new(prod_attr: attr_dim_depth, raw_value: 5) <<
                ProdAttrValue.new(prod_attr: attr_dim_height, raw_value: 30)

    sku2_code = ProductCode.create!(id:"SKU2", prod_variation_id: prod1.id)
    sku2 = Product.create!(product_code:sku2_code,
                           #cmpt_type: Product::SINGLE,
                           name: "T-shirt #1 red, small")
    sku2.prod_attr_values << ProdAttrValue.new(prod_attr: attr_color, enum_value:color_red) <<
                ProdAttrValue.new(prod_attr: attr_dim_width, raw_value: 10) <<
                ProdAttrValue.new(prod_attr: attr_dim_depth, raw_value: 5) <<
                ProdAttrValue.new(prod_attr: attr_dim_height, raw_value: 30)

    sku3_code = ProductCode.create!(id:"SKU3", prod_variation_id: prod2.id)
    sku3 = Product.create!(product_code:sku3_code,
                           #cmpt_type: Product::SINGLE,
                           name: "Pants #1 red, small")
    sku3.prod_attr_values << ProdAttrValue.new(prod_attr: attr_color, enum_value:color_red) <<
                ProdAttrValue.new(prod_attr: attr_dim_width, raw_value: 10) <<
                ProdAttrValue.new(prod_attr: attr_dim_depth, raw_value: 5) <<
                ProdAttrValue.new(prod_attr: attr_dim_height, raw_value: 70)

    # 販売 ###################

    # Variant ありの場合

    page1 = SalesPage.create! title: "Good Nice Great T-shirt #1 small"

    var1 = SalesVariantTrait.create! sales_page_id: page1.id,
                        name: "T-shirt color", prod_attr_id: attr_color.id
    #var2 = ProdVariantTrait.create! sales_page_id: page.id, prod_attr_id: attr_size.id
    var3 = SalesVariantTrait.create! sales_page_id: page1.id,
                        name: "set", prod_attr_id: attr_set.id
    var4 = SalesVariantTrait.create! sales_page_id: page1.id,
                        name: "Pants color", prod_attr_id: attr_color.id

    # 販売可能品は、sales SKU をつくっていく
    sales_sku1 = SalesSku.create! sales_page: page1, sales_sku_code: sku1_code.id,
                              name: sku1.name, discount_rate: 0.0
    sales_sku1.saleable_items <<
                    (SaleableItem.new product_code: sku1_code, qty:1)
    sales_sku2 = SalesSku.create! sales_page: page1, sales_sku_code: sku2_code.id,
                              name: sku2.name, discount_rate: 0.0
    sales_sku2.saleable_items <<
                    (SaleableItem.new product_code: sku2_code, qty:1)
    # セット品は, 販売コードのほうでつくる
    sales_set1 = SalesSku.create! sales_page: page1, name: "Set of " + sku1.name + sku3.name,
                              discount_rate: 0.0
    sales_set1.saleable_items <<
                    (SaleableItem.new product_code: sku1_code, qty:1)
    sales_set1.saleable_items <<
                    (SaleableItem.new product_code: sku3_code, qty:1)

    # 掛け算で必要
    SkuVariantValue.create! prod_trait: var3, sales_sku: sales_sku1,
                            enum_value: set_single
    SkuVariantValue.create! prod_trait: var1, sales_sku: sales_sku1,
                            enum_value: color_blue
  
    SkuVariantValue.create! prod_trait: var3, sales_sku: sales_sku2,
                            enum_value: set_single
    SkuVariantValue.create! prod_trait: var1, sales_sku: sales_sku2,
                            enum_value: color_red

    SkuVariantValue.create! prod_trait: var3, sales_sku: sales_set1,
                            enum_value: set_set
    SkuVariantValue.create! prod_trait: var1, sales_sku: sales_set1,
                            enum_value: color_blue
    SkuVariantValue.create! prod_trait: var4, sales_sku: sales_set1,
                            enum_value: color_red

=begin    
    set1_code = ProductCode.create!(id:"SET1")
    set1 = Product.create!( product_code:set1_code,
                           cmpt_type: Product::BUNDLE_SET,
                           name: "T-shirt #1 blue & Pants #1 red")

    ProdStructure.create!(struc_type: ProdStructure::KIT,
                          parent: set1_code, child: sku1_code, qty:1)
    ProdStructure.create!(struc_type: ProdStructure::KIT,
                          parent: set1_code, child: sku3_code, qty:1)
=end

    # variant がない場合   ##########

    page2 = SalesPage.create! title: "Awesome pants #1 small"
    sales_sku5 = SalesSku.create! sales_page: page2,
                                  sales_sku_code: sku3_code.id,
                                  name: sku3.name, discount_rate:0.0
    sales_sku5.saleable_items <<
                    (SaleableItem.new product_code: sku3_code, qty:1)
  end
end

make_data

