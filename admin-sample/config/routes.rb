
Rails.application.routes.draw do

  mount Motor::Admin => '/motor_admin'
  #mount Avo::Engine, at: Avo.configuration.root_path

  # バリエーション
  resources :product_groups

  # 品目, セット品 & 構成品
  resources :products do
    member do
      post :new_child, to: "prod_structures#new"
      post :approval_request
      post :new_revision
      post :add_attr
      get :edit_attrs
      post :update_attrs
    end
    resources :packages
  end

  resources :prod_structures, except:[:new]
  
  # 軸
  resources :prod_attrs do
    member do
      post :new_enum, to: "attr_enum_values#new"
    end
    resources :attr_enum_values, except:[:new]
  end

  # 販売
  resources :sales_pages
  
  resources :sales_skus

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/*
  get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker
  get "manifest" => "rails/pwa#manifest", as: :pwa_manifest

  # Defines the root path route ("/")
  root :to => redirect("/product_groups/")
end
