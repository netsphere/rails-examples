require "rails_helper"

RSpec.describe SalesSkusController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/sales_skus").to route_to("sales_skus#index")
    end

    it "routes to #new" do
      expect(get: "/sales_skus/new").to route_to("sales_skus#new")
    end

    it "routes to #show" do
      expect(get: "/sales_skus/1").to route_to("sales_skus#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/sales_skus/1/edit").to route_to("sales_skus#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/sales_skus").to route_to("sales_skus#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/sales_skus/1").to route_to("sales_skus#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/sales_skus/1").to route_to("sales_skus#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/sales_skus/1").to route_to("sales_skus#destroy", id: "1")
    end
  end
end
