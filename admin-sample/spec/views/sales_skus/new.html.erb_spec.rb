require 'rails_helper'

RSpec.describe "sales_skus/new", type: :view do
  before(:each) do
    assign(:sales_sku, SalesSku.new())
  end

  it "renders new sales_sku form" do
    render

    assert_select "form[action=?][method=?]", sales_skus_path, "post" do
    end
  end
end
