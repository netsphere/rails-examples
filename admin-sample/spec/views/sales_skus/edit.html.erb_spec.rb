require 'rails_helper'

RSpec.describe "sales_skus/edit", type: :view do
  let(:sales_sku) {
    SalesSku.create!()
  }

  before(:each) do
    assign(:sales_sku, sales_sku)
  end

  it "renders the edit sales_sku form" do
    render

    assert_select "form[action=?][method=?]", sales_sku_path(sales_sku), "post" do
    end
  end
end
