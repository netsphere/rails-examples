# -*- coding:utf-8 -*-

Rails.application.routes.draw do
  resources :tags
  resources :articles
  # For details on the DSL available within this file,
  # see https://guides.rubyonrails.org/routing.html

  # リダイレクトさせる
  # 別のコントローラで表示させる場合は、次のようにする。
  #   root to: "welcome#index"
  root to: redirect("/articles/")
end
