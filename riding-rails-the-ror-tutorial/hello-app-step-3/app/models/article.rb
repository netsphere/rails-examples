# -*- coding:utf-8 -*-

# 記事
class Article < ApplicationRecord
  # 多対多
  has_many :articles_tags
  has_many :tags, through: :articles_tags
  
  # 検証: 列は複数並べてもよい.
  validates :title, presence:true

  # カスタム検証
  validate :my_validation
  
  # Callback として, before_save, before_validation などがある.
  before_save :normalize_text

private
  # for validate.
  def my_validation
    if title == "NG"
      errors[:title] << "値がNG!"
    end
  end
  
  # for before_save
  def normalize_text
    self.title = title.unicode_normalize :nfkc # 試しに
    self.body = body.unicode_normalize :nfc
  end
end
