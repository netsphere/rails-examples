# -*- coding:utf-8 -*-

# タグ
class Tag < ApplicationRecord
  # 中間テーブルが独自の列を一切持たない場合は, has_and_belongs_to_many を使う
  # が、そのような状況は稀。通常は has_many :through を使う.
  has_many :articles_tags
  has_many :articles, through: :articles_tags

  validates :name, presence:true

  # Callback として `before_save`, `before_validation` などがある.
  # 値を更新するときは `before_save()` ではなく `validate()` のなかに入れること.
  validate :normalize_text

private
  # for `validate`.
  def normalize_text
    self.name = name.unicode_normalize :nfkc
  end
  
end
