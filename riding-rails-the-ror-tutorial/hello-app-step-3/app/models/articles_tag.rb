# -*- coding:utf-8 -*-

# 中間テーブル
class ArticlesTag < ApplicationRecord
  belongs_to :article
  belongs_to :tag
end
