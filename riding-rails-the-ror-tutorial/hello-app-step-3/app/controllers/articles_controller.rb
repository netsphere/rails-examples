# -*- coding:utf-8 -*-

# 記事コントローラ
class ArticlesController < ApplicationController
  # 各アクション (メソッド) の前に呼び出される.
  before_action :set_article, only: %i[ show edit update destroy ]

  # GET /articles or /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1 or /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
    @tags_str = (@tags.map do |tag| tag.name end).join(" ")
  end


  # POST /articles or /articles.json
  def create
    @article = Article.new(article_params)
    @tags_str = params[:tags_str]
    begin
      ActiveRecord::Base.transaction do
        @article.save!
        update_tags! @tags_str
      end
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity
      return
    end
    redirect_to @article, notice: "Article was successfully created." 
  end


  # PATCH/PUT /articles/1 or /articles/1.json
  def update
    @tags_str = params[:tags_str]
    begin
      ActiveRecord::Base.transaction do 
        @article.update!(article_params)
        update_tags! @tags_str
      end
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity
      return
    end

    redirect_to @article, notice: "Article was successfully updated." 
  end


  # DELETE /articles/1 or /articles/1.json
  def destroy
    ActiveRecord::Base.transaction do
      ArticlesTag.where(article_id:@article.id).delete_all
      @article.destroy!
    end
    redirect_to articles_url, status: :see_other, notice: "Article was successfully destroyed." 
  end

private
  def update_tags! tags_str
    # create() は無効な場合は単にそのオブジェクトを返す。無用メソッド。
    tags_str = tags_str.unicode_normalize(:nfkc).strip
    tags = tags_str.split(/[ \t]+/).uniq.map do |tag_name|
             Tag.where(name:tag_name).first || Tag.create!(name:tag_name)
           end
    # 全部消して、作り直す.
    ArticlesTag.where(article_id:@article.id).delete_all
    tags.each_with_index do |tag, idx|
      ArticlesTag.create!(article_id:@article.id, tag_id:tag.id, order: idx + 1)
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    # includes() は挙動が制御しづらく、使うべきでない.
    @article = Article.find(params[:id])
    @tags = Tag.eager_load(:articles_tags)
              .where('articles_tags.article_id =?', @article.id)
              .order('articles_tags."order"')
  end

  # Only allow a list of trusted parameters through.
  def article_params
    params.require(:article).permit(:title, :body)
  end
end
