# -*- coding:utf-8 -*-

class ApplicationController < ActionController::Base

  around_action :switch_locale

  # 国際化対応 (手抜き) ... http://localhost:3000/articles/3/edit?locale=ja
  # 本当は、URL に埋め込むようにする。https://hostname/ja-jp/resources/x/edit
  # See https://guides.rubyonrails.org/i18n.html
  def switch_locale(&action)
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &action)
  end
end
