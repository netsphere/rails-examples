# -*- coding:utf-8 -*-

# View から呼び出せるメソッド群
module ApplicationHelper
  # error_messages_for <var>record</var> メソッドは Rails v3.0 で廃止.
  # error_message_on <var>object</var>, <var>method</var> メソッドも Rails v3.0 で廃止.
  # => 'dynamic_form' gem. しかし2011 年が最終。
  def error_messages_for model_obj
    if model_obj
      render(partial: "layouts/error_messages", locals:{model_obj: model_obj})
    end
  end
end
