# -*- coding:utf-8 -*-

# 多対多サンプル
class CreateTags < ActiveRecord::Migration[6.1]
  def change
    create_table :tags do |t|
      t.string :name, null:false, index:{unique:true}
      t.timestamps
    end

    # 中間テーブル
    create_table :articles_tags do |t|
      # belongs_to は単数形. article_id 列が生成される.
      t.belongs_to :article, null:false, foreign_key:true
      t.belongs_to :tag, null:false, foreign_key:true
      t.integer :order, null:false
    end
    add_index :articles_tags, [:article_id, :tag_id], unique:true
  end
end
