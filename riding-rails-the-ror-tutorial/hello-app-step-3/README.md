# hello-app-step-3

See https://www.nslabs.jp/RoR-ruby-on-rails-hands-on-part-3.rhtml


## Dependencies

 - Ruby 2.7.3 以降 (v3.1対応)
 - Ruby on Rails 6.1. Webpacker 抜き. jsbundling-rails + webpack


## How to run

`config/database.yml.sample` を `database.yml` にコピーして、適宜、修正.

```shell
  $ rake db:migrate
```

サーバ起動。



This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
