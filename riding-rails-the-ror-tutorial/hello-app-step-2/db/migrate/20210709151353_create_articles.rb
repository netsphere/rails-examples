# -*- coding:utf-8 -*-

# articles テーブルを生成する.
class CreateArticles < ActiveRecord::Migration[6.1]
  # change メソッドの代わりに, up / down メソッドも使える.
  def change
    create_table :articles do |t|
      t.string :title, null:false
      t.text :body, null:false

      t.timestamps
    end
  end
end

