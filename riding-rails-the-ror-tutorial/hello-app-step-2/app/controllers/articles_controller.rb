# -*- coding:utf-8 -*-

# 記事コントローラ
class ArticlesController < ApplicationController
  # 各アクション (メソッド) の前に呼び出される.
  before_action :set_article, only: %i[ show edit update destroy ]

  # GET /articles or /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1 or /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles or /articles.json
  def create
    @article = Article.new(article_params)
    begin
      @article.save!
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity
      return
    end
    redirect_to @article, notice: "Article was successfully created." 
  end

  # PATCH/PUT /articles/1 or /articles/1.json
  def update
    begin
      @article.update!(article_params)
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity
      return
    end

    redirect_to @article, notice: "Article was successfully updated." 
  end

  # DELETE /articles/1 or /articles/1.json
  def destroy
    @article.destroy
    redirect_to articles_url, notice: "Article was successfully destroyed." 
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def article_params
    params.require(:article).permit(:title, :body)
  end
end
