# -*- coding:utf-8 -*-

# 記事
class Article < ApplicationRecord
  # 検証: 列は複数並べてもよい.
  validates :title, presence:true

  # カスタム検証 & 値の整え
  validate :my_validation
  
  # Callback として, `before_save`, `before_validation` などがある.
  # Validation のほうが `before_save()` より先に実行される. ので、値の更新は
  # `validate()` のなかに入れないといけない.
  #before_save :normalize_text

private
  # for `validate`.
  def my_validation
    self.title = title.unicode_normalize :nfkc # 試しに
    self.body = body.unicode_normalize :nfc
    
    if title == "NG"
      errors[:title] << "値がNG!"
    end
  end

end
