
# Rails + Rodauth + 2FA sample

 - ✅ ユーザ登録. パスワードの複雑さはどこで決めている?
 - eメイル実在確認
 - ✅ ログイン
 - パスワードリセット
 - ログイン中
   + パスワード変更
   + eメイル変更
 - ✅ QRコードを表示し, 2FA登録 (任意)
 - 2FA登録されていれば, ログイン後に2FAを求める
 - 紛失時の再登録用コード
 - 2FAの削除
 - 認証が必要なエリアであればlogin画面にリダイレクト
 
 
```shell
$ rails rodauth:routes
```

<table>
  GET       /multifactor-manage      rodauth.two_factor_manage_path
      1. 要ログイン
      2. 2FA なければ /otp-setup にリダイレクトされる
      3. 2FA を設定ずみなら, /otp-disable にリダイレクト
  GET       /multifactor-auth        rodauth.two_factor_auth_path
  GET|POST  /multifactor-disable     rodauth.two_factor_disable_path
  GET|POST  /otp-auth                rodauth.otp_auth_path
  GET|POST  /otp-setup               rodauth.otp_setup_path
      2FA を QRコードから追加。要パスワード
  GET|POST  /otp-disable             rodauth.otp_disable_path
      2FA が設定ずみなら, disable にできる (要パスワード)
  GET|POST  /login                   rodauth.login_path
      ログイン画面
  GET|POST  /create-account          rodauth.create_account_path
  GET|POST  /verify-account-resend   rodauth.verify_account_resend_path
      
  GET|POST  /verify-account          rodauth.verify_account_path
  GET|POST  /logout                  rodauth.logout_path
  GET|POST  /remember                rodauth.remember_path
  GET|POST  /reset-password-request  rodauth.reset_password_request_path
  GET|POST  /reset-password          rodauth.reset_password_path
  GET|POST  /change-password         rodauth.change_password_path
  GET|POST  /change-login            rodauth.change_login_path
  GET|POST  /verify-login-change     rodauth.verify_login_change_path
  GET|POST  /close-account           rodauth.close_account_path
</table>


## How to run

```shell
$ bundle config set --local path 'vendor/bundle'
$ bundle install
$ yarn
```


## How to develop

```shell
$ rails new rails-rodauth-mfa --skip-active-storage --javascript=esbuild --skip-bundle
```

Gemfile に追加。まず Rails を整え。

```shell
$ bundle exec vite install

$ rails turbo:install
       apply  /home/hori/src_local/rails-rodauth-mfa/vendor/bundle/ruby/3.3.0/gems/turbo-rails-2.0.11/lib/install/turbo_with_node.rb
  You must import @hotwired/turbo-rails in your JavaScript entrypoint file

$ rails stimulus:install
       apply  /home/hori/src_local/rails-rodauth-mfa/vendor/bundle/ruby/3.3.0/gems/stimulus-rails-1.3.4/lib/install/stimulus_with_node.rb
  Create controllers directory
      create    app/javascript/controllers
(中略)
  Couldn't find "app/javascript/application.js".
You must import "./controllers" in your JavaScript entrypoint file
```

Rodauth 関係をインストール

```shell
$ rails g rodauth:install
      create  db/migrate/20250201045122_create_rodauth.rb
      create  config/initializers/rodauth.rb
      create  app/misc/rodauth_app.rb
      create  app/misc/rodauth_main.rb
     gemfile  sequel-activerecord_connection (~> 2.0)
     gemfile  bcrypt (~> 3.1)
     gemfile  tilt (~> 2.4)
      create  app/controllers/rodauth_controller.rb
      create  app/models/account.rb
      create  test/fixtures/accounts.yml
```

2FA関係

```shell
$ rails g rodauth:migration otp
      create  db/migrate/20250201045426_create_rodauth_otp.rb

$ rails g rodauth:migration recovery_codes
      create  db/migrate/20250201081215_create_rodauth_recovery_codes.rb
```

View は、デフォルトのママでは見た目が微妙。
HTMLテンプレートをコピーさせる

```shell
$ rails g rodauth:views
```

