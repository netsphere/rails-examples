
●未了
 ユーザが認可しなかった scope を得るには、追加で問い合わせが必要
https://developers.facebook.com/docs/facebook-login/guides/permissions/handle-declined



# facebook-login

Facebook ログイン, グラフAPI は, 'fb_graph2' パッケージが有力だった。
しかし, 依存する rack-oauth2 v2 の破壊的変更で動かなくなっている。さらに, fb_graph2 が直接依存する httpclient も古すぎて, もはや使えない。

類似の Koala パッケージはメンテナンスが継続している。こちらを使った、ログイン、グラフAPI サンプル。



## 使い方

<img src="facebook-login-and-consent.png" width="320" />

<img src="facebook-authzed-scopes.png" width="320" />

email の認可を外すことができる。この場合、userinfo でメィルアドレスが取れない。



## Facebook ドキュメント

Facebook はドキュメントがグチャグチャで, 相互に異なったことが書いていることも多い。

一応, ここの code flow が動く; <a href="https://developers.facebook.com/docs/facebook-login/guides/advanced/manual-flow">ログインフローを手動で構築する</a>

Scope 一覧はここ; <a href="https://developers.facebook.com/docs/permissions">Permissions Reference for Meta Technologies APIs</a> これはフィールド名とは異なる.

OpenID Connect の設定もあるが,
https://www.facebook.com/.well-known/openid-configuration/ 

こちらでは, 次のように `"code"` は定められていない。
```json
"response_types_supported": [
    "id_token",
    "token id_token"
],
```
    


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
