
=begin
ユーザの認可があれば、いろいろ取れる
その一方で, `photos`, `languages` などが単に返ってきていない。パーミション不足か?
`location` にタイムゾーンがない。ちょっと微妙。
   -> `timezone` フィールド廃止. `locale` も廃止
{
  "id"=>"1XXXXXX3",   数字
  "first_name"=>"久",
  "last_name"=>"堀川",
  "picture"=>{
    "data"=>{
      "height"=>50,
      "is_silhouette"=>false,
      "url"=>"https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=XXXXXXXXXXXX",
      "width"=>50
    }
  },
  # ここ以降は、ユーザ認可が必要
  "email"=>"hisashi.horikawa@gmail.com",
  "gender"=>"male",
  "birthday"=>"01/01/2005",
  "age_range"=>{
    "min"=>21
  },
  "friends"=>{
    "data"=>[],   さすがにこれは空
    "summary"=>{
      "total_count"=>5
    }
  },
  "hometown"=>{
    "id"=>"1XXXXXX6",     数字
    "name"=>"XX県 うどん市"
  },
  "location"=>{
    "id"=>"1XXXXXX9",    数字
    "name"=>"キラキラ市"
  }
}
=end


# ユーザ認証
class AccountController < ApplicationController
  # GET
  def sign_in
  end

  def show
    @graph ||= Koala::Facebook::API.new(session['access_token'])

    # Node type `User` フィールド:
    # https://developers.facebook.com/docs/graph-api/reference/user
    # birthday [String] = "MM/DD/YYYY", または "MM/DD"
    
    @me = @graph.get_object("me",
                            {fields: 'id,first_name,last_name,picture' +
                                     ',photos,email,gender,birthday,age_range' + 
                                     ',friends,hometown,location,languages'} )
    # `user_age_range` (#100) Tried accessing nonexisting field (user_age_range) on node type (User)
  end
end
