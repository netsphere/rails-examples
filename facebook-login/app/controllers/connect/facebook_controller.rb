

class Connect::FacebookController < ApplicationController
  def self.fb_config
    return @fb_config if @fb_config

    @fb_config = YAML.load_file(Rails.root.to_s + "/config/connect/facebook.yml",
                             aliases:true)[Rails.env].symbolize_keys
    if Rails.env.production?
      @fb_config.merge!(
        app_id:     ENV['fb_app_id'],
        app_secret: ENV['fb_app_secret']
      )
    end

    return @fb_config
  end

  
  # POST
  def create
    fb_config = Connect::FacebookController.fb_config()
    oauth = Koala::Facebook::OAuth.new( fb_config[:app_id], fb_config[:app_secret],
                                        fb_config[:redirect_uri] )
    # 必須! なくてもエラーにならない.
    session[:state] = SecureRandom.hex(32)

    # デフォルト: https://www.facebook.com/dialog/oauth?client_id=...&redirect_uri=...
    # `state` を発生していない! -> 明示しないといけない. 危ない!
    # Node type `User`:
    #    パーミション: `public_profile` .. デフォルトで付いている
    #       fields: id, first_name, last_name, middle_name, name, name_format,
    #               picture, short_name
    url = oauth.url_for_oauth_code(state: session[:state],
                                   permissions: fb_config[:scope] )
    redirect_to url, allow_other_host:true
  end

  # callback
  def show
    fb_config = Connect::FacebookController.fb_config()
    oauth = Koala::Facebook::OAuth.new( fb_config[:app_id], fb_config[:app_secret],
                                        fb_config[:redirect_uri] )
    # Koala は `state` の検証をしていない。自分ですること!
    if session[:state] != params[:state]
      raise AuthenticationRequired.new
    end
    session.delete(:state)
      
    # `get_access_token()` は不十分。`get_access_token_info()` を使え.
    ac_token = oauth.get_access_token_info(params[:code])
    session['access_token'] = ac_token["access_token"]
    
    redirect_to "/account"
  end
end

