
Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  namespace :connect do
    resource :facebook, controller:'facebook', only:['create', 'show']
  end
  
  # 単数形は `controller:` でクラスを指定.
  resource :account, controller:'account' do
    # Sorcery と名前が被る。`login`, `logout` は避ける
    get 'sign_in'
    post 'sign_out'
  end

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "welcome#index"
end
