
# こんなピンポイントのライブラリが標準になっている

require "tsort"
require_relative "tsort-ve"

v1 = Vertex.new(1); v2 = Vertex.new(2); v3 = Vertex.new(3); v4 = Vertex.new(4)
v1.children = [Edge.new(v1, v2)] #, Edge.new(v1, v3)]
v2.children = [Edge.new(v2, v3), Edge.new(v2, v4)]
v3.children = [Edge.new(v3, v2)] #, Edge.new(v3, v4)]
v4.children = []

graph = [v1, v2, v3, v4]

#=> TSort::Cyclic 例外
p TSort.tsort( (-> &b { graph.each(&b) }),
              -> n, &b { #p n.inspect
        n.children.each do |e|
          b.call(e.to)
        end } )

