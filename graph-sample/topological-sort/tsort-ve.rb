
# 頂点
# 頂点同士の比較は `#eql?` と `#hash` が使われる.
class Vertex
  attr_accessor :id
  attr_accessor :children

  def initialize id, children = []
    @id = id
    @children = children
  end
end

# 辺の始点と終点の呼び方はさまざまな流派がある
# https://math.stackexchange.com/questions/3857017/what-are-the-standard-names-for-the-node-a-directed-edge-comes-out-of-and-goes-i
#
#   始点     終点
# --------- ------------ ------------------------------
# source    target       boost, Mathworks
# tail      head         Wikipedia
# origin    destination  レア?
# from_node to_node      from, to だけでは衝突しやすいか
# start     end          ややマイナ


# 辺
class Edge
  attr_accessor :from
  attr_accessor :to

  def initialize from, to
    @from = from; @to = to
  end
end

