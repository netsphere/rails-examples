
# graph-sample

SQL `WITH RECURSIVE` sample.



## ActiveRecord libraries
    
 - ▲ activerecord-hierarchical_query
    https://github.com/take-five/activerecord-hierarchical_query/  - Simple DSL for creating recursive queries with ActiveRecord
   2023.Oct v1.4.5
   `CycleDetector` クラスがある
   Naive tree is assumed, not graph.

 - ▲ typed_dag
    https://github.com/opf/typed_dag/ - Rails models as directed acyclic graphs (DAG) with typed edges
   gem を足すと rails が起動しない。モデルを追加できず、ニワトリ卵。アカン

 - ▲ acts-as-dag (kebab-case)
    https://github.com/resgraph/acts-as-dag/  - Directed Acyclic Graph hierarchy for Rail's ActiveRecord models
   最終が 2014年。activerecord >= 4.0, < 5.0 プロジェクト終了。

 - ▲ acts_as_graph_diagram
  https://github.com/routeflags/acts_as_graph_diagram/ - Extends Active Record to add simple function for draw the Force Directed Graph with html.
  クラス名が決め打ちっぽい。
  


## Ruby libraries

 - rgl
    https://github.com/monora/rgl/  - RGL is a framework for graph data structures and algorithms in Ruby.
   `add_edge()` ではサイクルするものも追加可能。`#cycles` でサイクルがあるか確認。こちらのほうがパフォーマンスいいか。


 - dag
    https://github.com/kevinrutherford/dag/ - Directed acyclic graphs for Ruby
    `add_edge()` 内で `has_path_to?()` でサイクル確認
    
    
 - ▲ dagwood
    https://github.com/rewindio/dagwood/  - For all your dependency graph needs
   内部で tsort を利用。ごく簡単なライブラリ。機能不足
   

 - gratr19
    https://github.com/amalagaura/gratr/  - Import of GRATR (GRAph Theory in Ruby) (version 0.5.1; svn rev 38; 20080616) with some minor fixes for ruby 1.9.
   `shortest_path()`, `dijkstras_algorithm()` など。
   
   
 - ▲ https://github.com/SciRuby/networkx.rb/ - A Ruby implementation of Python's well known Graph library "networkx"
  Python の移植。まだまだこれから。
  


