
# See http://blog.livedoor.jp/i_am_best/archives/8754733.html
#     SQL でグラフの最短経路は見つけられない?!
# SQL で高速に経路を見つける.

# 以下の例では, 手動でサイクルを判定して `isCycle` 列に入れている。
# 再帰行の `WHERE` 句で `isCycle` チェックが必要.
# `CYCLE` 句 (PostgreSQL 14) を使えば, 自動的に `isCycle_auto` 列, `path_edges`
# 列が追加される。

class VerticesController < ApplicationController
  before_action :set_vertex, only: %i[ show update destroy ]

  # GET /vertices
  def index
    from_id = Vertex.find_by_name("0").id
    to_id =   Vertex.find_by_name(params[:to]).id
    # この例ではサイクル (閉路) があるので、そのチェックが必要。
    sql = <<EOS
-- 再帰する
WITH RECURSIVE  Dijkstra (target_id, distance, isCycle, cost, path)
AS (
  -- 非再帰行
  SELECT DISTINCT source_id, 0, FALSE, 0, ARRAY[#{from_id}]
      FROM edges 
      WHERE source_id = #{from_id} 
  UNION ALL
  -- 再帰行. isCycle チェックがないと無限ループする
  SELECT edges.target_id,
         sources.distance + 1,
         edges.target_id = ANY(path),
         sources.cost + edges.weight,
         sources.path || edges.target_id
      FROM edges , Dijkstra AS sources 
      WHERE sources.target_id = edges.source_id -- AND NOT isCycle
  ) CYCLE target_id SET isCycle_auto USING path_edges
,

LowestCost (cost) 
AS (
  SELECT MIN(cost) 
      FROM Dijkstra 
      WHERE target_id = #{to_id})

SELECT #{from_id} AS source_id, target_id, distance, D.cost, path   
    FROM Dijkstra AS D,
       LowestCost AS L 
    WHERE D.cost = L.cost AND target_id = #{to_id};
EOS
    res = ActiveRecord::Base.connection.select_all(sql)
    #@vertices = Vertex.all

    render json: res
  end

  # GET /vertices/1
  def show
    render json: @vertex
  end

  # POST /vertices
  def create
    @vertex = Vertex.new(vertex_params)

    if @vertex.save
      render json: @vertex, status: :created, location: @vertex
    else
      render json: @vertex.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vertices/1
  def update
    if @vertex.update(vertex_params)
      render json: @vertex
    else
      render json: @vertex.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vertices/1
  def destroy
    @vertex.destroy!
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vertex
      @vertex = Vertex.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def vertex_params
      params.fetch(:vertex, {})
    end
end
