require "test_helper"

class VerticesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vertex = vertices(:one)
  end

  test "should get index" do
    get vertices_url, as: :json
    assert_response :success
  end

  test "should create vertex" do
    assert_difference("Vertex.count") do
      post vertices_url, params: { vertex: {} }, as: :json
    end

    assert_response :created
  end

  test "should show vertex" do
    get vertex_url(@vertex), as: :json
    assert_response :success
  end

  test "should update vertex" do
    patch vertex_url(@vertex), params: { vertex: {} }, as: :json
    assert_response :success
  end

  test "should destroy vertex" do
    assert_difference("Vertex.count", -1) do
      delete vertex_url(@vertex), as: :json
    end

    assert_response :no_content
  end
end
