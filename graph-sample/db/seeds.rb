# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end


=begin
設問
  https://algo-method.com/tasks/1645CQpP
  この例では, サイクル (閉路) がある。
想定解答  
0
26
31
68
10
=end

File.open("db/data1") do |fp|
  N, M = fp.gets.split(" ").map do |x| x.to_i end
  (0 .. N - 1).each do |n|
    Vertex.create!(name:n.to_s)
  end
  (1 .. M).each do
    u, v, w = fp.gets.split(" ").map do |x| x.to_i end
    Edge.create!(source_id:Vertex.find_by_name(u.to_s).id,
                 target_id:Vertex.find_by_name(v.to_s).id,
                 weight:w)
  end
end
