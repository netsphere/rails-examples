
class CreateVertices < ActiveRecord::Migration[8.0]
  def change
    # Vertex 頂点
    create_table :vertices do |t|
      t.string :name, null:false
      
      t.timestamps
    end
  end
end
