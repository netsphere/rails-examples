
# 用語 https://boostjp.github.io/archive/boost_docs/libs/graph/graph_theory_review.html

class CreateEdges < ActiveRecord::Migration[8.0]
  def change
    # Edge 辺
    create_table :edges do |t|
      # directed graph 有向グラフ
      
      # Source 始点 
      t.references :source, type: :integer, null:false, foreign_key:{to_table: 'vertices'}
      # Target 終点
      t.references :target, type: :integer, null:false, foreign_key:{to_table: 'vertices'}
      
      t.integer :weight, null:false
      
      t.timestamps
    end
    add_index :edges, [:source_id, :target_id], unique:true
  end
end
