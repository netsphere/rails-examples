require "test_helper"

class VatCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vat_category = vat_categories(:one)
  end

  test "should get index" do
    get vat_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_vat_category_url
    assert_response :success
  end

  test "should create vat_category" do
    assert_difference("VatCategory.count") do
      post vat_categories_url, params: { vat_category: {  } }
    end

    assert_redirected_to vat_category_url(VatCategory.last)
  end

  test "should show vat_category" do
    get vat_category_url(@vat_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_vat_category_url(@vat_category)
    assert_response :success
  end

  test "should update vat_category" do
    patch vat_category_url(@vat_category), params: { vat_category: {  } }
    assert_redirected_to vat_category_url(@vat_category)
  end

  test "should destroy vat_category" do
    assert_difference("VatCategory.count", -1) do
      delete vat_category_url(@vat_category)
    end

    assert_redirected_to vat_categories_url
  end
end
