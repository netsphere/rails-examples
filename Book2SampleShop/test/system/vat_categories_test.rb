require "application_system_test_case"

class VatCategoriesTest < ApplicationSystemTestCase
  setup do
    @vat_category = vat_categories(:one)
  end

  test "visiting the index" do
    visit vat_categories_url
    assert_selector "h1", text: "Vat categories"
  end

  test "should create vat category" do
    visit vat_categories_url
    click_on "New vat category"

    click_on "Create Vat category"

    assert_text "Vat category was successfully created"
    click_on "Back"
  end

  test "should update Vat category" do
    visit vat_category_url(@vat_category)
    click_on "Edit this vat category", match: :first

    click_on "Update Vat category"

    assert_text "Vat category was successfully updated"
    click_on "Back"
  end

  test "should destroy Vat category" do
    visit vat_category_url(@vat_category)
    click_on "Destroy this vat category", match: :first

    assert_text "Vat category was successfully destroyed"
  end
end
