# -*- coding:utf-8 -*-

# View から呼び出せるメソッド群.
module ApplicationHelper

  # エラーメッセージを表示します。
  # errors() は ActiveModel::Validations
  def error_messages_for model_obj
    if model_obj.respond_to?(:errors)
      render(partial: "layouts/error_messages", locals:{model_obj:model_obj})
    end
  end
end
