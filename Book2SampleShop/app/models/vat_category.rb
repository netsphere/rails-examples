# -*- coding:utf-8 -*-

# VATカテゴリ
class VatCategory < ApplicationRecord
  has_many :products

  validate :validate_category


private
  
  # for validate()
  def validate_category
    if !%w(AE E S Z G O K L M).include?(category_code)
      errors.add(:category_code, "Invalid category")
    end
    if %w(E G O).include?(category_code) && vat_rate != 0.0
      errors.add(:vat_rate, "Must be zero")
    end
  end
end
