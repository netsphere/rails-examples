# -*- coding:utf-8 -*-

# ユーザ
class User < ApplicationRecord
  # Sorcery core モジュール
  # この呼び出しにより, サブモジュールの include や, init_orm_hooks! など.
  authenticates_with_sorcery!

  # 顧客情報
  has_one :customer
  validates_associated :customer

  # 非推奨 deprecated. 地雷だらけで, 使ってはダメ!
  # See https://techracho.bpsinc.jp/hachi8833/2019_11_05/82601#2-2
  #accepts_nested_attributes_for :customer
  
  has_many :cart_lines
  
  validates :password, length: {minimum: 6},
                       if: -> { new_record? || changes[:crypted_password] }
  # ActiveRecord の機能。フィールド名は対象のフィールド名に _confirmation を加
  # えたものになる。
  validates :password, confirmation: true,
                       if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true,
                       if: -> { new_record? || changes[:crypted_password] }

  # eメイルアドレスで識別.
  validates :email, presence:true
  validates :email, uniqueness: true

  validates :family_name, presence:true
  validates :given_name, presence:true
  
  before_validation :normalize

private
  
  # for before_validation()
  def normalize
    self.family_name = (family_name || "").unicode_normalize(:nfkc).strip
    self.given_name = (given_name || "").unicode_normalize(:nfkc).strip
  end  
end
