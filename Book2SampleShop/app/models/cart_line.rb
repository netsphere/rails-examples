# -*- coding:utf-8 -*-

# カートの1行
class CartLine < ApplicationRecord
  belongs_to :user
  # 商品のほうが親
  belongs_to :product
end
