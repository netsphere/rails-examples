# -*- coding:utf-8 -*-

# 受注
class SalesOrder < ApplicationRecord
  belongs_to :user

  has_many :so_details
end
