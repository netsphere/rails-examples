# -*- coding:utf-8 -*-

# 顧客情報
class Customer < ApplicationRecord
  # 1:1
  belongs_to :user

  validates :address, presence:true

  # Validation のほうが `before_save()` より先に実行される. なので, 
  # validation に影響ある正規化などは `before_save()` にしてはいけない.
  before_validation :normalize

private

  # for `before_validation()`
  def normalize
    self.address = (address || "").unicode_normalize(:nfkc).strip
  end
end
