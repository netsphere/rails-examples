# -*- coding:utf-8 -*-

require 'ostruct'
# From https://tfig.unece.org/contents/recommendation-20.htm
UOM = {
  "H87" => OpenStruct.new(code: "H87", name: "Piece",    name_ja: "個"),
  "KGM" => OpenStruct.new(code: "KGM", name: "Kilogram", name_ja: "kg"),
  "GRM" => OpenStruct.new(code: "GRM", name: "gram",     name_ja: "g"),
  "MTQ" => OpenStruct.new(code: "MTQ", name: "Cubic metre", name_ja: "立米"),
  "HUR" => OpenStruct.new(code: "HUR", name: "Hour",     name_ja: "時間"),
}


# 商品
class Product < ApplicationRecord
  has_many :cart_lines
  belongs_to :vat_category

  # Active Storage で画像添付
  has_many_attached :images
  
  validates :part_no, presence: true
  validates :part_no, uniqueness: true
  # 使える文字を制限する
  validates :part_no, format: { with: /\A[0-9A-Z]+(-[0-9A-Z]+)*\z/,
                                message:"英数字とハイフンのみが使えます" }
  
  validates :name, presence: true

  # この validation を掛けないと、数値のフィールドに数値以外を入れても通ってし
  # まい, しかも 0 になる。
  validates :price_with_vat, numericality: { greater_than: 0 }

  # いったん削除
  #validates :base_qty, numericality: { greater_than: 0 }

  # カスタムバリデーション
  validate :check_uom

  before_validation :normalize


  class << self
    def available_for_sale
      d = Date.today
      return where('release_date <= ? AND (end_of_sale_date IS NULL OR end_of_sale_date >= ?)', d, d)
    end
  end

  def available_for_sale?
    d = Date.today
    if !release_date || release_date > d || (end_of_sale_date && end_of_sale_date < d)
      return false
    end

    return true
  end

private

  # for before_validation()
  def normalize
    self.part_no = part_no.unicode_normalize(:nfkc).strip.upcase
    self.name = name.unicode_normalize(:nfkc).strip
    self.description = description.unicode_normalize(:nfc).strip
  end

  # for validate()
  def check_uom
    errors.add(:uom, "Invalid UoM code") if !UOM[uom]

    # 添付ファイルのファイル形式、大きさを確認
    # Direct uploads を使うかどうかに関わらず、ここに検証コードを書けばよい.
    # Direct uploads の場合は、先にファイルがアップロードされてからモデルを
    # 保存しようとするので、不正な内容であればファイルを消せばよい
    #
    # image/jpg は不正: https://stackoverflow.com/questions/33692835/is-the-mime-type-image-jpg-the-same-as-image-jpeg
    # active_storage_validations パッケージ https://rubygems.org/gems/active_storage_validations/ を使ってもよいが、大したことないので、手書きする
    if images.attached?
      images.each do |image|
        if image.blob.byte_size > 300_000
          image.purge
          errors.add(:images, 'ファイルは300Kバイト以内にしてください。')
        elsif !%w[image/jpg image/jpeg image/gif image/png image/svg+xml].include?(image.blob.content_type)
          image.purge
          errors.add(:images, 'JPEG, GIF, PNG SVG形式のみです。')
        end
      end
    end
  end

end
