
import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
    // ターゲット (操作対象の DOM) のプロパティ
    // this.xxxTarget でアクセスできる
    static targets = ["price"]

    // View 側の data-action 属性値で, アクション->JSクラス#メソッドを指定.
    handleVatCatChanged() {
        this.priceTarget.value = "123456";
    }

    handlePriceWithVatChanged() {
        this.priceTarget.value = "89002151";
    }
}
