// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails";
import * as bootstrap from "bootstrap"

// Stimulus
import "./controllers";

// direct upload
// Requiring via importmap-rails without bundling through the asset pipeline.
import * as ActiveStorage from "@rails/activestorage";
ActiveStorage.start();

// See config/importmap.rb
import "./direct_uploads.js";
