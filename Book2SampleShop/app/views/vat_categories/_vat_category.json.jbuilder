json.extract! vat_category, :id, :created_at, :updated_at
json.url vat_category_url(vat_category, format: :json)
