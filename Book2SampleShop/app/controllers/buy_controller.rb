# -*- coding:utf-8 -*-

class BuyController < ApplicationController
  # 異なるアクセスコントロールの場合は、コントローラを分けた方がポカしにくい.
  before_action :forbid_guest

  # GET /buy/payselect
  # 本当は、ここで支払い手段の表示
  def pay_select
    # このページに直接来れる。価格変更があったときは、カートに戻す
    CartLine.where('user_id = ?', current_user.id).each do |cart_line|
      if cart_line.product.price_with_vat != cart_line.cart_price_with_vat
        redirect_to "/cart", notice:"商品の価格変更がありました。"
        return
      end
    end
  end


  # 購入!
  def create
    sales_order = SalesOrder.new user_id: current_user.id,
                                 amount_total: 0,
                                 remarks: '',
                                 sell_day: Date.today
    # 明細行
    CartLine.where('user_id = ? AND buy_later IS FALSE', current_user.id).each_with_index do |cart_line, i|
      so_detail = SoDetail.new line_no: i + 1,
                    product_id: cart_line.product.id,
                    price_with_vat: cart_line.cart_price_with_vat, # きわどいタイミング回避
                    qty: cart_line.qty,
                    line_amount: cart_line.cart_price_with_vat * cart_line.qty,
                    vat_category_code: cart_line.product.vat_category.category_code,
                    vat_rate: cart_line.product.vat_category.vat_rate
      sales_order.so_details << so_detail
      sales_order.amount_total += so_detail.line_amount
    end

    ActiveRecord::Base.transaction do
      sales_order.save!
      CartLine.where('user_id = ? AND buy_later IS FALSE', current_user.id).delete_all
    end

    redirect_to "/", notice:"ご購入ありがとうございました!"
  end


end
