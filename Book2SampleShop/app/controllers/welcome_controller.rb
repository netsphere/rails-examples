class WelcomeController < ApplicationController

  # GET /
  def index
    @products = Product.available_for_sale
  end

end
