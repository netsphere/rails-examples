# -*- coding:utf-8 -*-

# ユーザアカウント
class AccountController < ApplicationController
  before_action :forbid_guest, only: %i[ show edit update destroy ]

  # GET /
  # 単数形リソースでは, index メソッドではなく show メソッド.
  def show
    @user = current_user
  end


  # POST /
  # Sign-up
  def create
    raise "internal error" if !current_user
    @user = current_user
    @user.assign_attributes user_params # まだ保存しない
    # accepts_nested_attributes_for を使ったサンプルが山のように見つかるが、全
    # 部ゴミ.
    # 1:1 の場合は、これが一番簡単.
    @customer = @user.build_customer customer_params
    @signin_default = false

    # ユーザがすでに存在していることがある。
    if User.find_by_email @user.email
      flash[:alert] = 'eメイルアドレスすでにあります。ログインしてください。'
      @signin_default = true
      render 'sign_in', status: :unprocessable_entity
      return
    end
    
    # TODO: 通常は、ここか後続で, メールアドレスの実在チェックが必要。

    begin
      ActiveRecord::Base.transaction do
        @user.role_guest = false
        @user.role_admin = false
        pw = @user.password; pw_c = @user.password_confirmation
        @user.save!  # customer も保存される
        @user.password = pw; @user.password_confirmation = pw_c
      end
    rescue ActiveRecord::RecordInvalid
      render 'sign_in', status: :unprocessable_entity
      return
    end

    # 流れでログインする. => 単に属性の更新なので、不要
    # カート情報の引き継ぎも不要.
    #login(@user.email, user_params[:password])
    #raise "internal error" if !current_user

    redirect_back_or_to "/", notice:'顧客情報を更新しました。'  
  end


  # GET /edit
  def edit
    @user = current_user
    @customer = @user.customer || Customer.new
  end


  # PUT /
  def update
    @user = current_user
    #@user.customer ||= Customer.new(user_id: @user.id)  # association= で保存が走ってしまう. しかも ActiveRecord::RecordNotSaved 例外 (RecordInvalid ではない)
    @customer = @user.customer || @user.build_customer(user_id: @user.id) # 保存されない
    begin
      ActiveRecord::Base.transaction do
        @customer.assign_attributes customer_params # まだ保存しない
        @user.update!(user_params)   # customer は保存されない!
        @customer.save!  # validation 済み
      end
    rescue ActiveRecord::RecordInvalid
      render 'edit', status: :unprocessable_entity
      return
    end

    redirect_to account_url, notice:"Account was successfully updated."
  end


  # GET /sign_in
  # POST /sign_in
  def sign_in
    if request.get?
      @user = User.new  # ここはブランク情報でよいので, current_user ではない.
      @signin_default = current_user && !current_user.role_guest
      return 
    end

    @user = User.new user_params
    @remember = params[:remember]
    @signin_default = true

    # ユーザの切り替えなど、ゲストユーザとは限らないことに注意.
    orig_user = current_user

    # remember_me サブモジュールを組み込んだ場合, 第3引数で記憶させるか選ぶ
    # login() から呼び出される auto_login() メソッド内で, 単に真偽値で見ている
    # Trap: check_box の値をそのまま渡すと、常に真.
    if !login(@user.email, @user.password,
              ActiveRecord::Type::Boolean.new.cast(@remember))
      # ここで current_user が nil になる. reset_session() されるので、単に
      # current_user に代入するだけではダメ.
      auto_login orig_user

      flash[:alert] = 'Login failed'
      # HTTP 422 Validation error. このstatus を付けないとエラー表示されない.
      render 'sign_in', status: :unprocessable_entity
      return
    end

    # ゲストユーザからのサインインのみ、データを引き継ぐ.
    if orig_user.role_guest
      ActiveRecord::Base.transaction do
        # 1. カート情報を merge
        CartLine.where('user_id = ?', orig_user).each do |orig_line|
          cart_line = CartLine.where('user_id = ? AND product_id = ?',
                                     current_user.id, orig_line.product.id).first
          if cart_line
            cart_line.buy_later = false if !orig_line.buy_later
            cart_line.qty += orig_line.qty
            cart_line.save!
          else
            orig_line.user_id = current_user.id
            orig_line.save!
          end
        end

        # 2. 元のゲストユーザは削除する
        orig_user.destroy!
      end # transaction
    end

    redirect_back_or_to "/", notice: 'Login successful'
  end


  # DELETE /sign_out  -- destroy_user_session
  def sign_out
    logout
    # これだけで、ゲストユーザでログインしなおす。
    redirect_to '/', notice: 'Logged out!'
  end


private
  # 次の形式でデータが来る
  # {"authenticity_token"=>"[FILTERED]",
  #  "user"=>{"email"=>"", "family_name"=>"", "given_name"=>"",
  #           "customer"=>{"address"=>""},
  #           "password"=>"[FILTERED]",
  #           "password_confirmation"=>"[FILTERED]"}}
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation,
                                 :family_name, :given_name)
  end
  
  def customer_params
    # id, user_id を含めてはいけない.
    params.require(:customer).permit(:address)
  end

end # class AccountController
