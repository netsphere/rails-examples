# -*- coding:utf-8 -*-

# 顧客情報
class CustomersController < ApplicationController
  # 管理者限定
  before_action :require_admin
  before_action :set_customer, only: %i[ show edit update destroy ]

  # GET /customers or /customers.json
  def index
●●    @users = User.where customer <> nil Customer.all
    @customers = Customer.all
  end

  # GET /customers/1 or /customers/1.json
  def show
  end

=begin
  # GET /customers/new
  def new
    @customer = Customer.new
  end
=end
  
  # GET /customers/1/edit
  def edit
  end


  # PUT /customers/1 or /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to customer_url(@customer), notice: "Customer was successfully updated." }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1 or /customers/1.json
  def destroy
    @customer.destroy

    respond_to do |format|
      format.html { redirect_to customers_url, notice: "Customer was successfully destroyed." }
      format.json { head :no_content }
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_customer
    @customer = Customer.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def customer_params
    params.fetch(:customer, {})
  end
end
