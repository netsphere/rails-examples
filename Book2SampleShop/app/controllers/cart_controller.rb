# -*- coding:utf-8 -*-

# カート
class CartController < ApplicationController
  before_action :set_cart_line, only: %i[remove_item set_buy_later move_to_cart]

  def show
    @buy_laters = []
    @cart_lines = []
    CartLine.where('user_id = ?', current_user.id).each do |cart_line|
      if cart_line.buy_later
        @buy_laters << cart_line
      else
        @cart_lines << cart_line
      end

      # ここで価格を更新する
      if cart_line.product.price_with_vat != cart_line.cart_price_with_vat
        cart_line.cart_price_with_vat = cart_line.product.price_with_vat
        cart_line.save!
        # アナウンス文をつくる
      end
    end
  end

  # DELETE /cart/remove_item/:item_id
  def remove_item
    @product = @cart_line.product
    @cart_line.destroy
    render 
  end

  # あとで買う
  def set_buy_later
    @cart_line.buy_later = true
    @cart_line.save!
    render
  end

  # カートに戻す
  def move_to_cart
    @cart_line.buy_later = false
    @cart_line.save!
    render
  end

private

  def set_cart_line
    @cart_line = CartLine.where('user_id = ? AND id = ?',
                                current_user.id, params[:cart_line_id]).first
  end
end
