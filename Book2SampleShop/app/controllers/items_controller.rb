# -*- coding:utf-8 -*-

# 顧客用の商品表示
class ItemsController < ApplicationController
  before_action :set_product, only: %i[ show add_to_cart ]

  def show
    # length() はレコード取得してしまう。count() で SQL COUNT を利用.
    @cart_count = CartLine.where('user_id = ?', current_user.id).count
    # first() では勝手に ORDER BY が付いてしまう. take() を使え.
    r = CartLine.where('user_id = ?', current_user.id).select("sum(qty) AS qty_count").take
    @cart_qty_count = r ? r.qty_count : 0
  end


  # POST /items/:item_id/add_to_cart
  # カートに追加
  def add_to_cart
    cart_line = CartLine.where('user_id = ? AND product_id = ?',
                               current_user.id, @product.id).first ||
                CartLine.new( user_id: current_user.id,
                              product_id: @product.id, qty: 0, buy_later:false )
    cart_line.cart_price_with_vat = @product.price_with_vat
    if cart_line.buy_later
      cart_line.buy_later = false
      cart_line.qty = params[:qty].to_i
    else
      cart_line.qty += params[:qty].to_i   # TODO: すでにカートにあります、と表示するほうがよい.
    end
    cart_line.save!

    # レスポンスでリダイレクトする場合もあるので, Turbo Frames は使えない.
    # Turbo Frames では, リダイレクトが window ではなく frame に適用される.
    if params[:add_item]
      # `add_to_cart.turbo_stream.erb` を描画する。
      # content-type: text/vnd.turbo-stream.html
      render 
    elsif params[:buy_now]
      redirect_to '/cart'
    else
      raise "internal error"
    end
  end


private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

end
