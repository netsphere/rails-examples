# -*- coding:utf-8 -*-

# 製品/商品 (管理画面)
class ProductsController < ApplicationController
  before_action :require_admin
  before_action :set_product,
                only: %i[ show edit update destroy attach_images remove_images ]

  # GET /products or /products.json
  # 管理用画面
  def index
    @products = Product.all
  end

  # GET /products/1 or /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
    @vat_categories = VatCategory.all
  end

  # GET /products/1/edit
  def edit
    @vat_categories = VatCategory.all
  end

  # POST /products or /products.json
  def create
    @product = Product.new(product_params)
    @vat_categories = VatCategory.all
    begin
      @product.save!
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity 
      return
    end

    redirect_to product_url(@product), notice:"Product was successfully created."
  end


  # PATCH/PUT /products/1 or /products/1.json
  def update
    @vat_categories = VatCategory.all
    begin
      # アタッチされた画像があるとき、追加ではなく、差し替えになる.
      # -> 特に複数画像のときは、別に追加, 削除機能を設けた方がよい.
      @product.update!(product_params)
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity
      return
    end

    redirect_to product_url(@product), notice:"Product was successfully updated."
  end

  # DELETE /products/1 or /products/1.json
  def destroy
    @product.destroy

    # Trap. status: :see_other が必要.
    redirect_to products_url, notice: "Product was successfully destroyed.",
                              status: :see_other
  end


  # POST attach_images: 画像の追加
  def attach_images
    begin
      ActiveRecord::Base.transaction do
        # 追加は, 複数を一撃でできる
        # ActiveStorage::Attached::Many#attach() は例外を投げない. おっと!
        @product.images.attach params[:images]
        raise ActiveRecord::RecordInvalid if !@product.valid?
      end
    rescue ActiveRecord::RecordInvalid
      # See turbo-rails パッケージ.
      render turbo_stream: turbo_stream.replace('attach_error', "<div id='attach_error'>添付エラーです!!</div>")
      return
    end

    redirect_to product_url(@product), notice:"画像を添付しました。"
  end

  def remove_images
    @removed_images = []
    ActiveRecord::Base.transaction do
      if params[:image_ids]
        params[:image_ids].each do |image_id|
          image = @product.images.find(image_id)
          @removed_images << image
          image.purge
        end
      end
    end
    # Turbo Streams で画像を消す
    render
  end


private
  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def product_params
    # permit() は, 配列での受け取りについては hash で指定.
    params.require(:product).permit(:part_no, :name, :description,
                                    :vat_category_id, :price_with_vat, :uom,
                                    :release_date, :end_of_sale_date,
                                    images:[])
  end
end
