# -*- coding:utf-8 -*-

# 各コントローラの基底クラス.
# - ActionController::Base クラスは Rails における web request のコア。
#   ActionController::Metal クラスから派生.
# - Metal は actionpack パッケージ, AbstractController::Base から派生.
class ApplicationController < ActionController::Base
  # このサンプルでは、問答無用でログインする
  before_action :require_login

  # remember_me: login_from_cookie() では有効期限が伸びない. 
  Sorcery::Controller::Config.after_remember_me << :after_login_from_cookie

private

  # callback
  def after_login_from_cookie user
    if user.role_guest
      # Rails.application.config.sorcery.configure の値はこうやって取れる
      t = User.sorcery_config.remember_me_for
      raise "internal error" if !(t > 0)
      user.remember_me_token_expires_at = Time.now.in_time_zone + t
      user.save!
    end
  end

  # for before_action
  def require_admin
    return if current_user.role_admin

    # redirect back を正常に動かすため, GET verb限定
    if request.get? && !request.xhr? && !request.format.json?
      session[:return_to_url] = request.url
    end

    redirect_to '/account/sign_in', alert:'管理者でログインしてください。'
  end


  # for before_action
  def forbid_guest
    return if current_user && !current_user.role_guest

    if request.get? && !request.xhr? && !request.format.json?
      session[:return_to_url] = request.url
    end

    redirect_to '/account/sign_in', alert:'ユーザ登録またはログインしてください。'
  end

  
  # 未ログインの状態で要ログインのページにアクセスした時に呼び出される
  def not_authenticated
    # ゲストログイン機能
    loginid = SecureRandom.alphanumeric(10) # 10文字
    pw = SecureRandom.alphanumeric(10)
    guest_user = User.create!(
                   email: loginid + "@guest.invalid",
                   family_name: 'Guest',
                   given_name: loginid,
                   role_guest:true, role_admin:false,
                   password: pw, password_confirmation: pw)
    # 流れで login する
    auto_login(guest_user, true)  # 強制的に remember me する
    # 元の画面に戻る
    redirect_back_or_to "/",
                        notice: "#{guest_user.family_name} #{guest_user.given_name} ようこそ!"
  end  
end
