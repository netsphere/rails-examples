# -*- coding:utf-8 -*-

# VATカテゴリ
class VatCategoriesController < ApplicationController
  # 管理者限定
  before_action :require_admin

  before_action :set_vat_category, only: %i[ show edit update destroy ]


  # GET /vat_categories or /vat_categories.json
  def index
    @vat_categories = VatCategory.all
  end

  # GET /vat_categories/1 or /vat_categories/1.json
  def show
  end

  # GET /vat_categories/new
  def new
    @vat_category = VatCategory.new
  end

  # GET /vat_categories/1/edit
  def edit
  end

  # POST /vat_categories or /vat_categories.json
  def create
    @vat_category = VatCategory.new(vat_category_params)

    if @vat_category.save
      redirect_to vat_category_url(@vat_category), notice: "Vat category was successfully created."
    else
      render :new, status: :unprocessable_entity 
    end
  end

  # PATCH/PUT /vat_categories/1 or /vat_categories/1.json
  def update
    if @vat_category.update(vat_category_params)
      redirect_to vat_category_url(@vat_category), notice: "Vat category was successfully updated."
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  # DELETE /vat_categories/1 or /vat_categories/1.json
  def destroy
    @vat_category.destroy!

    redirect_to vat_categories_url, notice: "Vat category was successfully destroyed."
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_vat_category
    @vat_category = VatCategory.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def vat_category_params
    params.fetch(:vat_category, {})
  end
end
