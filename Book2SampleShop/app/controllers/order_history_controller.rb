# -*- coding:utf-8 -*-

# 顧客用の注文履歴
class OrderHistoryController < ApplicationController
  before_action :forbid_guest

  before_action :set_sales_order, only: %i[ show edit update destroy ]
  
  def index
    @sales_orders = SalesOrder.where('user_id = ?', current_user.id)
  end

  def show
  end


private
  # Use callbacks to share common setup or constraints between actions.
  def set_sales_order
    @sales_order = SalesOrder.where('user_id = ? AND id = ?',
                                    current_user.id, params[:id]).first
  end
  
end
