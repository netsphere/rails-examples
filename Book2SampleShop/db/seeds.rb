# -*- coding:utf-8 -*-

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

password = '123456' # ● TODO: 更新すること。

ActiveRecord::Base.transaction do 
  admin = User.create!(email:'hori@localhost',
                    family_name:'山田', given_name:'花子',
                    role_guest:false, role_admin:true,
                    password:password, password_confirmation:password)

  vat1 = VatCategory.create!(name: "ほかに該当しない商品",
                           description: "", 
                           category_code: "S", vat_rate: 10.0 )
  vat1 = VatCategory.create!(name: "酒類",
                           description: "",
                           category_code: "S", vat_rate: 10.0 )
  vat1 = VatCategory.create!(name: "飲食料品 (酒類を除く)",
                           description: "",
                           category_code: "S", vat_rate: 8.0 )
  vat1 = VatCategory.create!(name: "身体障害者用物品",
                           description: "車椅子など",
                           category_code: "E", vat_rate: 0.0 )
  vat1 = VatCategory.create!(name: "プリペイドカードの発行",
                           description: "",
                           category_code: "O", vat_rate: 0.0 )
                           
end

