# -*- coding:utf-8 -*-

# 商品マスタ
class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products do |t|
      # 商品コード
      t.string :part_no,     null:false, index:{unique:true}
      t.string :name,        limit:100, null:false
 
      t.string :description, null:false

      t.date :release_date
      t.date :end_of_sale_date # 終売

      # MySQL の BLOB 型は高々 65,536 バイト. limit: でそれを超える長さを指定.
      # -> Active Storage を使う
      #t.binary :photo, limit: 700_000

      # 価格の持ちかた ###########
      # B2C
      #   日本: 平15(2003年)改正法: 総額表示の導入. 2021.4.1から義務化
      #         税込みで合計してから、消費税額を算出.
      #   EU: 内税表示. 例えば https://eumag.jp/feature/b0515/
      #   シンガポールも内税.
      # B2B
      #   Full VAT invoice は, 1行ごとに「税抜価格」の表示, 通貨単位への
      #   rounding (円なら小数点以下 0桁).
      #   かつ、税抜きの合計に消費税率を掛けて、税込金額を算出.
      # B2Bと B2C で要求が矛盾しており,
      #   - "B2B のみ" の場合以外は,「税込価格」or 税額もマスタで持つ必要.
      #     (税抜き + 税率) だけだと、セブンイレブン、イオンのように, 税込
      #     105.84円とか
      #   - B2BとB2C で、税込合計額を合わせることはできない.
      #     サイトを分ける必要. B2Cのほうには矛盾なく消費税を表示できない.

      # 消費税抜き価格: exclusive of VAT
      # 注意! 税込み->税抜きの丸めは必ず "四捨五入" でなければならない. でなけ
      # れば validation を通らない.
      #   例) 税込み10円 切り捨てで丸め -> 税抜き 10円, 税0円.
      # t.integer :net_price, null:false   ●●マスタで持つかは, 後で考える

      # VATカテゴリ. 課税/非課税, 税率などのグルーピング.
      t.belongs_to :vat_category, foreign_key:true, null:false

      # base_qty での価格 (税込み). 総額表示に必要。こちらをマスタとして持つ.
      # Simplified invoice ではこちらを利用.
      t.integer :price_with_vat, null:false

      # 通常は1ヶ.
      # 100g当たり価格表示で1g単位で量り売りだと 100.
      # -> 挙動が不必要に複雑になるので、いったん削除.
      #t.integer :base_qty, null:false

      # Unit of Measure
      # 省略時は "H87" Piece.
      t.string :uom, limit:3, null:false

      t.timestamps
    end
  end
end
