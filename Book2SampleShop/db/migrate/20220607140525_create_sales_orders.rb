# -*- coding:utf-8 -*-

# 受注オーダ
class CreateSalesOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :sales_orders do |t|
      # 親 1:多
      t.belongs_to :user, foreign_key:true, null:false

      # 総額 (税込金額)
      t.integer :amount_total, null:false

      t.string :remarks, null:false
 
      # 受注日
      t.date :sell_day, null:false

      t.timestamps
    end
  end
end
