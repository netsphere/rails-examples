# -*- coding:utf-8 -*-

class SorceryActivityLogging < ActiveRecord::Migration[7.0]
  def change
    # 'after_login' hook.
    # -> ログインしましたメールなどに使う.
    add_column :users, :last_login_at,     :datetime #, default: nil

    # 明示的にログアウトした場合のみ記録される. "before_logout" hook.
    add_column :users, :last_logout_at,    :datetime #, default: nil

    # 一つ一つのリクエストのときに更新される。ただし、logout では更新されない。
    # 'after_action' hook.
    add_column :users, :last_activity_at,  :datetime #, default: nil

    # 'after_login' hook. request.remote_ip 値が記録される.
    # -> ログインしましたメールなどに使う.
    add_column :users, :last_login_from_ip_address, :string #, default: nil

    # Trap. この書き方では複合キーに張る形になるとの解説があるが、誤り.
    # -> 一つ一つの列にインデックスが張られる.
    #add_index :users, [:last_logout_at, :last_activity_at]
  end
end
