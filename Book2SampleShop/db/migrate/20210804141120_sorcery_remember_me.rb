# -*- coding:utf-8 -*-

# remember_me サブモジュール
class SorceryRememberMe < ActiveRecord::Migration[6.1]
  def change
    # Trap. add_column ではインデックスを張れない.
    add_column :users, :remember_me_token, :string, limit:64 #, default: nil
    add_column :users, :remember_me_token_expires_at, :datetime #, default: nil

    add_index :users, :remember_me_token, unique:true
  end
end
