# -*- coding:utf-8 -*-

# カートの1行
class CreateCartLines < ActiveRecord::Migration[7.0]
  def change
    create_table :cart_lines do |t|
      # 親 1:多
      t.belongs_to :user, foreign_key:true, null:false

      # 商品コード
      t.belongs_to :product, foreign_key:true, null:false

      # カートに入れたときの単価
      # このサンプルでは扱わないが、カート内の商品の価格変更時の通知に必要.
      t.integer :cart_price_with_vat, null:false
      
      # 数量
      t.decimal :qty, precision:8, scale:2, null:false

      # あとで買う
      t.boolean :buy_later, null:false

      t.timestamps
    end

    # Trap. これでは一つ一つの列にインデックスが張られる。
    #add_index :cart_lines, [:user_id, :product_id], unique:true
    # 次のように, 名前を明記してやると, 複合キーにインデックスを張れる。
    # いくら何でもヒドい...
    add_index :cart_lines, [:user_id, :product_id],
              name: "cart_lines_user_id_product_id_idx", unique:true
  end
end
