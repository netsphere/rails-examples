# -*- coding:utf-8 -*-

# 自動生成されたモデル
# users 表
class SorceryCore < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      # フィールド名は config の email_attribute_name で変更可.
      # デフォルト 'email'
      # メイルアドレスは最長 254 文字.
      t.string :email,            null: false, index: { unique: true }

      # Google Identity Platform
      # https://accounts.google.com/.well-known/openid-configuration
      # "claims_supported": [
      #     "email", "email_verified", "family_name", "given_name",
      #     "locale", "name", "picture", ... 略

      # Azure AD
      # https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration
      # "claims_supported":[
      #     "preferred_username", "name", "email", ... 略
      # 'profile' で要求すれば, 次の応答.
      #     {
      #       "sub": "OLu859SGc2Sr9ZsqbkG-QbeLgJlb41KcdiPoLYNpSFA",
      #       "name": "Mikah Ollenburg", // names all require the “profile” scope.
      #       "family_name": " Ollenburg",
      #       "given_name": "Mikah",
      #       "picture": "https://graph.microsoft.com/v1.0/me/photo/$value",
      #       "email": "mikoll@contoso.com" //requires the “email” scope.
      #     }
      t.string :family_name, limit:40, null:false
      t.string :given_name,  limit:40, null:false

      t.boolean :role_guest, null:false
      t.boolean :role_admin, null:false

      # フィールド名は config の crypted_password_attribute_name で変更可.
      # デフォルト 'crypted_password'
      t.string :crypted_password, limit:64
      t.string :salt,             limit:64

      t.timestamps                null: false
    end

    #add_index :users, :email, unique: true
  end
end
