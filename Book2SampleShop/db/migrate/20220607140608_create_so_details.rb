# -*- coding:utf-8 -*-

# 受注オーダ - 明細行
class CreateSoDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :so_details do |t|
      # 親 1:多
      t.belongs_to :sales_order, foreign_key:true, null:false

      # 行番号. 1始まり
      t.integer :line_no, null:false

      # 商品コード
      t.belongs_to :product, foreign_key:true, null:false

      # B2C: 販売単価 (税込み)
      t.integer :price_with_vat, null:false
      #複雑になるため、いったん削除
      #t.integer :base_qty, null:false

      # 数量
      t.decimal :qty, precision:8, scale:2, null:false

      # (item price ÷ item price base qty) × qty (税込み)
      # 例: 商品マスタが100g当たりで1g単位の量り売り.
      t.integer :line_amount, null:false
 
      # 税率. 転記する.
      t.string :vat_category_code, limit:2, null:false
      t.decimal :vat_rate, precision:4, scale:2, null:false

      t.timestamps
    end

    ActiveRecord::Base.connection.execute <<EOS
CREATE UNIQUE INDEX ON so_details (sales_order_id, line_no);
CREATE UNIQUE INDEX ON so_details (sales_order_id, product_id);
EOS
  end
end
