# -*- coding:utf-8 -*-

# VAT カテゴリ
class CreateVatCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :vat_categories do |t|
      # 不課税 - 商品券・ギフト券・プリペイドカードの発行 [物品切手等の発行]
      #          See https://www.cs-acctg.com/column/kaikei_keiri/004594.html
      # 非課税 - 商品券・ギフト券・プリペイドカードの譲渡 (不課税ではない)
      #          身体障害者用物品
      # 軽減税率 Reduced rate - 酒類・外食を除く飲食料品
      # 標準税率 - 上記以外
      t.string :name, limit:100, null:false

      t.string :description, null:false

      # VATカテゴリ
      #   "S" 標準税率
      #       システム的には, Standard rate, Reduced rate, Super-reduced rate
      #       いずれも、このカテゴリを使う.
      #   "E" 非課税.  -- ゼロ税率 Zero rate "Z" とは意味が異なる.
      #   "O" 課税対象外 (不課税)  -- 非課税とは意味が異なる.
      # 相手先との関係で決まる輸出免税 "G", リバースチャージ "AE" は、ここには
      # 設定しない.
      # 例えば,
      #   進出先国で生じる間接税の諸問題と現地制度の基礎知識
      #   https://www.meti.go.jp/policy/external_economy/toshi/kokusaisozei/itaxseminar2021/05_kansetsuzeikiso.pdf
      t.string :category_code, limit:2, null:false

      # さすがに、小数点以下2桁あれば大丈夫だろう
      t.decimal :vat_rate, precision:4, scale:2, null:false

      t.timestamps
    end
  end
end
