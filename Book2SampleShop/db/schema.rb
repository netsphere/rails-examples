# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_06_07_140608) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "cart_lines", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "product_id", null: false
    t.integer "cart_price_with_vat", null: false
    t.decimal "qty", precision: 8, scale: 2, null: false
    t.boolean "buy_later", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_cart_lines_on_product_id"
    t.index ["user_id", "product_id"], name: "cart_lines_user_id_product_id_idx", unique: true
    t.index ["user_id"], name: "index_cart_lines_on_user_id"
  end

  create_table "customers", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "address", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_customers_on_user_id", unique: true
  end

  create_table "products", force: :cascade do |t|
    t.string "part_no", null: false
    t.string "name", limit: 100, null: false
    t.string "description", null: false
    t.date "release_date"
    t.date "end_of_sale_date"
    t.bigint "vat_category_id", null: false
    t.integer "price_with_vat", null: false
    t.string "uom", limit: 3, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["part_no"], name: "index_products_on_part_no", unique: true
    t.index ["vat_category_id"], name: "index_products_on_vat_category_id"
  end

  create_table "sales_orders", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "amount_total", null: false
    t.string "remarks", null: false
    t.date "sell_day", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sales_orders_on_user_id"
  end

  create_table "so_details", force: :cascade do |t|
    t.bigint "sales_order_id", null: false
    t.integer "line_no", null: false
    t.bigint "product_id", null: false
    t.integer "price_with_vat", null: false
    t.decimal "qty", precision: 8, scale: 2, null: false
    t.integer "line_amount", null: false
    t.string "vat_category_code", limit: 2, null: false
    t.decimal "vat_rate", precision: 4, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_so_details_on_product_id"
    t.index ["sales_order_id", "line_no"], name: "so_details_sales_order_id_line_no_idx", unique: true
    t.index ["sales_order_id", "product_id"], name: "so_details_sales_order_id_product_id_idx", unique: true
    t.index ["sales_order_id"], name: "index_so_details_on_sales_order_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "family_name", limit: 40, null: false
    t.string "given_name", limit: 40, null: false
    t.boolean "role_guest", null: false
    t.boolean "role_admin", null: false
    t.string "crypted_password", limit: 64
    t.string "salt", limit: 64
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "remember_me_token", limit: 64
    t.datetime "remember_me_token_expires_at", precision: nil
    t.string "reset_password_token", limit: 64
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.integer "access_count_to_reset_password_page", default: 0, null: false
    t.datetime "last_login_at"
    t.datetime "last_logout_at"
    t.datetime "last_activity_at"
    t.string "last_login_from_ip_address"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["remember_me_token"], name: "index_users_on_remember_me_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vat_categories", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "description", null: false
    t.string "category_code", limit: 2, null: false
    t.decimal "vat_rate", precision: 4, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "cart_lines", "products"
  add_foreign_key "cart_lines", "users"
  add_foreign_key "customers", "users"
  add_foreign_key "products", "vat_categories"
  add_foreign_key "sales_orders", "users"
  add_foreign_key "so_details", "products"
  add_foreign_key "so_details", "sales_orders"
end
