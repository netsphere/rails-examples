# ショップサンプル: Sorcery による認証, ゲストログイン, Active Storage ほか

未了:
 - Recommendation 機能の作り直し

 
## これは何か

私 (堀川) が2003年に上梓した『基礎から学ぶデータベースプログラミング』のサンプルプログラムを、現代の技術で更新した。

ごくごく簡単なショッピングカートのサンプルプログラムで、元のは 2003年, まだ Ruby on Rails も存在せず, CGI で作っていた時代のもの。
See https://www.nslabs.jp/book2.rhtml

20年も経って変わったといえば変わったが、本質的な部分は案外変わっていない。息の長い技術を学ぶメリットがある。

 - 当時, Perl + CGI が Ruby + CGIになった。CGIは、httpリクエストのたびにプロセスが起動され、標準出力にレスポンスHTMLを出力していた。パフォーマンスが非常に悪かったはずだが、これぐらいでやれていた。
 - DBMS は PostgreSQL, MySQL, SQLite. 今とぜんぜん変わらない。
 - Ruby on Rails がエポック。Rails v1.0 は 2005年12月リリース. 現代まで, 多くのフレームワークが Rails 流の MVC モデルを使っている。
 - http も, HTTP/2, HTTP/3 が登場したが, semantics に大きな変化はない。Stateless, application-level, request/response プロトコル.
 - Webブラウザの進化とフロントエンド開発を巡る状況は、この20年間で激変した。2006年8月 jQuery v1.0リリース。<i>Babel</i> の開始が 2014年ごろ? この間に大きく変わった.
 - サーバサイドも JavaScript で制作するようになった (生産性低く, お勧めしない)。この動向はどこからだろう? Express.js v3.0 (Sencha Connectベース) 2012年10月リリース. Express.js v4.0 2014年4月リリース.


使っているパッケージなどは、まったく変わった。
 - Apache HTTP Server は廃れた。
 - MySQL は, Oracle に買収されて, 廃れた。
 - Ruby のパッケージは、20年というスパンで見ると、ほとんど入れ替わった。Ruby/DBI -> ActiveRecord, Amrita -> ERB などなど。でも、やってることは変わっていない。

20年前のサンプルプログラムにはレコメンデーション機能まで付いていたが、実装があまりにも naive で、サンプルとしてもあまり適切ではない。
アイテムベースレコメンデーションを、次のステップで実装する。




## Topic: ゲストログイン

### 謎の風潮

<a href="https://ritou.hatenablog.com/entry/2021/02/01/070000">最近よく見かける初心者エンジニアが転職の際に作る今時のポートフォリオに必須だと噂の「ゲストログイン」について - r-weblife</a>

 - 転職にポートフォリオが求められるらしい
 - ポートフォリオを作成する際はゲストログイン機能が必須と言われています。へー?
 - <a href="https://qiita.com/nasuB7373/items/10bebd8e9f0b3331f348">ポートフォリオのアプリを公開したらイタズラされまくった話 - Qiita</a>  フォームのテキストコントロールで <code>readonly</code>属性値を付けるだけで、サーバ側でチェックしない、とか怖すぎる。レベルが低い。

[かんたんログイン] ボタンを押したらゲストとしてログイン、という挙動はダメだろう。と思ったら、ほかのスクールのポートフォリオ紹介でも [ゲストユーザーでログイン] ボタンがあるものがあり、どこぞのスクールでこんな変なやり方を教えているのか。


### 実装

一口にゲストログインといっても、求められる機能はアプリケィションの性質に応じて千差万別。大きくは, 次のいずれか:
 (1) デモユーザの意味で、事前に用意されたユーザとしてログイン。または,
 (2) ログインせずにある程度使えるようにするためのもの。

このサンプルでは、後者の意図で、次の組み合わせにする。
 - 未ログインのユーザに対して、自動的に, 別々のゲストユーザを発行する
 - 決済の直前で、新規登録かログインを求める
 - ゲストの状態からから新規登録/ログインした場合、カートの内容を引き継ぐ




## Topic: Sorcery

Rails での認証は, Devise がメジャーだが, 私はシンプルな Sorcery を勧めている。
Sorcery はサブモジュールを組み合わせる。

```shell
  $ rails g sorcery:install remember_me reset_password activity_logging
```


### reset_password の設定

ActionMailer を定義

```shell
  $ rails g mailer UserMailer reset_password_email
```

`config/initializers/sorcery.rb` ファイルで、次の設定

```ruby
    # REQUIRED:
    # Password reset mailer class. 必須! ActionMailer のクラスを指定.
    # Default: `nil`
    #
    user.reset_password_mailer = UserMailer

    # Reset password email method on your mailer class.
    # Default: `:reset_password_email`   この例はデフォルトどおり。
    #
    user.reset_password_email_method_name = :reset_password_email
```




## Topic: HOTWIRE (HTML Over The Wire)

Turbo Frames, Turbo Streams は名前は似ているが、別物。Turbo Frames はごく限定的な状況でのみフィットする。きちんと使い分ける。


### Turbo Frames

HTML `<iframe>` と似た挙動イメージ。サーバからの text/html レスポンスを特定の `<turbo-frame>` 内に表示する。

View の `<turbo-frame>` 内から link `<a>` か `<form>` などのリクエストを投げた場合、そのレスポンスは同じフレーム内に表示。リダイレクトもフレームのみが変わる (これが罠)。

レスポンスに `<turbo-frame>` がただ一つ必要。同じ `id` ブロックのみが置換される。レスポンスのうち `<turbo-frame>` の外側は、単に無視される。

`<turbo-frame>` タグの代わりに `turbo_frame_tag()` helper method を使ってもよい。

外側から, 特定の `<turbo-frame>` をターゲットにして、レスポンスで置換させることもできる。

すごい簡単。その分、トレードオフがある。
 - 当該 `<turbo-frame>` のみが置換される. レスポンスに複数の `<turbo-frame>` があっても, `id` が一致する一つだけが使われる.
 - `id` が一致する `<turbo-frame>` HTML 片がなければ、単に何も起きない. エラーが失われる.
 - View の側がターゲット frame を指定する作りで, サーバレスポンスが frame 外側にアクセスできない。なので, window リダイレクトもできない。違う, そうじゃない.


### Turbo Streams

`<turbo-frame>` 要素を使わずに, あらゆる HTML 要素に対して作用できる。
上記, Turbo Frames で要求を満たせないときは, Turbo Streams を使え.

Web には, Turbo Streams は `<turbo-frame>` と共に使うような誤認している解説ページも多い。


8つのアクションがある:
 - append
 - prepend
 - replace
 - update
 - remove
 - before
 - after
 - morph ※追加
 



## Topic: Active Storage

画像などをファイルとして保存する。Amazon S3, Google Cloud Storage, Microsoft Azure Storage などのサービスも使える。

設定は, まず `config/storage.yml` ファイルにてサービスに名前を付ける。例えば次は, ローカルディスクへの保存を `local` と名づける。

```yaml
local:
  service: Disk
  root: <%= Rails.root.join("storage") %>
```

次に, `config/environments/development.rb` ファイルで, どのサービスを使うか選択する。

```ruby
  # Store uploaded files on the local file system (see config/storage.yml for
  # options).
  config.active_storage.service = :local
```

Active Storage は, 3つのDBテーブルを利用する。`active_storage_blobs`, `active_storage_attachments`, `active_storage_variant_records`.

```shell
$ rails active_storage:install
Copied migration 20220601123446_create_active_storage_tables.active_storage.rb from active_storage
```


### 挙動

Redirect mode と proxy mode の二つのモードがある。
●●加筆すること。


Rails v7 でストリーミングが改良された。このサンプルでは扱わない。例えば, https://techracho.bpsinc.jp/hachi8833/2021_04_15/106801


### ファイルの認証

Rails v6.0 以前と v6.1以降で作りが異なる。Web上の古い記事は, そのままでは動かないので注意。

Rails v6.0 以前: class <code>ActiveStorage::BlobsController</code> のファイルをコピーしてきて、上書きする。`BlobsController` をゴニョゴニョしている解説は、すべて古い。

Rails v6.1 以降: <code>ActiveStorage::BlobsController</code> は単に廃止された。次のクラスのいずれかまたは複数を上書きする。
 - ActiveStorage::Blobs::RedirectController,
 - ActiveStorage::Blobs::ProxyController,
 - ActiveStorage::Representations::RedirectController
 - ActiveStorage::Representations::ProxyController

`Blobs` 以下が本体の画像。`Representations` 以下がプレビュー。プレビューも制限を掛けたい場合は、後者のいずれかも上書きすること。



