# -*- coding:utf-8 -*-

Rails.application.routes.draw do
  # VATカテゴリ (管理者のみ)
  resources :vat_categories

  # 受注
  resources :sales_orders

  # ユーザアカウント. 単数形リソース singleton resource を使う
  # 単数形リソースであってもデフォルトコントローラ名は複数形。
  # => controller: オプションでクラス名を指定する
  resource :account, only: [:show, :create, :edit, :update],
                     controller: 'account' do
    #get    'customer_sign_up'
    match 'sign_in', via:[:get, :post]
    delete 'sign_out'

    # /account/order_history/:id など.
    # ベストプラクティス: ネストは1回以下にすること。
    resources :order_history, only: %i[ index show ]
  end

  resource :cart, only:[:show], controller:'cart' do
    # `to:` オプションは "コントローラ#アクション" (コントローラ名が必須).
    # アクションだけを指定するときは `action:`
    post 'remove_item/:cart_line_id',  action:'remove_item'
    post 'set_buy_later/:cart_line_id',    action:'set_buy_later'
    post 'move_to_cart/:cart_line_id', action:'move_to_cart'
  end

  resource :buy, only:[:create], controller:'buy' do
    # redirect back のため, GET のページでロールを確認.
    get 'pay_select'
  end

  # 顧客情報 (管理者のみ)
  resources :customers

  # 製品/商品 (管理者のみ)
  resources :products do
    post 'attach_images', on: :member
    post 'remove_images', on: :member
  end

  # 顧客用の商品画面
  resources :items, only: [:show] do
    # Trap. on: :member を付けないと, ネストしたリソースと見なされ,
    # /items/:item_id/add_to_cart になってしまう. (:id でなくなる)
    post :add_to_cart, on: :member    
  end

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "welcome#index"
end
