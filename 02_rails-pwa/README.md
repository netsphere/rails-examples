
# PWA Chat - Rails 7.0 sample

PWA サンプル



## How To Run

`config/database.yml.sample` を `database.yml` にコピーし、適宜編集。

```shell
  $ bin/rails db:migrate
  $ yarn build
```



## 開発

`serviceworker-rails` gem はアセットパイプラインを前提としているようだ。importmap ではなく jsbundling-rails を使う.

Webpacker は Rails7 で単に廃止された。生の webpack を指定する。

```shell
  $ rails new pwa-chat --javascript=webpack
```

パタパタ作っていく。ひな形ができたら、各ライブラリをインストールする。

```shell
  $ bin/rails javascript:install:webpack
  $ bin/rails turbo:install
  $ bin/rails stimulus:install
```

この段階で、きちんと動くようにする。



### serviceworker-rails (Rails 5用) の導入

Rails 6, Rails 7と, エントリポイントが替わっているので、<kbd>touch</kbd> してからインストールする。

<pre>
$ <kbd>touch app/assets/javascripts/application.js</kbd>

$ <kbd>bin/rails g serviceworker:install</kbd>
      create  app/assets/javascripts/manifest.json.erb
      create  app/assets/javascripts/serviceworker.js.erb
      create  app/assets/javascripts/serviceworker-companion.js
      create  config/initializers/serviceworker.rb
      append  app/assets/javascripts/application.js
      append  config/initializers/assets.rb
      insert  app/views/layouts/application.html.erb
      create  public/offline.html
</pre>

Rails7 (jsbundling-rails): `/app/javascript/application.js` がエントリポイント。

Rails 6.1 (Webpacker) では `/app/javascript/packs/application.js` がエントリポイント。`/app/assets/javascripts/application.js` は恐らく Rails v3.1 から v5.x (Sprockets) までか。

`app/assets/javascripts/application.js` ファイルを削除し, `serviceworker-companion.js` の内容を `app/javascript/application.js` に追記 (移動) する。




## TOPIC: favicon (ファビコン)

結局、どの大きさのアイコンをどのように用意すればいいかは、次を参照.
歴史の積み重ねすぎで、一つにはできない。

<a href="https://techracho.bpsinc.jp/hachi8833/2024_02_09/108697">2024年のファビコンを極める: 本当に必要なファイルはほぼ6つ（翻訳)</a>

<a href="https://zenn.dev/pacchiy/articles/e4dcd7bd29d387">【2021年版】favicon（ファビコン）の設定方法と画像の作り方</a>

Favicon Generator は次がよい;
 - https://realfavicongenerator.net/

