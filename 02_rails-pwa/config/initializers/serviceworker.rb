# -*- coding:utf-8 -*-
# frozen_string_literal: true

Rails.application.configure do
  config.serviceworker.routes.draw do
    # map to assets implicitly
    match "/serviceworker.js"
    match "/manifest.json"

    # Examples
    #
    # map to a named asset explicitly
    # match "/proxied-serviceworker.js" => "nested/asset/serviceworker.js"
    # match "/nested/serviceworker.js" => "another/serviceworker.js"
    #
    # capture named path segments and interpolate to asset name
    # match "/captures/*segments/serviceworker.js" => "%{segments}/serviceworker.js"
    #
    # capture named parameter and interpolate to asset name
    # match "/parameter/:id/serviceworker.js" => "project/%{id}/serviceworker.js"
    #
    # insert custom headers
    # match "/header-serviceworker.js" => "another/serviceworker.js",
    #   headers: { "X-Resource-Header" => "A resource" }
    #
    # anonymous glob exposes `paths` variable for interpolation
    # match "/*/serviceworker.js" => "%{paths}/serviceworker.js"
  end

  # デフォルト値
  # config.serviceworker.icon_sizes = %w[36 48 60 72 76 96 120 152 180 192 512]
  
  # manifest.webmanifest ファイルは Android 用だけでよい. See https://developers.google.com/web/fundamentals/app-install-banners/native
  # iOS 用は <link rel="apple-touch-icon" href="..." /> (180px x 180px)
  config.serviceworker.icon_sizes = [192, 512] # "image/png"
end

