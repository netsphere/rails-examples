# -*- coding:utf-8 -*-

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Rails 7.0.1 以降: Ruby 3.1.0 も可.
ruby ">= 3.1.1"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
# Ruby 3.1.0 を使うときは Rails 7.0.1 以降. 
gem "rails", "~> 7.1.1"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use sqlite3 as the database for Active Record
gem "sqlite3", "~> 1.4"

# Use the Puma web server [https://github.com/puma/puma]
#gem "puma", "~> 5.0"

# Bundle and transpile JavaScript [https://github.com/rails/jsbundling-rails]
# 次のコマンドで雛形を生成すると、この gem が必要.
#   $ rails new <app-name> --javascript=webpack
gem "jsbundling-rails"

# デフォルトは importmap.
#gem "importmap-rails"

# Hotwire (HTML-over-the-wire) は Turbo, Stimulus および Strada から成る.
# hotwire-rails パッケージは早くも deprecated.

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
# 次の 4つのコマンド;
#    turbo:install
#    turbo:install:importmap
#    turbo:install:node   <- importmap を使わないときは, これ.
#    turbo:install:redis
# You must either be running with node (package.json) or importmap-rails
# (config/importmap.rb) to use this gem.
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
# 次の3つのコマンド;
#    stimulus:install
#    stimulus:install:importmap
#    stimulus:install:node
gem "stimulus-rails"

# PWA 化する
gem 'serviceworker-rails'

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production.
# Turbo Streams broadcasting に必要. `config/cable.yml` ファイルで指定した場合.
gem "redis", "~> 5.0"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ mingw mswin x64_mingw jruby ]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri mingw x64_mingw ]
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"
end

# 認証
gem 'sorcery', '>= 0.16.1'
