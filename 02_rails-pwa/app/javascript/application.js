// Entry point for the build script in your package.json

// 過去との互換性. Rails7: 非推奨
//import Rails from "@rails/ujs";
//Rails.start();

// Turbo
import "@hotwired/turbo-rails";
// Stimulus
import "./controllers";

import ServiceWorkerInstall from "./serviceworker-install.js";


