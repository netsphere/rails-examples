'use strict';

if (navigator.serviceWorker) {
    navigator.serviceWorker.register('/serviceworker.js', { scope: './' })
        .then(function(reg) {
            console.log('[Companion]', 'Service worker registered!');
        });
}


///////////////////////////////////////////////////////////////////////
// 独自のプロンプトを表示する

// Initialize deferredPrompt for use later to show browser install prompt.
var deferredPrompt;

class ServiceWorkerInstall {
    static getPrompt() {
        return deferredPrompt;
    }

    static clearPrompt() {
        deferredPrompt = null;
    }
}
window.swi = ServiceWorkerInstall;

// serviceworker.js 内では window は定義されない。
window.addEventListener('beforeinstallprompt', (ev) => {
    // Prevent the mini-infobar from appearing on mobile
    ev.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = ev;
    
    // Update UI notify the user they can install the PWA
    //showInstallPromotion();
    let div = document.getElementById('pwainstall');
    if (div) {
        div.style.display = 'block';
    }
    
    // Optionally, send analytics event that PWA install promo was shown.
    console.log(`'beforeinstallprompt' event was fired.`);
    return false;
});
