# -*- coding:utf-8 -*-

# 書き込み
class Post < ApplicationRecord
  # 親
  belongs_to :room
  belongs_to :posted_by, class_name: 'User'

  validates :message, presence:true

  # コールバック.
  after_create_commit :send_message

private
  # For after_create_commit
  def send_message
    # `broadcast_append_to()` は Turbo のメソッド.
    # 第1引数は stream を識別する.
    broadcast_append_to 'hogehoge_stream',
                  target:'message_list', partial:'posts/post', locals:{post:self}
  end
end
