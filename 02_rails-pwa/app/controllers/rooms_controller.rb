# -*- coding:utf-8 -*-

# 部屋
class RoomsController < ApplicationController
  before_action :require_login
  before_action :set_room, only: %i[ show post_message edit update destroy ]

  # GET /rooms or /rooms.json
  def index
    @rooms = Room.joins(:rooms_users).where('rooms_users.user_id = ?', current_user.id).order('rooms.updated_at')
    @room ||= Room.new  # /rooms/enter 用
  end

  # GET /rooms/1 or /rooms/1.json
  def show
    @posts = Post.where('room_id = ?', @room.id).order('updated_at')
    @post = Post.new
  end


  # GET /rooms/new
  def new
    @room = Room.new
  end


  # POST /rooms or /rooms.json
  def create
    @room = Room.new room_params.permit(:name)

    begin
      Room.transaction do
        @room.save!
        ru = RoomsUser.new room: @room, participant: current_user
        ru.save!
      end
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity
      return
    end
    
    redirect_to @room, notice: "Room was successfully created."
  end


  # POST /rooms/:id/message
  def post_message
    raise "internal error" if !@room
    @post = Post.new params.require(:post).permit(:message)
    @post.room = @room
    @post.posted_by = current_user
    begin
      @post.save!  # ここで, モデル `Post` クラスを通じて配信。
    rescue ActiveRecord::RecordInvalid
      @posts = Post.where('room_id = ?', @room.id).order('updated_at')
      render :show, status: :unprocessable_entity
      return
    end

    # ここではフォームを初期化する.
    # `<turbo-frame>` を置き換えるだけのときは, `turbo_stream.replace()` ではな
    # く, 単に `.html.erb` ファイルで新しい `<turbo-frame>` を送信すれば OK.
    #render turbo_stream: turbo_stream.replace('message_input',
    #                                    partial: "message_input",
    render(partial:"message_input", locals: {post:Post.new, room:@room})
  end

  # POST /rooms/enter
  def enter
    params = room_params.permit(:identifier)
    @room = Room.where("identifier = ?", params[:identifier].strip).take
    if !@room
      index()
      @room.errors.add :identifier, '見つかりません'
      # 部分描画
      render turbo_stream: turbo_stream.replace(:room_enter,
                                partial:'rooms/enter', locals:{room:@room})
      return
    end

    ru = RoomsUser.where('user_id = ? AND room_id = ?',
                         current_user.id, @room.id).first
    if !ru
      ru = RoomsUser.new room: @room, participant: current_user
      ru.save!
    end

    # `<turbo-frame>` へのレスポンスでは, フレーム内でリダイレクトしてしまう。
    # 意図した動作ではなく, この場合は Turbo Streams を使うべき。
    redirect_to @room, status:303
  end


  # GET /rooms/1/edit
  def edit
  end

  # PATCH/PUT /rooms/1 or /rooms/1.json
  def update
    if @room.update(room_params.permit(:name))
      redirect_to @room, notice: "Room was successfully updated."
    else
      render :edit, status: :unprocessable_entity 
    end
  end


  # DELETE /rooms/1 or /rooms/1.json
  def destroy
    Room.transaction do
      Post.where(room_id: @room.id).delete_all
      RoomsUser.where(room_id: @room.id).delete_all
      @room.destroy!
    end
    redirect_to rooms_url, status: :see_other, 
                           notice: "Room was successfully destroyed."
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_room
    ru = RoomsUser.where('user_id = ? AND room_id = ?',
                         current_user.id, params[:room_id] || params[:id]).first
    raise ActiveRecord::RecordNotFound if !ru
    @room = ru.room
  end

  # Only allow a list of trusted parameters through.
  def room_params
    params.fetch(:room, {})
  end
end
