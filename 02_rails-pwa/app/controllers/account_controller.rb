# -*- coding:utf-8 -*-

# ユーザアカウント
class AccountController < ApplicationController
  before_action :require_login, except: %i[sign_up create sign_in]

  # GET /
  # 単数形リソースでは, index メソッドではなく show メソッド.
  def show
    @user = current_user
  end

  # GET /sign_up
  def sign_up
    if current_user
      redirect_to sign_in_account_url
      return
    end
    @user = User.new
  end


  # POST /  -- user_registration
  def create
    @user = User.new user_params
    begin
      @user.save!
    rescue ActiveRecord::RecordInvalid
      render :sign_up, status: :unprocessable_entity
      return
    end

    # 流れでログインする.
    # login() は Sorcery::Controller module で定義される。
    u = login(@user.email, user_params.permit(:password)[:password])
    raise "internal error" if !u
    redirect_to(account_url, notice: 'Login successful')
  end

  # GET /edit
  def edit
    @user = current_user
  end

  # PUT /
  def update
    raise # TODO: impl.
  end


  # GET /sign_in   -- new_user_session
  # POST /sign_in  -- user_session
  def sign_in
    if request.get?
      @user = User.new  # ここはブランク情報でよいので, current_user ではない.
      return 
    end

    @user = User.new user_params
    @remember = params[:remember]

    # remember_me サブモジュールを組み込んだ場合, 第3引数で記憶させるか選ぶ
    # `login()` から呼び出される `auto_login()` メソッド内で, 単に真偽値で見ている
    # Trap: check_box の値をそのまま渡すと、常に真.
    if login(@user.email, @user.password,
             ActiveRecord::Type::Boolean.new.cast(@remember))
      redirect_back_or_to(account_url, notice: 'Login successful')
    else
      flash[:alert] = 'Login failed'
      # HTTP 422 Validation error. このstatus を付けないとエラー表示されない.
      render 'sign_in', status: :unprocessable_entity
    end    
  end

  # DELETE /sign_out  -- destroy_user_session
  def sign_out
    logout
    redirect_to '/', notice: 'Logged out!'
  end


private
  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation)
  end

end # class AccountController
