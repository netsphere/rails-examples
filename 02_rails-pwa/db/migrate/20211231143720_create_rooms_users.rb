# -*- coding:utf-8 -*-

# 中間テーブル
class CreateRoomsUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :rooms_users do |t|
      t.references :room, null:false, foreign_key:true
      t.references :user, null:false, foreign_key:true
      t.timestamps null:false # 参加した日時
    end
    # インデックスを張るときは [:room, :user] では失敗.
    add_index :rooms_users, [:room_id, :user_id], unique:true
  end
end
