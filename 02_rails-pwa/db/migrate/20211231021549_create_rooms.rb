# coding: utf-8
class CreateRooms < ActiveRecord::Migration[7.0]
  def change
    create_table :rooms do |t|
      t.string :identifier, null:false, index:{unique:true}
      t.string :name,       null:false, index:{unique:true}
      t.timestamps          null:false
    end
    # このコード
    #    t.type :column_name, index:{ key:value }
    # は、次に展開される。
    #    add_index :table_name, column_name, {key:value}
    # なので、index: にはハッシュを渡すこと。
    # t.type :column_name, unique:true は不正なコード.
  end
end
