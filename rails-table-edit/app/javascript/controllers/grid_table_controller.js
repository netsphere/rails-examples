
import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="grid-table"
export default class extends Controller
{
    static targets = ["tbl"];

    // @override
    connect() {
        this.tableData = JSON.parse(document.getElementById('tbl-json-data').textContent);
        // カラムもサーバから与える
        const columns = JSON.parse(document.getElementById('tbl-columns').textContent);
        this.tbl = new Tabulator("#grid-tabulator-tbl", {
                        // enable reactive mode. 元データの更新で表示に反映
                        reactiveData: true, 
                        data:         this.tableData, //set initial table data
                        columns: columns
                   });

        this.dirty = false;

        // 「このサイトを離れますか?」ダイアログ。メッセージは変更できない
        this.beforeUnloadListener = (event) => {
            // Cancel the event as stated by the standard.
            event.preventDefault();
            // Chrome requires returnValue to be set.
            event.returnValue = '';
            console.log("window.addEventListener() set"); // DEBUG
        }

        // formula の場合, すぐキャンセルする
        this.tbl.on("cellEditing", (cell) => {
            console.log("cellEditing: cell = ", cell);
            if (cell.getData()["formula"] !== undefined ) {
                cell.cancelEdit(); //clearEdited();
                return;
            }
            
        });
        
        // `cellEdited` はユーザによる編集が終わったとき
        this.tbl.on("cellEdited", (cell) => {
            console.log("cellEdited: cell = ", cell._cell ); // これで cell data
            console.log("cellEdited: row = ", cell.getData() ); // row が得られる

            const newValue = Number(cell._cell.value); // 文字列で来る
            if (Number.isNaN(newValue) ) {
                cell.restoreOldValue(); //clearEdited();
                return;
            }
            
            /* if (cell.getData()["formula"] !== undefined ) {
                cell.restoreOldValue(); //clearEdited();
                return;
            } */

            // dataChanged の中からだと、ループしてしまう
            this.update_formula(cell);

            if (!this.dirty) {
                window.addEventListener("beforeunload", this.beforeUnloadListener);
                this.dirty = true;
            }
        });

        // `dataChanged` は理由に関わらず, データが変更されたとき発火
        //   -> 下の `update_formula()` でも呼び出される
        // 先に `cellEdited`, 次に `dataChanged`.
        this.tbl.on("dataChanged", (data) => {
            console.log("dataChanged: data = ", data);
        });
    }

    find_by_name(name) {
        return this.tableData.find((x) => x["name"] == name);
    }
    
    // row が絡む式だけを更新する
    // reactive mode  -> tableData を更新すると表示に反映される
    update_formula(edited_cell) {
        const col_name = edited_cell._cell.column.field;
        for (let i = 0; i < this.tableData.length; ++i) {
            const row = this.tableData[i];
            if (row["formula"] !== undefined ) {
                const fields = row["formula"].split(',');
                if (fields.includes(edited_cell.getData()["name"])) {
                    let v = fields.reduce((acc, cur) => {
                        const r = this.find_by_name(cur);
                        return acc + Number(r[col_name]);
                    }, 0);
                    row[col_name] = v;
                    this.tableData.splice(i, 1, row); // これで replace
                }
            }
        }
    }

    getCsrfToken() {
        const metas = document.getElementsByTagName('meta');
        for (let meta of metas) {
            if (meta.getAttribute('name') === 'csrf-token') 
                return meta.getAttribute('content');
        }
        return null;
    }
    
    do_submit(event) {
        // not `#data`, use `getData()`
        //=> `Array` updated object.
        fetch("/grid_table/save", {
            method:"POST",
            credentials: 'same-origin',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                'X-CSRF-Token': this.getCsrfToken() },
            body: JSON.stringify(this.tbl.getData())
        })
            .then( response => {
                console.log(response);
                window.alert('response = ' + response.statusText);
            });

        this.dirty = false;
        window.removeEventListener("beforeunload", this.beforeUnloadListener);
    }
}
