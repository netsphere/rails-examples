
class GridTableController < ApplicationController
  def show
    @json_data = {}
  end

  # POST
  def save
    # `formula` など非表示のフィールドも来る. OK
    # params.require(:_json) で配列が得られる.
    # `_json` キーは、ここで付けられている。`Hash` でない場合のみ。
    # https://github.com/rails/rails/blob/main/actionpack/lib/action_dispatch/http/parameters.rb
    raise params.require(:_json).inspect
  end
end
