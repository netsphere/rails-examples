
# Tabulator cell editing + Stimulus 

JavaScript table/data grid ライブラリ <i>Tabulator</i> のサンプル。
セル編集機能があるが、ドキュメントだけでは今ひとつ使い方が分かりにくい。

<img src="Screenshot_20250103_191711.png" width="480" />

 - 列が固定の場合はいいが, この例の時間軸のように列方向にも可変にする場合は, 列一覧もサーバから与える。

 - 「このサイトを離れますか?」ダイアログ

 - セル編集に入ると, `cellEditing` イベントが発火する。編集不可のセルだった場合は, `cell.cancelEdit()` で編集キャンセルできる。"Cell Component" に記載がある (ドキュメントの場所が悪い).
 
 - 編集が終わると `cellEdited` イベント。値が不正などの場合は `cell.restoreOldValue()` が正解。

 - tabulator を reactive mode にすると, データの差し替え (`splice()`) で表示が更新される。




## pivottable

https://github.com/jjagielka/pivottable-grouping/ +
https://www.npmjs.com/package/multifact-pivottable/



### https://rubygems.org/gems/active_reporting/

v0.6.2: 2024年3月リリース

 - Snowflake schema でうまく動かせなかった. 普通に SQL を手書きすればいいのでは?
 - どの辺が ROLAP-like なのか不明
 


