# -*- coding:utf-8 -*-

Rails.application.routes.draw do
  # Define your application routes per the DSL in
  # https://guides.rubyonrails.org/routing.html

  # 部屋
  resources :rooms do
    # to: のコントローラ名は省略できない。ムム..
    post 'message', to: 'rooms#post_message'
    collection do
      post 'enter'  # /rooms/enter
    end
  end

  # ユーザアカウント
  resource :account,
           only: [:show, :create, :edit, :update], controller: 'account' do
    get    'sign_up'
    match  'sign_in', via: [:get, :post]
    delete 'sign_out'
  end

  # Defines the root path route ("/")
  root "top#index"
end
