// Configure your import map in config/importmap.rb.
// Read more: https://github.com/rails/importmap-rails

// 過去との互換性. Rails7: 非推奨
//import Rails from "@rails/ujs";
//Rails.start();

// Turbo
import "@hotwired/turbo-rails";
// Stimulus
import "./controllers";

// Bootstrap
import * as bootstrap from "bootstrap"
