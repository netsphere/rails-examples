# -*- coding:utf-8 -*-

# 中間テーブル
class RoomsUser < ApplicationRecord
  belongs_to :room
  belongs_to :participant, foreign_key: :user_id, class_name: 'User'
end
