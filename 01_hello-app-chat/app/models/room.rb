# -*- coding:utf-8 -*-

# 部屋
class Room < ApplicationRecord
  has_many :posts
  has_many :rooms_users
  has_many :participants, through: :rooms_users

  # before_validation -> #validate() -> after_validation ->
  #   before_save -> around_save -> before_create or before_update ->
  #   まだ続く.
  # around_save は特別. See https://stackoverflow.com/questions/4998553/rails-around-callbacks
  # 値を変更するものは `before_validation`. `before_save` では遅い.
  before_validation :setup, on: :create
  before_validation :norm_fields
  
  validates :name, presence:true
  validates :name, uniqueness: true

  
private
  # For `before_validation` on: :create
  def setup
    self.identifier = SecureRandom.hex(16)  # 16オクテット = 128bit.
  end

  # For `before_validation`. `before_save` では遅い.
  def norm_fields
    self.name = (self.name || "").unicode_normalize(:nfkc).strip
  end
end
