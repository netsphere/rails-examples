# -*- coding:utf-8 -*-

# 一つ一つの投稿
class CreatePosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts do |t|
      # t.references() は t.belongs_to() の別名。機能は同じ.
      # -> module TableDefinition
      # 親.
      t.references :room, null:false, foreign_key:true
      # この場合, posted_by_id カラムが作られる.
      t.references :posted_by, null:false, foreign_key:{to_table: :users}
      t.text  :message,   null:false
      t.timestamps        null:false
    end
  end
end
