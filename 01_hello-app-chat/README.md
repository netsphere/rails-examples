
# Hotwire Sample

Ruby on Rails 7.0 + Ruby 3.0 による chat サンプルアプリケィション。Hotwire, Action Cable (WebSocket), Redis. JavaScript なしでの SPA.
Sorcery による認証。

https://www.nslabs.jp/rubymemo.rhtml


## How To Run

  `config/database.yml.sample` ファイルを `database.yml` にコピーして、適宜編集。

  redis を動かす.

```shell
  $ redis-server
```

  アプリケーションを起動

```shell
  $ bin/rails db:migrate
  $ passenger start
```

  Webブラウザを2種類開いて、片方での書き込みが他方にも表示されることを確認。



### production 環境

```shell
  $ RAILS_ENV=production bin/rails credentials:edit

  $ RAILS_ENV=production bin/rails db:migrate

  $ RAILS_ENV=production bin/rails assets:precompile

  $ RAILS_ENV=production passenger start
```



## 解説

Rails 7 では Rails 6 までの Webpacker は単に廃止された。デフォルトは importmap. JavaScript をバンドルしない。もし JavaScript をバンドルしたいときは, <kbd>rails new</kbd> コマンドに <kbd>--javascript=webpack</kbd> オプションを渡す。この場合は jsbundling-rails gem が使われる.

```shell
  $ rails new hello-app
```

`Gemfile` をちょっとだけ修正. 

```ruby
gem 'sorcery', '>= 0.16.1'
```

Sorcery の設定は `config/initializers/sorcery.rb` ファイル。サブモジュールを追加して機能拡充できる。

<kbd>passenger start</kbd> でアプリケィションが起動することを確認。

モデルを作っていく. <kbd>rails g scaffold</kbd> コマンドの引数は単数形.

```shell
  $ bin/rails g scaffold room

  $ bin/rails g model post

  $ bin/rails g sorcery:install
```

マイグレィションファイルを適宜修正。Migrate する.

```shell
  $ bin/rails db:migrate
```

JavaScript の導入

```shell
  $ bin/rails importmap:install
```

必要なライブラリ を pin する. `config/importmap.rb` ファイルにライブラリへの参照が追加される。

```shell
$ bin/importmap pin @rails/ujs
Pinning "@rails/ujs" to https://ga.jspm.io/npm:@rails/ujs@7.0.0/lib/assets/compiled/rails-ujs.js
```

Turbo の導入

```shell
  $ bin/rails turbo:install
```

Turbo Streams broadcasting をおこなうには, Action Cable アダプタとして <i>Redis</i> が必要。Turbo には Action Cable が必須というわけではない。

```shell
  $ bin/rails stimulus:install
```

この状態で, `config/importmap.rb` ファイルは次のようになっている。

```ruby
pin "application", preload: true
pin "@rails/ujs", to: "https://ga.jspm.io/npm:@rails/ujs@7.0.0/lib/assets/compiled/rails-ujs.js"
pin "@hotwired/turbo-rails", to: "turbo.min.js", preload: true
pin "@hotwired/stimulus", to: "stimulus.min.js", preload: true
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin_all_from "app/javascript/controllers", under: "controllers"
```

`config/cable.yml` ファイルは次のようになっている. 上述のように, Turbo Streams broadcasting には Redis が必要。<code>development:</code> も Redis を使うように書き換える。

```yaml
development:
  adapter: async

test:
  adapter: test

production:
  adapter: redis
  url: <%= ENV.fetch("REDIS_URL") { "redis://localhost:6379/1" } %>
  channel_prefix: hello_app_production
```

`app/javascript/application.js` ファイルが, importmap-rails であれ jsbundling-rails であれ, JavaScript エントリポイントになる。次のようになっている。

```javascript
import Rails from "@rails/ujs";
Rails.start();
import "@hotwired/turbo-rails"
import "controllers"
```




