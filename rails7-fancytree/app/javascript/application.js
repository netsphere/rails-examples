// Configure your import map in config/importmap.rb.
// Read more: https://github.com/rails/importmap-rails

// TURBO
import "@hotwired/turbo-rails"

// サイト全体で Turbo Drive を無効にする: 個々の HTML 要素で `data-turbo="true"`
// を付けると有効.
//Turbo.session.drive = false;

// jQuery -- この書き方ではダメ。旧来の書き方で行くこと。
// import jQuery from "jquery";
// window.$ = jQuery;

// Stimulus
import "controllers"
