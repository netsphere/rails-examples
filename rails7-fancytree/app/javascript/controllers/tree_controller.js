
// Stimulus: JavaScript もレールに沿って

import { Controller } from "@hotwired/stimulus"

//import 'jquery.fancytree/dist/skin-lion/ui.fancytree.css';

// これはダメ The requested module 'jquery.fancytree' does not provide an export named 'createTree' 
//import {createTree} from 'jquery.fancytree';
//require.resolve('jquery.fancytree');  これはダメ. ReferenceError: require is not defined

// import 'jstree';

// アクション, ターゲット
export default class extends Controller
{
    static targets = ["hogeTree1"];

    // クリックでフレーム内に表示するようにする.
    // See https://wwwendt.de/tech/fancytree/demo/sample-iframe.html#
    printTree() {
        console.log("hog");

        $('#tree1').jstree();

        // Listen for events: https://www.jstree.com/docs/events/
        $('#tree1').on('changed.jstree', function(e, data) {
            if(data.node) {
                const frame = document.querySelector('#edit');
                frame.src = data.node.a_attr.href;
            }
        });
/*        
        $('#tree1').fancytree({
            minExpandLevel: 1,
            activate: function(event, data) {
                var node = data.node;
                // Use <a> href and target attributes to load the content:
                if( node.data.href ) {
                    // Open target
                    const frame = document.querySelector('#edit');
                    frame.src = node.data.href;
                }
            }
        });
*/
    }
}


