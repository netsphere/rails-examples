
# 各要素
class TreeItemsController < ApplicationController
  before_action :set_tree_item, only: %i[ show edit update destroy ]

  # GET /tree_items or /tree_items.json
  #def index
  #@tree_items = TreeItem.all
  #end

  # GET /tree_items/1 or /tree_items/1.json
  def show
    # `update()` と使いまわす
    # これはダメ。hover で表示が切り替わってしまう。
    #render turbo_stream: turbo_stream.replace('edit', partial:'show')
  end

  # GET /tree_items/new
  def new
    @tree_item = TreeItem.new
    @tree_item.parent_id = params[:parent] if !params[:parent].blank?
  end

  def new_cxl
  end
  
  # GET /tree_items/1/edit
  def edit
  end

  # POST /tree_items or /tree_items.json
  def create
    @tree_item = TreeItem.new tree_item_params.permit(:name)
    if !tree_item_params[:parent_id].blank?
      parent = TreeItem.find tree_item_params[:parent_id]
      @tree_item.parent_id = parent.id 
    end
    
    # 先に兄弟の末尾を得る
    @target = parent ? parent.children.last : nil
    
    if @tree_item.save
      flash[:notice] = "Tree item was successfully created." 
      #redirect_to tree_item_url(@tree_item), notice: 
    else
      render :new, status: :unprocessable_entity 
    end
  end

  
  # PATCH/PUT /tree_items/1 or /tree_items/1.json
  def update
    if @tree_item.update(tree_item_params.permit(:name) )
      flash[:notice] = "Tree item was successfully updated." 
      #redirect_to tree_item_url(@tree_item), 
    else
      render :edit, status: :unprocessable_entity
    end
  end

  
  # DELETE /tree_items/1 or /tree_items/1.json
  def destroy
    @tree_item.destroy!

    flash[:notice] = "Tree item was successfully destroyed."
    #redirect_to tree_items_url, notice: 
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_tree_item
    @tree_item = TreeItem.find params[:id]
  end

  # Only allow a list of trusted parameters through.
  def tree_item_params
    params.require('tree_item')
  end
end
