
# それぞれの node
class TreeItem < ApplicationRecord
  belongs_to :parent, class_name: 'TreeItem', optional: true
  has_many :children, class_name: 'TreeItem', foreign_key: 'parent_id'
end
