# Pin npm packages by running ./bin/importmap

pin "application", preload: true

# TURBO
pin "@hotwired/turbo-rails", to: "turbo.min.js"

# $ bin/importmap pin jquery.fancytree  -> ギブアップ

# Stimulus
pin "@hotwired/stimulus", to: "stimulus.min.js", preload: true
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin_all_from "app/javascript/controllers", under: "controllers"

# jstree  -> ギブアップ

