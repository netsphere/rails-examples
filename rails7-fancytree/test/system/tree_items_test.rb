require "application_system_test_case"

class TreeItemsTest < ApplicationSystemTestCase
  setup do
    @tree_item = tree_items(:one)
  end

  test "visiting the index" do
    visit tree_items_url
    assert_selector "h1", text: "Tree items"
  end

  test "should create tree item" do
    visit tree_items_url
    click_on "New tree item"

    click_on "Create Tree item"

    assert_text "Tree item was successfully created"
    click_on "Back"
  end

  test "should update Tree item" do
    visit tree_item_url(@tree_item)
    click_on "Edit this tree item", match: :first

    click_on "Update Tree item"

    assert_text "Tree item was successfully updated"
    click_on "Back"
  end

  test "should destroy Tree item" do
    visit tree_item_url(@tree_item)
    click_on "Destroy this tree item", match: :first

    assert_text "Tree item was successfully destroyed"
  end
end
