require "test_helper"

class TreeItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tree_item = tree_items(:one)
  end

  test "should get index" do
    get tree_items_url
    assert_response :success
  end

  test "should get new" do
    get new_tree_item_url
    assert_response :success
  end

  test "should create tree_item" do
    assert_difference("TreeItem.count") do
      post tree_items_url, params: { tree_item: {  } }
    end

    assert_redirected_to tree_item_url(TreeItem.last)
  end

  test "should show tree_item" do
    get tree_item_url(@tree_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_tree_item_url(@tree_item)
    assert_response :success
  end

  test "should update tree_item" do
    patch tree_item_url(@tree_item), params: { tree_item: {  } }
    assert_redirected_to tree_item_url(@tree_item)
  end

  test "should destroy tree_item" do
    assert_difference("TreeItem.count", -1) do
      delete tree_item_url(@tree_item)
    end

    assert_redirected_to tree_items_url
  end
end
