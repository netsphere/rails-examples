class CreateTreeItems < ActiveRecord::Migration[7.1]
  def change
    create_table :tree_items do |t|
      # naive tree. root は NULL
      # `_id` は不要. 次のように書けば `parent_id` になる.
      t.references :parent, foreign_key:{to_table: :tree_items}
      t.string :name, null:false
      
      t.timestamps
    end
  end
end
