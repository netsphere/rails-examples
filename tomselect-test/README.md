
# A Choices.js sample

<i>Tom Select</i> だと、どうもユーザ体験が狙った感じにならなかった。<i>Choices.js</i> に切り替える。


## パッケージ選定

HTML `select` 要素を洒落た感じにするライブラリには、次のものがある:

 - ▲ select2  -- 500,000件/週で圧倒的.
     v4.1.0-rc で開発が止まっている。Bootstrap4, Internet Explorer 11 と古く、新しいプロジェクトで選びにくい. ドキュメントを見ると, JavaScript API もやや弱い。

 - tom-select -- 100,000件/週の下ぐらいで団子状態。モダンな感じでよさそう。
 - <a href="https://github.com/Choices-js/Choices/">Choices-js/Choices: A vanilla JS customisable select box/text input plugin ⚡️</a>  これもモダンな感じ。

選外:
 - downshift   -- React component. 非常にインストール数が大きいが, 低落傾向
 - ▲ bootstrap-select  -- Bootstrap4. 古い.
 - ▲ chosen-js  -- 更新されていない。開発終了
 - ▲ Selectize  -- 更新されていない。開発終了. 後継が tom-select



## 手順

importmap でやってみたが、どうも上手くいかなかった。"jsbundling-rails" でやり直し。
 - jsbundling-rails
 - bootstrap5

先行記事: <a href="https://qiita.com/kazama1209/items/87734013dd2d1938d7b6">Rails 7 + Stimulus + Tom Select で作るインクリメンタルサーチ付き動的セレクトボックス #Rails7 - Qiita</a>



