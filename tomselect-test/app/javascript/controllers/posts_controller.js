
// Tom Select だと、どうもユーザ体験が狙った感じにならない。
// Choices.js に切り替える。

import { Controller } from "@hotwired/stimulus"
//import TomSelect from "tom-select";
import Choices from "choices.js";

/*
const MyConfig = {
    hidePlaceholder:true
};
*/

export default class extends Controller
{
    // `data-controller` でコントローラ class を指定し,
    // `data-コントローラ名-target` でターゲットを指定.
    //   <div data-controller="s​earch">
    //     <div data-search-target="results"></div>
    //   </div>
    // 自動的に `this.xxxTarget` というプロパティが生える. 名前にハイフン不可
    static targets = ["mainCatSelect", "subCatSelect"];


    // @override
    connect() {
        let m = this.mainCatSelectTarget;
        console.log('main = ', m); // ここが `undefined` の場合, TomSelect() が
                                   // TypeError: Cannot read properties of undefined (reading 'jquery')
        //this.mainCatSelector = new TomSelect(m, MyConfig);
        //this.subCatSelector  = new TomSelect(this.subCatSelectTarget,  MyConfig);
        this.mainCatSelector = new Choices(m, {
            shouldSort:false });
        this.subCatSelector  = new Choices(this.subCatSelectTarget, {
            shouldSort:false });
        this.filterSubCatOptions({target:{value: this.mainCatSelector.getValue(true)[0]}});
        // 保存しておく
        //this.sub_cat_options = Object.values(this.subCatSelector.options);
        //console.log('sub_cat_options = ', this.sub_cat_options);
    }

    // Callback
    filterSubCatOptions(ev) {
        const main_cat_id = ev.target.value;
/*        
        const filtered = this.sub_cat_options.filter((option) =>
                             option.mainid == main_cat_id);
        this.subCatSelector.clear();
        this.subCatSelector.clearOptions();
        this.subCatSelector.addOptions(filtered);
*/
        const sub_choices = JSON.parse(document.getElementById("json-data").textContent);
        const filtered = sub_choices.filter((option) =>
            option.main == main_cat_id);
        console.log('value = ', filtered[0].value);
        this.subCatSelector.setValue([filtered[0]]); // 必ず配列
        this.subCatSelector.setChoices(filtered, 'value', 'label', true);
    }
}
