
import React, {useState} from 'react';

function Counter(props) {
    const [name, setName] = useState(props.name);
    const [age, setAge] = useState(props.age);

    const handleClick = () => {
	alert('You clicked me!');

	setAge(age + 1);
	setAge((pre_count) => { return pre_count + 1; });
    }
    
    return <div>
		   Hello, {name}! age = {age}
		   <button onClick={handleClick}>Push!</button>
	       </div>;
}

export default Counter;
