
import React, {useState} from 'react';

export default function HeavyComponent() {
    const [count, setCount] = useState(0);
    return (
    <div className='card'>
    <button onClick={() => setCount(count => count + 1)}>Click me</button>
    <p>Count is {count}</p>
    <InnerHeavyComponent />
    </div> );
}

function InnerHeavyComponent() {
    console.log("InnerHeavyComponent rendered.");
    return (
        <div style={{border:'solid red 2pt'}}>
            <h1>Heavy Component</h1>
        </div> );
}
