
import React from 'react';
import {createRoot} from 'react-dom/client';

class SmallReact
{
    static _app;
    
    static start() {
        console.log("_app = ", SmallReact._app);
        if (SmallReact._app === undefined)
            SmallReact._app = new SmallReact();
        SmallReact._app.start();
        return SmallReact._app;
    }

    // load 後に呼び出される.
    static parseDocument(targetNode) {
        const matches = targetNode.querySelectorAll('[data-react-class]');
        matches.forEach((item) => {
            console.log("parse matched ", item);
            const ident = item.getAttribute('data-react-class'); //=> "counter"
	    const ctor = SmallReact._app.component_map[ident];
	    if (ctor !== undefined) 
                SmallReact._app.render(item, ctor);
	    else {
		console.log("error: React component not found: ", ident);
	    }
        });
    }
    
    constructor() {
        this.component_map = {};
        this.after_load = false; // 登録のときだけ考慮
    }
    
    // private
    async start() {
        await domReady();
        this.after_load = true;

        // Turbo が off の場合と, Turbo ありの場合は初回だけここで render
        //  -> やっぱり `turbo:load` でやる
        //SmallReact.parseDocument();
    }

    render(div_elem, componentCtor) {
        console.log("SmallReact#render() enter");

	if (div_elem.innerHTML !== "")
	    return;
	
	const propsJSON = div_elem.getAttribute('data-react-props');
	const props = propsJSON !== null && propsJSON !== "" ?
	                  JSON.parse(propsJSON) : {};
	
        const root = createRoot(div_elem);
        root.render(React.createElement(componentCtor, props, null) );
    }
    
    register(identifier, theCtor) {
        this.component_map[identifier] = theCtor;
/*
        if (this.after_load) {
            const e = document.querySelector('[data-react-class="' + identifier + '"]');
            this.render(e, identifier);
        }
 */
    }
}

function domReady() {
    return new Promise((resolve) => {
        // "loading" -> "interactive" -> "complete"
        if (document.readyState === "loading")
            document.addEventListener("DOMContentLoaded", () => resolve() );
        else 
            resolve();
    });
}

export default SmallReact;
