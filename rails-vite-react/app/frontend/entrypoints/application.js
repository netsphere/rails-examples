// To see this message, add the following to the `<head>` section in your
// views/layouts/application.html.erb
//
//    <%= vite_client_tag %>
//    <%= vite_javascript_tag 'application' %>
console.log('Vite ⚡️ Rails')

// If using a TypeScript entrypoint file:
//     <%= vite_typescript_tag 'application' %>
//
// If you want to use .jsx or .tsx, add the extension:
//     <%= vite_javascript_tag 'application.jsx' %>

console.log('Visit the guide for more information: ', 'https://vite-ruby.netlify.app/guide/rails')

// Example: Load Rails libraries in Vite.
//
import "@hotwired/turbo-rails";
//
// import ActiveStorage from '@rails/activestorage'
// ActiveStorage.start()
//
// // Import all channels.
// const channels = import.meta.globEager('./**/*_channel.js')

// Example: Import a stylesheet in app/frontend/index.css
// import '~/index.css'

// Stimulus
import "../../javascript/controllers";

// react components
import SmallReact from "./small_react";

const small_react = SmallReact.start();
// Top-level await: ES2022
//await small_start.start();
window.small_react = small_react;

import Counter from "../components/counter.jsx";
small_react.register("counter", Counter);

import HeavyComponent from "../components/heavy_component.jsx";
small_react.register("heavy_component", HeavyComponent);

// Turbo Streams 対策
// 単に `<div id="hoge"></div>` なので、見分けつかない. 全部なめるか?
function streamChanges(records, observer) {
  for (const mutation of records) {
      console.log("type = ", mutation.type, mutation);
    
      if (mutation.type === "childList" && mutation.addedNodes.length > 0) {
	  SmallReact.parseDocument(mutation.target);
      }
/*    
    for (const addedNode of mutation.addedNodes) {
      // comment, text, element, ...
      if ( addedNode instanceof HTMLElement ) {
        console.log("mutation added", addedNode);
        const ident = addedNode.getAttribute('data-react-class'); //=> "counter"
        if (ident in window.small_react.component_map)
          window.small_react.render(addedNode, ident);
      }
    }
    */
  }    
}

// ページ遷移で切れる.
const observer = new MutationObserver(streamChanges);
//observer.observe(document.body,  // targetNode
//                 {childList: true, subtree: true}); // config

// Turbo 対策
// "turbo:load" 初回も2回目以降も呼び出される。チラつきが気になる. 
// "turbo:render" 初回は呼び出されない (2回目以降のみ)
//    -> Turbo が off の場合と, 初回呼び出しは `start()` 内で描画.
//    `disconnect` のタイミングでも呼び出される。やっぱり `turbo:load` がよい.
document.addEventListener("turbo:load", () => {
  SmallReact.parseDocument(document);

  observer.observe(document.body,  // targetNode
                 {childList: true, subtree: true}); // config
});

