
=begin
react-rails style:
  react_component('Post', {title: 'New Post'}, prerender: true) 
  react_component('Post', {title: 'New Post'}, :div) 

  options:
      tag: HTML tag name, otherwise `div`
      prerender:
      camelize_props:

react_on_rails style:
  react_component('Post', props: {title: 'New Post'}, prerender:true) 

  options:
      id: `<div>` id
      html_options: any other HTML options
=end


module ApplicationHelper

  # @param [String] ident   React component identifier
  # @param [Hash] props     pass it to React component's `constructor()` 
  # @param [Hash] options   `react_component()` options and html_options
  # @option options [String] tag  HTML tag name, otherwise `div`
  def react_component(ident, props = nil, options = nil)
    # react_component('Post', {title: 'New Post'}, :div) 
    options = {tag: options} if options.is_a?(Symbol)

    # HTML の属性に `prerender` はないので、安全に分離できる
    html_options = options || {}
    html_options["data-react-class"] = ident
    html_options["data-react-props"] = props.to_json if props
    
    tag_name = html_options.delete(:tag) || "div"
    options = {
      prerender: html_options.delete(:prerender),
      camelize_props: html_options.delete(:camelize_props)
    }
    
    return content_tag(tag_name, "", html_options, true)
  end
end
