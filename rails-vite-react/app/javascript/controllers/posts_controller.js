
import { Controller } from "@hotwired/stimulus"
//import Counter from "../components/counter.jsx";

// Connects to data-controller="posts"
export default class extends Controller
{
    static targets = ["output", "root"];
    
    connect() {
        console.log("PostsController#connect() called");
    }

    // callback
    changeText() {
        this.outputTarget.textContent = "テキストが変更されました!";
    }
}
