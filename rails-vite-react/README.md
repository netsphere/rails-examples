
# Ruby on Rails 8 の view で React 19 を動かす

フロントエンドに React を用いる場合、よくあるのは、
 - Rails 側は API モードで動かす
 - React 側は独立した SPA として、Rails の API を叩く

API として OpenAPI を活用したりするが、それでも、アプリケィションを2つ開発することになって、生産性がよくない。

少ない React コンポーネントのみ活用で足りる場合、Rails アプリケィションに partial view として React コンポーネントを表示すればいい。

似た取り組み: <a href="https://jetthoughts.com/blog/react-ruby-on-rails-without-any-gems-typescript/">React + Ruby on Rails without any gems</a>




## vite バンドラ (build tool) を使う

Build tool (バンドラ) は、Vite がぐいぐいシェアを伸ばしている。業務目的では webpack を逆転。
https://2024.stateofjs.com/en-US/libraries/build_tools/

Babel なしに JSX のトランスパイル可能。これは簡単に成功。

Stimulus や react_on_rails のように, 次のようにする

 - ✅ 各コンポーネントを陽に登録
```javascript
const small_react = SmallReact.start();
window.small_react = small_react;

import Counter from "../../javascript/components/counter.jsx";
small_react.register("counter", Counter);
```

 - ✅ view helper として `react_component()` を使う
```erb
<%= react_component("counter", {name:"Tanaka Taro", age:42}) %>
```


### Turbo 対応

Turbo Drive が有効だと, `DOMContentLoaded` イベントが最初のページ読み込み時にしか発火しないため, `turbo:load` イベントで react component をレンダリングするようにする。

また, Turbo Streams がページ内の一部要素だけ更新してくるので, `MutationObserver` オブジェクトで変更を検出する



### React Compiler 対応

`vite.config.js` ファイルにプラグインを書くだけで, React Compiler も問題なく動く。
```javascript
export default defineConfig({
    plugins: [
        react({
            babel: {
                plugins: [
                    ["babel-plugin-react-compiler", ReactCompilerConfig],
                ],
            },
        }),
        RubyPlugin(),
    ],
})
```




### インストール

esbuild を指定する。

```shell 
$ rails new rails-vite-react --skip-active-storage --javascript=esbuild --skip-bundle
```

Add `gem "vite_rails"` to `Gemfile` file.

```shell
$ bundle install --path vendor/bundle
$ bundle exec vite install

$ rails turbo:install
$ rails stimulus:install
```

```shell
$ rails g scaffold Post
```

entry point file が代わる: `app/javascript/application.js` -> `app/frontend/entrypoints/application.js`




## gem 利用 (失敗)

2つ gem がある:
 - react-rails 
```erb
<%= react_component('HelloMessage', name: 'John') %>
<!-- becomes: -->
<div data-react-class="HelloMessage" data-react-props="{&quot;name&quot;:&quot;John&quot;}"></div>
```
   オプションを与えるときは、次のようになる
```erb
<%= react_component('Post', {title:'New Post'}, prerender: true) %>
```

   `prerender:` オプションで, クライアント側レンダリングとサーバ側レンダリングを選ぶ
   
 - react_on_rails
   同じ `react_component()` という名前だが, 引数が異なる
```erb
<%= react_component('Post', props:{title: 'New Post'}, prerender: true) %>
```

react-rails のほうがダウンロード数は多いが、react_on_rails への移行が推奨されている。

結果: どちらも動かすことができなかった。
両方とも, 最小構成を狙ったり, サンプルプログラムを動かしたりしたが、ダメ。両方とも issue が立っている。
 - <a href="https://github.com/reactjs/react-rails/issues/1351">Feature Request: React 19 preparation · Issue #1351 · reactjs/react-rails · GitHub</a>
 - <a href="https://github.com/shakacode/react_on_rails/issues/1669">Support React 19 · Issue #1669 · shakacode/react_on_rails · GitHub</a>

どちらだったかは, 古い React なら動いたが、それでは意味がない



## 生の webpack + babel (失敗)

JSX のトランスパイルができればいいので、webpack に babel を組み込む方法がある。

結果: 上手くいかず。
逆に JSX 以外は何も触らなくていいのに、特に ES modules (ESM) を CommonJS module に変換されるのを抑制する方法が分からなかった。できないはずないが、何か簡単な見落としありそう。






## サーバ側レンダリング (実装しない)

React Server Components (RSC) は、React コンポーネントをサーバ側で動かし、ファイルシステムから読み取ったり、データベースにアクセスできたりする。
それは、もはや Rails と組み合わせる意味がない。

ここでいうサーバ側レンダリングは、クライアント側でやっていた内容のレンダリングをサーバ側で実行し, リアルDOM (HTML片) を返すようにする。

"babel-transpiler" gem と "execjs" gem を組み合わせるのが考えられる。しかし, "execjs" は評価のたびに子プロセスとして nodejs を起動するため、とてもではないがパフォーマンスが出ない。

いくつか取り組みがあるが, 普及していない。
 - execjs-fastnode
 - execjs-pcruntime
 
