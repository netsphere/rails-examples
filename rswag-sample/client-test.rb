
# HTTP client ライブラリ
#  - faraday メジャ. 内部で `Net::HTTP` (net-http gem) を利用

CLIENT_ID = "b7fb0338ee5946e54bc29a62f807f1b4"
CLIENT_SECRET = "0e058614faf286547df7ade2ba1019676f4fc1e0ca0993b7aba9d0d53d0bf769"

#gem 'net-http'
require 'net/http' # 多様な書き方ができて、よくない
require 'base64'
require 'json'

SERVER = URI.parse("http://localhost:3000")
http = Net::HTTP.new(SERVER.host, SERVER.port)

# OAuth 2.0 [RFC 6749] section 4.3 Resource Owner Password Credentials Grant (ROPC)
#   grant_type ="password"
#   username   リソースオーナのユーザ名
#   password   リソースオーナのパスワード
#   scope      アクセス要求スコープ.
# -> OAuth 2.1 で削除. https://datatracker.ietf.org/doc/html/draft-ietf-oauth-v2-1-11
#    解説 https://ritou.hatenablog.com/entry/2018/09/26/113512

# 認証, トークン取得
# RFC 7617 The 'Basic' HTTP Authentication Scheme  -> URL safe *でない* ほうのbase64
def get_token http
  req = Net::HTTP::Post.new SERVER + "/auth/token"
  req.basic_auth(CLIENT_ID, CLIENT_SECRET)
  req.form_data = {email: "hori", password: "horikawa"}  # 手抜き ●●
  res = http.request req
  return (JSON.parse res.body)
end

def failed_to_get_token http
  # secret が違う
  req = Net::HTTP::Post.new SERVER + "/auth/token"
  req.basic_auth(CLIENT_ID, CLIENT_SECRET + "x")
  #req.form_data = {email: "fuga", password: "hoge"}
  res = http.request req
  p res.body

  # password が違う
  req = Net::HTTP::Post.new SERVER + "/auth/token"
  req.basic_auth(CLIENT_ID, CLIENT_SECRET)
  req.form_data = {email: "fuga", password: "hoge"}
  res = http.request req
  p res.body
end

#failed_to_get_token http
#exit

json = get_token http
p json
#=> {"access_token"=>"c6e01d8a88fa055654407dc90d7772b490490d5cc5dd10ae89ea3bcaa8f08ab9", "token_type"=>"Bearer", "expires_in"=>86399}

# API にアクセス
def get_todos http, access_token
  req = Net::HTTP::Get.new SERVER + "/api/v1/todos"
  # RFC 6750 The OAuth 2.0 Authorization Framework: Bearer Token Usage
  req['Authorization'] = 'Bearer ' + access_token
  req['Accept'] = 'application/json'
  res = http.request req
  return (JSON.parse res.body)
end

def failed_to_get_todos http, access_token
  req = Net::HTTP::Get.new SERVER + "/api/v1/todos"
  # RFC 6750 The OAuth 2.0 Authorization Framework: Bearer Token Usage
  req['Authorization'] = 'Bearer ' + access_token + "x"
  req['Accept'] = 'application/json'
  res = http.request req
  res.each_header do |k, v| puts "#{k}=#{v}" end
  p res.body
end

json = get_todos http, json['access_token']
p json

#failed_to_get_todos http, json['access_token']
