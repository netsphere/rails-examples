
# Ruby on Rails + OpenAPI 3.0

OpenAPI 3.1.0 は OpenAPI 3.0.x と互換性がない。3.0 のほうを使え.


## Gem の選定

サーバ側

https://www.libhunt.com/compare-rspec-openapi-vs-rswag   rswag のほうがメジャー。

 - rswag  https://github.com/rswag/rswag/  RSpec から swagger.yml を生成.
   Ruby 文法で OpenAPI schema を書いていく感じ。
   
 - Committee::Rails  https://github.com/willnet/committee-rails/  テストケースに組み込み、スキーマ通りに動くか確認。Schema-First で開発する場合に利用。

 - ▲ rspec-openapi https://github.com/exoego/rspec-openapi/   specから OpenAPI 定義 (.yaml) を生成. rswag と同じ?
   型を書く感じではない。
   
 - ▲ https://github.com/zhandao/zero-rails_openapi/ メソッド定義に DSL を付けるタイプ。開発が止まっている。


クライアント側

 - https://github.com/OpenAPITools/openapi-generator/ (Java)
   クライアントコードの自動生成.





## rswag 使い方

```ruby
# Gemfile
gem 'rswag-api'
gem 'rswag-ui'

group :development, :test do
  gem 'rspec-rails'
  gem 'rswag-specs'
end
```

```shell
  $ bin/rails g rspec:install
  $ rails g rswag:api:install
  $ rails g rswag:ui:install
  $ RAILS_ENV=test rails g rswag:specs:install
```


Swagger JSON ファイルを生成する

```shell
  $ bin/rails rswag:specs:swaggerize
```

