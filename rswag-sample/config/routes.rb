
Rails.application.routes.draw do
  # RPs
  resources :clients
  
  resources :todos do
    post 'done', on: :member
    get 'edit_cxl', on: :member
  end
  
  resource :account, controller:'account', only: %i[edit show update] do
    match "sign_in", via:[:get, :post]
    # メールアドレスで登録する場合は, ページを分ける。
    # OpenID Connect only にするときは `sign_in` だけにまとめたほうがいい.
    get "sign_up"
    
    post "sign_out"
  end
  
  # Display Swagger UI
  mount Rswag::Ui::Engine => '/api-docs'
  # これも必要。これを有効にしないと, Fetch error Not Found `/api-docs/v1/swagger.yaml`
  mount Rswag::Api::Engine => '/api-docs'

  namespace :auth do
    # "token_endpoint"
    post 'token', to: 'tokens#create'

    # "revocation_endpoint"
    post 'revoke', to: 'tokens#destroy'
  end
  
  namespace :api do
    namespace :v1 do
      resources :todos, only: %i[ index show create update destroy ]
    end
  end
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "welcome#index"
end
