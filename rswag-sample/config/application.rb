require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# フォームエラーのときに `.field_with_errors` が自動で付く。
#   <div class="field_with_errors">
#     <label class="col-sm-2 col-form-label" for="user_password_confirmation">Password (再度):</label>
#   </div>
# ラベルのほうに付くのも微妙.
# Bootstrap そのほか UI toolkit を使っていると邪魔。
# -> [Rails] field_error_proc を使ってバリデーションエラーの表示をいい感じにする
#    https://qiita.com/youcune/items/76a50ae3a2863a8f8b00

module RswagSample
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w(assets tasks))

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
