# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.openapi_root = Rails.root.join('swagger').to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under openapi_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a openapi_spec tag to the
  # the root example_group in your specs, e.g. describe '...', openapi_spec: 'v2/swagger.json'
  config.openapi_specs = {
    'v1/swagger.yaml' => {
      # 必須. バージョン.
      openapi: '3.0.3',
      # 必須.
      info: {
        title: 'TODO Sample API V1',
        version: 'v1'
      },
      # 必須. 自動生成させる
      paths: {},
      servers: [
        {
          url: "http://localhost:3000",
          #url: 'https://{defaultHost}',
          variables: {
            defaultHost: {
              default: 'www.example.com'
            }
          }
        }
      ],
      components: {
        schemas: {
          todo: {  # 手で作ってしまう
            type: 'object',
            required: %i[id title done created_at],
            properties: {
              id: { type: :integer },
              title: { type: :string, example: "やることやること" },
              done: { type: :boolean, description: "is it finished?" },
              created_at: { type: :string, format: "date-time" },
              updated_at: { type: :string, format: "date-time" }
            }
          }
        },
        parameters: {  # 共通パラメータ  
          skipParam: {
            name: "$skip",
            in: :query,
            description: "number of items to skip",
            #required: true
            schema: { type: :integer }
          },
          limitParam: {
            name: "$top",
            in: :query,
            description: "max records to return",
            #required: true
            schema: { type: :integer }
          }
        },
        securitySchemes: {  # セキュリティの "定義"
          mySecurityScheme: {
            type: "http",
            # name: "MyHogeKey",  これは type:"apiKey" のときのみ
            #in: header
            scheme: "Bearer",
            bearerFormat: "JWT"
          }
        }
      }, # /components
      security: [
        {
          mySecurityScheme: []
        }
      ]
    }
  }

  # Specify the format of the output Swagger file when running 'rswag:specs:swaggerize'.
  # The openapi_specs configuration option has the filename including format in
  # the key, this may want to be changed to avoid putting yaml in json files.
  # Defaults to json. Accepts ':json' and ':yaml'.
  config.openapi_format = :yaml
end
