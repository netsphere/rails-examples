
require 'swagger_helper'
#require 'rails_helper'


RSpec.describe "Api::V1::Todos", type: :request do
  # これが必要
  let(:user) { User.create!(email:"hoge@hoge", password:"longlongsecret", password_confirmation:"longlongsecret") }
  let(:token) {
    client = Client.create!(name:"test-app")
    AccessToken.create!(client_id: client.id, user_id: user.id)
  }
  let(:'Authorization') { "Bearer #{token.token}" }
        
  # path 単位で作る
  path "/api/v1/todos" do
    # index()
    get 'List todos' do
      # Swagger UI のグルーピングに使われる
      tags 'Todos'
      
      description 'List all todos in the system'
      parameter name: "$skip",  in: :query, required:false, type: :integer
      parameter name: "$top",   in: :query, required:false, type: :integer
      parameter name: "$count", in: :query, required:false, type: :boolean
      
      #consumes 'application/json'
      
      produces 'application/json'
      # 複数の `response`. 正常時とエラー
      response '200', 'successful' do
        schema type: :object,
               properties: {
                 count: {type: :integer},
                 value: {type: :array,
                         items: {"$ref" => "#/components/schemas/todo"} } },
               required: ['count', 'value']
        run_test!
      end
    end

    # create()
    post 'Creates a todo' do
      tags 'Todos'

      consumes 'application/json'
      parameter name: :todo, in: :body, schema: {
                  type: :object,
                  properties: {
                    title: {type: :string},
                    done: {type: :boolean} },
                  required: ['title', 'done']
                }

      produces 'application/json'
      # 正常時とエラー
      response '201', 'todo created' do
        let(:todo)  { Todo.create!(title:'foo', done:false, user_id:user.id) }
        schema({"$ref" => "#/components/schemas/todo"})
        run_test!
      end
      
      response '422', 'invalid request' do
        let(:todo) { {title:nil} }
        run_test!
      end
    end  # POST
  end # path "/todos"

  path '/api/v1/todos/{id}' do
    # show()
    get 'Retrieves a todo' do
      tags 'Todos'

      description 'Get a todo'

      #consumes 'application/json'
      parameter name: :id, in: :path, type: :string
      response '404', 'todo not found' do
        let(:id) { 'invalid' }
        run_test!
      end

      produces 'application/json'
      response '200', 'todo found' do
        schema({"$ref" => "#/components/schemas/todo"})

        # これが必要っぽい?
        let(:id) { Todo.create!(title: 'foo', done:true, user_id:user.id).id }
        run_test!
      end

      # いつでも JSON で返す
      #response '406', 'unsupported accept header' do
      #  let(:'Accept') { 'application/foo' }
      #  let(:id) { Todo.create!(title: 'foo', done:true, user_id:user.id).id }
      #  run_test!
      #end
    end

    # update()
    patch 'Updates a todo' do
      tags 'Todos'

      consumes 'application/json'
      parameter name: :id, in: :path, type: :string
      response '404', 'todo not found' do
        let(:id) { 'invalid' }
        let(:todo) {}
        run_test!
      end
      
      parameter name: :todo, in: :body, schema: {
                  type: :object,
                  properties: {
                    title: {type: :string},
                    done: {type: :boolean} },
                  required: ['title', 'done']
                }
      
      produces 'application/json'
      response '200', 'todo updated' do
        let(:todo) { Todo.create!(title:'foo', done:false, user_id:user.id) }
        let(:id)   { todo.id }
        
        schema({"$ref" => "#/components/schemas/todo"}) 
        run_test!
      end
    end # patch

    delete 'Deletes a todo' do
      tags 'Todos'
      
      parameter name: :id, in: :path, type: :string
      response '404', 'todo not found' do
        let(:id) { 'invalid' }
        run_test!
      end

      response '204', 'todo deleted' do
        let(:id) { Todo.create!(title: 'foo', done:true, user_id:user.id).id }
        run_test!
      end
    end # delete
    
  end # path '/todos/{id}'
end
