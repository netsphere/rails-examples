
require 'rails_helper'

RSpec.describe "todos/edit", type: :view do
  let(:todo) { Todo.create!(title:"name_hoge", done:false, user_id:@user.id) }

  before(:each) do
    @user = User.create!(email:"hoge@hoge", password:"123456",
                         password_confirmation:"123456")
    assign(:todo, todo)
  end

  it "renders the edit todo form" do
    render
    assert_select "form[action=?][method=?]", todo_path(todo), "post" do |form|
      assert_select 'input[name=_method][value=patch]', count:1
      assert_select 'input[name=?]', 'todo[title]'
    end
  end
end
