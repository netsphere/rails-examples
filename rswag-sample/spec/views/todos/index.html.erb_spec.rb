
require 'rails_helper'

RSpec.describe "todos/index", type: :view do
  before(:each) do
    @user = User.create!(email:"hoge@hoge", password:"123456",
                         password_confirmation:"123456")

    # `assign()` は view スコープで, インスタンス変数に値をセット.
    assign(:todos, [
      Todo.create!(title:"name_hoge", done:false, user_id:@user.id),
      Todo.create!(title:"name_hage", done:true, user_id:@user.id)
    ])
  end

  it "renders a list of todos" do
    render
    #cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    expect(rendered).to include "name_hoge"
    expect(rendered).to include "name_hage"
  end

  it "renders new todo form" do
    render
    assert_select "form[action=?][method=?]", todos_path, "post" do
      assert_select 'input[name=?]', 'todo[title]'
    end
  end

end
