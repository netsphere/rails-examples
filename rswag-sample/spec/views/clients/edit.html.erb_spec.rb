
require 'rails_helper'

RSpec.describe "clients/edit", type: :view do
  let(:client) { Client.create!(name:"hoge") }

  before(:each) do
    # view の scope でのインスタンス変数
    assign(:client, client)
  end

  it "renders the edit client form" do
    render

    assert_select "form[action=?][method=?]", client_path(client), "post" do |form|
      assert_select 'input[name=_method][value=patch]', count:1
      assert_select 'input[name=?]', 'client[name]'
    end
  end
end
