
require 'rails_helper'

RSpec.describe "clients/show", type: :view do
  before(:each) do
    assign(:client, Client.create!(name:"name-hage"))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to include "name-hage"
  end
end
