
# アクセストークン
class CreateAccessTokens < ActiveRecord::Migration[7.1]
  def change
    create_table :clients do |t|
      t.string "name", null:false
      t.string "identifier", null: false, index:{ unique:true }
      # client secret
      t.string "secret", null: false

      t.timestamps
    end

    create_table :access_tokens do |t|
      t.references :client, null:false, foreign_key:true
      t.references :user, null:false, foreign_key:true
      t.string "token", null:false, index:{unique:true}
      t.datetime "expires_at", null: false

      t.timestamps
    end
  end
end
