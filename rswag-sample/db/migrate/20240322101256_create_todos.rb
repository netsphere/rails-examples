
class CreateTodos < ActiveRecord::Migration[7.1]
  def change
    create_table :todos do |t|
      t.string :title, null:false
      t.boolean   :done, null:false
      #t.integer :user_id
      t.references :user, null:false, foreign_key:true
      
      t.timestamps
    end
  end
end
