
# クライアントアプリケィション
class Client < ApplicationRecord
  has_many :access_tokens

  before_validation :setup, on: :create
  
  validates :name,       presence:true
  validates :identifier, presence:true, uniqueness:true
  validates :secret,     presence:true

private
  # `before_validation`
  def setup
    self.identifier = SecureRandom.hex(16)
    self.secret     = SecureRandom.hex(32)
  end
  
end
