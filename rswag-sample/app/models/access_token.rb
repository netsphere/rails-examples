
# アクセストークン
class AccessToken < ApplicationRecord
  belongs_to :client
  belongs_to :user

  before_validation :setup, on: :create
  
  validates :client,     presence:true
  validates :user,       presence:true
  validates :token,      presence:true, uniqueness:true
  validates :expires_at, presence:true

  def accessible?(scopes_or_claims = nil)
    true # 手抜き ●●
  end
  
private
  def setup
    self.token = SecureRandom.hex(32)
    # `DateTime` クラスは非推奨
    self.expires_at = Time.now + (60 * 60 * 24) # 1日
  end
  
end
