
# ユーザ
class User < ApplicationRecord
  # Sorcery core モジュール
  # この呼び出しにより, サブモジュールの include や, init_orm_hooks! など.
  authenticates_with_sorcery!

  has_many :access_tokens

  # メールアドレスで識別.
  validates :email, presence:true
  validates :email, uniqueness: true

  validates :password, length: {minimum: 6},
                       if: -> { new_record? || changes[:crypted_password] }
  # ActiveRecord の機能。フィールド名は対象のフィールド名に _confirmation を加
  # えたものになる。
  validates :password, confirmation: true,
                       if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true,
                       if: -> { new_record? || changes[:crypted_password] }
end

