// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails

// TURBO
import "@hotwired/turbo-rails"

// Stimulus
import "controllers"

// Bootstrap はこれが必要:
import * as bootstrap from "bootstrap"
