
# トークン発行 & 削除
class Auth::TokensController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include ActionController::RequestForgeryProtection
  
  # revoke も basic 認証. 
  before_action :client_authentication

  #skip_before_action :require_login
  
  # `verify_authenticity_token` (verify CSRF token) を無効化
  # protect_from_forgery with: :null_session   これじゃない
  skip_forgery_protection  # こっちが正解

  # POST /auth/token (単数形)
=begin  本当は次のようなリクエストに応答する:
POST /token HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW

grant_type=authorization_code&code=SplxlOBeZQQYbYS6WxSbIA
    &redirect_uri=https%3A%2F%2Fclient.example.org%2Fcb
=end
  def create
    raise if !current_user
    
    actoken = AccessToken.create!({client: @client, user: current_user})
    render json: {
             "access_token": actoken.token,
             "token_type": "Bearer",
             "expires_in": (actoken.expires_at - Time.now).to_i,
           }
    response.headers['Cache-Control'] = 'no-store'
    response.headers['Pragma'] = 'no-cache'
  end

  # RFC 7009 OAuth 2.0 Token Revocation
  # POST
  def destroy
    # TODO: impl.
  end

  
private
  # `client_secret_basic` - HTTP Basic authentication scheme を使う
  # ちょー手抜き実装. email, password からトークンをつくる
  # @note `authenticate_or_request_with_http_basic()` だとエラーレスポンスを
  #       カスタマイズできない。
  def client_authentication
    # 失敗したときは 401 を返す
    (authenticate_with_http_basic do |username, password|
      # non-nil value を返すと認証成功.
      @client = Client.where('identifier = ? AND secret = ?', username, password).first
      next nil if !@client
      
      # ここが手抜き ●●
      @user = User.new(email: params[:email], password: params[:password])
      login(@user.email, @user.password)
    end) || request_authentication("Application", nil)
  end

  # JSON で返す
  # See https://www.ietf.org/archive/id/draft-ietf-oauth-v2-1-10.html#section-3.2.4
  def request_authentication(realm, message)
    message ||= "HTTP Basic: Access denied."
    response.headers["WWW-Authenticate"] = %(Basic realm="#{realm.tr('"', "")}")
    render json: {"error" => "invalid_request", "error_description" => message},
           status: :unauthorized # 401
  end
end
