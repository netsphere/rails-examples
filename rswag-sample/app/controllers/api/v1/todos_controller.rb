
class Api::V1::TodosController < Api::ApiController
  before_action :set_todo, only: %i[ show update destroy ]

  # GET
  def index
    raise if !current_user
    @todos = Todo.where('user_id = ?', current_user.id)
    count = @todos.length
    render json: {
             count: count,
             value: @todos
           }
  end

  # POST
  def create
    @todo = Todo.new(todo_params)
    @todo.done = false
    @todo.user_id = current_user.id
    
    if @todo.save
      render json: @todo, status: :created # 201
    else
      render json: {
               error: "invalid"
             }, status: :unprocessable_entity # 422
    end
  end

  # GET /todos/{id}
  def show
    render json: @todo
  end

  # PUT
  def update
    if @todo.update(todo_params)
      render json: @todo
    else
      render json: {
               error: "invalid"
             }, status: :unprocessable_entity # 422
    end
  end

  def destroy
    @todo.destroy!

    render json: {}, status: :no_content # 204
  end

  
private
  def set_todo
    @todo = Todo.where('user_id = ? AND id = ?', current_user.id, params[:id])
                .first
    raise ActiveRecord::RecordNotFound if !@todo
  end

  # Only allow a list of trusted parameters through.
  def todo_params
    params.require(:todo).permit(:title, :done)
  end

end
