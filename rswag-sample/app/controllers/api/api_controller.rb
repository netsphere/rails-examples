
# API のコントローラは `ActionController::API` から派生
class Api::ApiController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  
  # 認可。ポカ避けのため, 不要な場合は `skip_before_action` すること。
  before_action :authorization


private
  # for `before_action`
  def authorization
    # TOKEN_REGEX = /^(Token|Bearer)\s+/  このいずれか
    # `authenticate_or_request_with_http_token()` は, 
    # 失敗したときのメッセージ返信を兼ねるが, "Token" を使っており,
    # RFC 6750 The OAuth 2.0 Authorization Framework: Bearer Token Usage (October 2012) に適合しない
    dbg_token = nil # DEBUG
    (authenticate_with_http_token do |token, options|
       dbg_token = token
       # non-nil value を返すと認可成功.
       actoken = AccessToken.find_by_token token
       next nil if !actoken || !actoken.accessible?

       # sorcery: `current_user=` メソッドではない
       auto_login(actoken.user)
       true
     end) || request_http_bearer_authentication("Application", "Your token is #{dbg_token}, not found.")
  end

  # HTTP/1.1 401 Unauthorized
  # `Bearer` はヘッダで返す.
  # WWW-Authenticate: Bearer realm="example",
  #                   error="invalid_token",
  #                   error_description="The access token expired"
  def request_http_bearer_authentication(realm, message, attrs = {})
    message ||= "HTTP Token: Access denied.\n"
    res = (attrs.merge({realm: realm}).map do |k, v|
             %Q(#{k}="#{v.tr('"', "")}")
           end).join(", ")
    response.headers["WWW-Authenticate"] = "Bearer " + res
    # `before_action` メソッド内で `render`, `redirect_to` すると, 以降の chain は止まる
    render plain: message, status: :unauthorized
  end
  
end

