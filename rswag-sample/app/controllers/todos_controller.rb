
# ユーザがアクセスする
class TodosController < ApplicationController
  before_action :set_todo, only: %i[ done edit edit_cxl update destroy ]

  # GET /todos or /todos.json
  # 一覧表示と [追加] ボタン
  def index
    @todos = Todo.where('user_id = ?', current_user.id)
    @todo = Todo.new
  end

  # POST
  def done
    @todo.done = true
    if @todo.save
      render partial:'todo', locals:{todo:@todo}  # `turbo_frame_tag()` で返す
    else
      render :new, status: :unprocessable_entity 
    end
  end
  
  # GET /todos/1/edit
  def edit
    # ここは turbo frames を使う
    # <s>spec test を考えると, `edit.turbo_stream.erb` ファイルを作ったほうがよい。</s>
    #render turbo_stream: turbo_stream.replace(@todo, partial:'form', locals:{todo:@todo})
  end

  def edit_cxl
    render partial:'todo', locals:{todo:@todo} # `turbo_frame_tag()` で返す
  end
  
  # POST /todos or /todos.json
  def create
    @todo = Todo.new(todo_params)
    @todo.done = false
    @todo.user_id = current_user.id
    
    if @todo.save
      #redirect_to todo_url(@todo), notice: "Todo was successfully created."
      @new_todo = Todo.new
      render :create
    else
      render turbo_stream: turbo_stream.replace('new_todo', partial:'new', locals:{todo:@todo})
    end
  end

  # PATCH/PUT /todos/1 or /todos/1.json
  def update
    if @todo.update(todo_params)
      #redirect_to todo_url(@todo), notice: "Todo was successfully updated."
      render partial:'todo', locals:{todo:@todo}  # `turbo_frame_tag()` で返す
    else
      render turbo_stream: turbo_stream.replace(@todo, partial:'form', locals:{todo:@todo})
    end
  end

  # DELETE /todos/1 or /todos/1.json
  def destroy
    @todo.destroy!

    # pagination が変わるので、リロードする
    redirect_to todos_url, notice: "Todo was successfully destroyed." #,
                #status: :see_other
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_todo
    @todo = Todo.where('user_id = ? AND id = ?', current_user.id, params[:id])
                .first
    raise ActiveRecord::RecordNotFound if !@todo
  end

  # Only allow a list of trusted parameters through.
  def todo_params
    params.require(:todo).permit(:title, :done)
  end
end
