
class AccountController < ApplicationController
  skip_before_action :require_login, only: %i[create sign_in sign_up]

  # GET
  def show
    @tokens = AccessToken.where('user_id = ? AND expires_at > ?',
                                current_user.id, Time.now)
  end
  
  # GET
  # メールアドレスで登録させる場合は、ページを分ける
  def sign_up
    if current_user
      redirect_to sign_in_account_url
      return
    end
    
    @user = User.new
  end
  
  # POST  -- user_registration
  def create
    @user = User.new user_params
    begin
      @user.save!  # 注意: ここで, @user.password がクリアされる
    rescue ActiveRecord::RecordInvalid
      @user.password = user_params[:password]
      render :sign_up, status: :unprocessable_entity
      return
    end

    # 流れでログインする.
    # `login()` は Sorcery::Controller module で定義される。
    u = login(@user.email, user_params[:password])
    raise "internal error" if !u
    redirect_to("/todos", notice: 'User created.')
  end
  
  # GET / POST
  # `login` というメソッド名にしてはならない。Sorcery と被る
  def sign_in
    if request.get?
      @user = User.new
      return 
    end
    
    @user = User.new user_params

    if login(@user.email, @user.password)
      redirect_back_or_to("/todos", notice: 'Login successful')
    else
      flash[:alert] = 'Login failed'
      # HTTP 422 Validation error. このstatus を付けないとエラー表示されない.
      render 'sign_in', status: :unprocessable_entity
    end    
  end

  # POST
  def sign_out
    logout
    redirect_to '/', notice: 'Logged out!' #, status: :see_other
  end

  
private
  def user_params
    # AccountController では自分のデータしか更新させないので, :password をこの
    # 場所で permit() しても差し支えない.
    # permit() は重ねることはできない (許可を広げられない) ので、コントローラ内
    # で許可範囲を変えたいときは、それぞれのアクション内で permit() すること.
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
