
# ユーザが使うコントローラは `ActionController::Base` から派生
class ApplicationController < ActionController::Base
  # ポカ避けのため, 不要な場合は派生クラスで `skip_before_action` すること。
  before_action :require_login

private
  def not_authenticated
    redirect_to "/account/sign_in", alert: 'ログインしてください'
  end  
end
