
# Ruby on Rails Examples

Ruby 3, Ruby on Rails 7/8 チュートリアル, サンプルアプリケィション.



## <a href="./riding-rails-the-ror-tutorial/">riding-rails-the-ror-tutorial/</a>

Tutorial. 最初のアプリケィション.
See https://www.nslabs.jp/RoR.rhtml




## Application Samples

### <a href="./Book2SampleShop/">Book2SampleShop/</a>

ショッピングカートのサンプル
See https://www.nslabs.jp/book2.rhtml


### <a href="./admin-sample/">admin-sample/</a>

Product master data.




## Authentication (authn)

### `LoginSample-sorcery-1/`

[Sorcery: Magical Authentication](https://github.com/Sorcery/sorcery/) のサンプル.

 - Ruby 3
 - Ruby on Rails 6.1
 - webpack (Webpacker は使わない)



### <a href="./facebook-login/">facebook-login/</a>

Facebook ログイン, グラフAPI は, 'fb_graph2' パッケージが有力だった。
しかし, 依存する rack-oauth2 v2 の破壊的変更で動かなくなっている。さらに, fb_graph2 が直接依存する httpclient も古すぎて, もはや使えない。

類似の Koala パッケージはメンテナンスが継続している。こちらを使った、ログイン、グラフAPI サンプル。



### <a href="./rails-rodauth-mfa/">rails-rodauth-mfa/</a>

rodauth-rails, MFA






### `01_hello-app-chat/`

チャットアプリ。サーバからの push.

Rails7 のアセットパイプライン (gem の JavaScript, CSS アセットとの結合、配信) は、デフォルトで次の組み合わせです。
 - JavaScript - importmap-rails
 - CSS - sprockets

Rails 6.1 までは sprockets が JavaScript, CSS の両方を担当していた。また、その前段として, Webpacker が JavaScript ファイルをまとめたり (バンドリング), トランスパイル (変換) していた。

Webpacker は Rails7 で単に廃止、importmap は, **トランスパイルやバンドルが不要なプロジェクトで**, それぞれのJavaScriptファイルをそのまま配信する。


#### Push 技術

 - Turbo Broadcast. View 側に `turbo_stream_from()`, モデル (!) に `broadcast_append_to()` などでストリームを流す。





### `02_rails-pwa/`

前回のサンプル `01_hello-app-chat` を修正。
 - jsbundling-rails
 - serviceworker-rails

#### PWA

次のファイルの置き場所は `/config/initializers/serviceworker.rb` に設定がある
|      URL            |   実体                                        |
|---------------------|-----------------------------------------------|
|`/manifest.json`     |`/app/assets/javascripts/manifest.json.erb`    |
|`/serviceworker.js`  |`/app/assets/javascripts/serviceworker.js.erb` |

優れた PWA は、オフラインのときにでも動作すべき。●未了.
 - Offline 時の対応   `public/offline.html`




### `rails-vite-react/`

Webpack to Vite.

`gem "vite_rails"`

.....





## API mode, backend

### `rswag-sample/`

rswag で OpenAPI の自動生成


### `graph-sample/`

graph model.

.....





## User Interactions

### <a href="./11_sample-with-pagy-and-inifite-scroll/">11_sample-with-pagy-and-inifite-scroll/</a>

ページネィションのサンプル。2018年に v1.0 になった軽量 pagination ライブラリ <a href="https://rubygems.org/gems/pagy/">Pagy</a> を利用。

Pagy は, <i>will_paginate</i> よりも速く、軽い。Rails だけでなく, Rack フレームワークであれば動作可能。

<i>Kaminari</i> は, Web上の解説が多いが, どんくさすぎて比較にならない。





### `tomselect-test/`

Tom Select - 検索なども可能な `<select>` タグの置き換え. Tom Select だと、どうもユーザ体験が狙った感じにならなかった。

同様のライブラリ Choices.js に切り替えた。よい。




### `rails7-fancytree/`

●●●未了. ツリーの更新を Turbo Streams でやろうとしても上手くいかない。Stimulus で更新すべき。



### `rails-table-edit/`

.....



